#!/bin/bash

###################
apt-get update
apt-get -y upgrade
apt-get -y install \
  wget \
  unzip \
  nginx \
  imagemagick \
  php \
  php-gd \
  php-fpm \
  php-xml \
  php-mbstring \
  php-pgsql \
  php-mysql
mkdir -p /var/run/php
chown www-data. /var/run/php
cd /var/run/php
fpm=`ls /etc/init.d/php*`
fpm=`basename ${fpm}`
ln -s ${fpm}.sock php-fpm.sock
###################
cd /srv
wget {{ phpbb_dl_url }}
tar xjf *.bz2
rm *.bz2
mv phpBB3 phpbb
cp /var/tmp/config.php ./phpbb/config.php
printf ok > ./phpbb/store/io_lock.lock
sed -i "s/'REMOTE_ADDR'/'HTTP_X_FORWARDED_FOR'/g" ./phpbb/phpbb/session.php
rm -rf ./phpbb/install
####################
cd ./phpbb/styles
mv /var/tmp/styles/* .
for style in `ls -1`; do
  [[ -n `echo ${style} | grep zip$` ]] && unzip ${style}
  [[ -n `echo ${style} | grep gz$` ]]  && tar xzf ${style}
  rm ${style}
done
####################
if [[ -n `echo "{{ phpbb_board_logo }}" | egrep -i 'yes|true'` ]]; then
  cd /var/tmp/logo
  logo=`ls -1`
  logo_ext="${logo##*.}"
  logo_basename="${logo%.*}"
  convert ${logo} -resize x26 ${logo_basename}_small.${logo_ext}
  convert ${logo} -resize x52 ${logo}
  logo_width=`convert ${logo} -format "%[w]px\n" info:`
  cd /srv/phpbb/styles
  for imgdir in `find -type d -name images`; do
    cp /var/tmp/logo/* ${imgdir}
  done
  for css_file in `find -name colours.css`; do
    sed -i "s/site_logo.png/${logo}/g" ${css_file}
    sed -i "s/site_logo.svg/${logo}/g" ${css_file}
    sed -i "s/site_logo.jpg/${logo}/g" ${css_file}
  done
  for css_file in `find -name common.css`; do
    ln=`cat -n ${css_file} | grep site_logo | awk -F' ' '{print $1}'`
    let ln1=${ln}+1
    let ln2=${ln}+2
    let ln3=${ln}+3
    let ln4=${ln}+4
    sed -i "${ln1}s/width.*/width: ${logo_width};/g" ${css_file}
    sed -i "${ln2}s/width.*/width: ${logo_width};/g" ${css_file}
    sed -i "${ln3}s/width.*/width: ${logo_width};/g" ${css_file}
    sed -i "${ln4}s/width.*/width: ${logo_width};/g" ${css_file}
  done
fi
####################
cd /var/tmp/languages
for lang in `ls *.zip`; do
  unzip ${lang}
  lang_pack=`find -maxdepth 1 -type d | tail -1`
  mv ${lang_pack}/language/* /srv/phpbb/language/
  mv ${lang_pack}/styles/prosilver/theme/* /srv/phpbb/styles/prosilver/theme/
  mv ${lang_pack}/ext/phpbb/viglink/language/* /srv/phpbb/ext/phpbb/viglink/language/
  rm -rf ${lang_pack}
done
####################
chown -R www-data. /srv/phpbb
chmod 755 /srv/phpbb/bin/*
###################
mv -v /var/tmp/nginx.conf /etc/nginx/sites-available/phpbb.conf
cd /etc/nginx/sites-enabled
rm default
ln -s ../sites-available/phpbb.conf

