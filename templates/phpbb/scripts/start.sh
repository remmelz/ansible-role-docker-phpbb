#!/bin/bash

cd /srv/phpbb/bin/
if [[ -z `./phpbbcli.php db:list \
  | grep -i 'no migrations'` ]];
then
  ./phpbbcli.php db:migrate --safe-mode
  rm -rf ../cache/*
  sleep 1
fi

fpm=`ls /etc/init.d/php*`
fpm=`basename ${fpm}`

service ${fpm} start
service nginx start
sleep 1

while true; do
  service nginx status
  sleep 10
done

