--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3 (Debian 13.3-1.pgdg100+1)
-- Dumped by pg_dump version 13.3 (Debian 13.3-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: varchar_ci; Type: DOMAIN; Schema: public; Owner: phpbb
--

CREATE DOMAIN public.varchar_ci AS character varying(255) NOT NULL DEFAULT ''::character varying;


ALTER DOMAIN public.varchar_ci OWNER TO phpbb;

--
-- Name: _varchar_ci_equal(public.varchar_ci, public.varchar_ci); Type: FUNCTION; Schema: public; Owner: phpbb
--

CREATE FUNCTION public._varchar_ci_equal(public.varchar_ci, public.varchar_ci) RETURNS boolean
    LANGUAGE sql STRICT
    AS $_$SELECT LOWER($1) = LOWER($2)$_$;


ALTER FUNCTION public._varchar_ci_equal(public.varchar_ci, public.varchar_ci) OWNER TO phpbb;

--
-- Name: _varchar_ci_greater_equals(public.varchar_ci, public.varchar_ci); Type: FUNCTION; Schema: public; Owner: phpbb
--

CREATE FUNCTION public._varchar_ci_greater_equals(public.varchar_ci, public.varchar_ci) RETURNS boolean
    LANGUAGE sql STRICT
    AS $_$SELECT LOWER($1) >= LOWER($2)$_$;


ALTER FUNCTION public._varchar_ci_greater_equals(public.varchar_ci, public.varchar_ci) OWNER TO phpbb;

--
-- Name: _varchar_ci_greater_than(public.varchar_ci, public.varchar_ci); Type: FUNCTION; Schema: public; Owner: phpbb
--

CREATE FUNCTION public._varchar_ci_greater_than(public.varchar_ci, public.varchar_ci) RETURNS boolean
    LANGUAGE sql STRICT
    AS $_$SELECT LOWER($1) > LOWER($2)$_$;


ALTER FUNCTION public._varchar_ci_greater_than(public.varchar_ci, public.varchar_ci) OWNER TO phpbb;

--
-- Name: _varchar_ci_less_equal(public.varchar_ci, public.varchar_ci); Type: FUNCTION; Schema: public; Owner: phpbb
--

CREATE FUNCTION public._varchar_ci_less_equal(public.varchar_ci, public.varchar_ci) RETURNS boolean
    LANGUAGE sql STRICT
    AS $_$SELECT LOWER($1) <= LOWER($2)$_$;


ALTER FUNCTION public._varchar_ci_less_equal(public.varchar_ci, public.varchar_ci) OWNER TO phpbb;

--
-- Name: _varchar_ci_less_than(public.varchar_ci, public.varchar_ci); Type: FUNCTION; Schema: public; Owner: phpbb
--

CREATE FUNCTION public._varchar_ci_less_than(public.varchar_ci, public.varchar_ci) RETURNS boolean
    LANGUAGE sql STRICT
    AS $_$SELECT LOWER($1) < LOWER($2)$_$;


ALTER FUNCTION public._varchar_ci_less_than(public.varchar_ci, public.varchar_ci) OWNER TO phpbb;

--
-- Name: _varchar_ci_not_equal(public.varchar_ci, public.varchar_ci); Type: FUNCTION; Schema: public; Owner: phpbb
--

CREATE FUNCTION public._varchar_ci_not_equal(public.varchar_ci, public.varchar_ci) RETURNS boolean
    LANGUAGE sql STRICT
    AS $_$SELECT LOWER($1) != LOWER($2)$_$;


ALTER FUNCTION public._varchar_ci_not_equal(public.varchar_ci, public.varchar_ci) OWNER TO phpbb;

--
-- Name: <; Type: OPERATOR; Schema: public; Owner: phpbb
--

CREATE OPERATOR public.< (
    FUNCTION = public._varchar_ci_less_than,
    LEFTARG = public.varchar_ci,
    RIGHTARG = public.varchar_ci,
    COMMUTATOR = OPERATOR(public.>),
    NEGATOR = OPERATOR(public.>=),
    RESTRICT = scalarltsel,
    JOIN = scalarltjoinsel
);


ALTER OPERATOR public.< (public.varchar_ci, public.varchar_ci) OWNER TO phpbb;

--
-- Name: <=; Type: OPERATOR; Schema: public; Owner: phpbb
--

CREATE OPERATOR public.<= (
    FUNCTION = public._varchar_ci_less_equal,
    LEFTARG = public.varchar_ci,
    RIGHTARG = public.varchar_ci,
    COMMUTATOR = OPERATOR(public.>=),
    NEGATOR = OPERATOR(public.>),
    RESTRICT = scalarltsel,
    JOIN = scalarltjoinsel
);


ALTER OPERATOR public.<= (public.varchar_ci, public.varchar_ci) OWNER TO phpbb;

--
-- Name: <>; Type: OPERATOR; Schema: public; Owner: phpbb
--

CREATE OPERATOR public.<> (
    FUNCTION = public._varchar_ci_not_equal,
    LEFTARG = public.varchar_ci,
    RIGHTARG = public.varchar_ci,
    COMMUTATOR = OPERATOR(public.<>),
    NEGATOR = OPERATOR(public.=),
    RESTRICT = neqsel,
    JOIN = neqjoinsel
);


ALTER OPERATOR public.<> (public.varchar_ci, public.varchar_ci) OWNER TO phpbb;

--
-- Name: =; Type: OPERATOR; Schema: public; Owner: phpbb
--

CREATE OPERATOR public.= (
    FUNCTION = public._varchar_ci_equal,
    LEFTARG = public.varchar_ci,
    RIGHTARG = public.varchar_ci,
    COMMUTATOR = OPERATOR(public.=),
    NEGATOR = OPERATOR(public.<>),
    MERGES,
    HASHES,
    RESTRICT = eqsel,
    JOIN = eqjoinsel
);


ALTER OPERATOR public.= (public.varchar_ci, public.varchar_ci) OWNER TO phpbb;

--
-- Name: >; Type: OPERATOR; Schema: public; Owner: phpbb
--

CREATE OPERATOR public.> (
    FUNCTION = public._varchar_ci_greater_than,
    LEFTARG = public.varchar_ci,
    RIGHTARG = public.varchar_ci,
    COMMUTATOR = OPERATOR(public.<),
    NEGATOR = OPERATOR(public.<=),
    RESTRICT = scalargtsel,
    JOIN = scalargtjoinsel
);


ALTER OPERATOR public.> (public.varchar_ci, public.varchar_ci) OWNER TO phpbb;

--
-- Name: >=; Type: OPERATOR; Schema: public; Owner: phpbb
--

CREATE OPERATOR public.>= (
    FUNCTION = public._varchar_ci_greater_equals,
    LEFTARG = public.varchar_ci,
    RIGHTARG = public.varchar_ci,
    COMMUTATOR = OPERATOR(public.<=),
    NEGATOR = OPERATOR(public.<),
    RESTRICT = scalargtsel,
    JOIN = scalargtjoinsel
);


ALTER OPERATOR public.>= (public.varchar_ci, public.varchar_ci) OWNER TO phpbb;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: phpbb_acl_groups; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_acl_groups (
    group_id integer DEFAULT 0 NOT NULL,
    forum_id integer DEFAULT 0 NOT NULL,
    auth_option_id integer DEFAULT 0 NOT NULL,
    auth_role_id integer DEFAULT 0 NOT NULL,
    auth_setting smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_acl_groups_auth_option_id_check CHECK ((auth_option_id >= 0)),
    CONSTRAINT phpbb_acl_groups_auth_role_id_check CHECK ((auth_role_id >= 0)),
    CONSTRAINT phpbb_acl_groups_forum_id_check CHECK ((forum_id >= 0)),
    CONSTRAINT phpbb_acl_groups_group_id_check CHECK ((group_id >= 0))
);


ALTER TABLE public.phpbb_acl_groups OWNER TO phpbb;

--
-- Name: phpbb_acl_options_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_acl_options_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_acl_options_seq OWNER TO phpbb;

--
-- Name: phpbb_acl_options; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_acl_options (
    auth_option_id integer DEFAULT nextval('public.phpbb_acl_options_seq'::regclass) NOT NULL,
    auth_option character varying(50) DEFAULT ''::character varying NOT NULL,
    is_global smallint DEFAULT '0'::smallint NOT NULL,
    is_local smallint DEFAULT '0'::smallint NOT NULL,
    founder_only smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_acl_options_auth_option_id_check CHECK ((auth_option_id >= 0)),
    CONSTRAINT phpbb_acl_options_founder_only_check CHECK ((founder_only >= 0)),
    CONSTRAINT phpbb_acl_options_is_global_check CHECK ((is_global >= 0)),
    CONSTRAINT phpbb_acl_options_is_local_check CHECK ((is_local >= 0))
);


ALTER TABLE public.phpbb_acl_options OWNER TO phpbb;

--
-- Name: phpbb_acl_roles_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_acl_roles_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_acl_roles_seq OWNER TO phpbb;

--
-- Name: phpbb_acl_roles; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_acl_roles (
    role_id integer DEFAULT nextval('public.phpbb_acl_roles_seq'::regclass) NOT NULL,
    role_name character varying(255) DEFAULT ''::character varying NOT NULL,
    role_description character varying(4000) DEFAULT ''::character varying NOT NULL,
    role_type character varying(10) DEFAULT ''::character varying NOT NULL,
    role_order smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_acl_roles_role_id_check CHECK ((role_id >= 0)),
    CONSTRAINT phpbb_acl_roles_role_order_check CHECK ((role_order >= 0))
);


ALTER TABLE public.phpbb_acl_roles OWNER TO phpbb;

--
-- Name: phpbb_acl_roles_data; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_acl_roles_data (
    role_id integer DEFAULT 0 NOT NULL,
    auth_option_id integer DEFAULT 0 NOT NULL,
    auth_setting smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_acl_roles_data_auth_option_id_check CHECK ((auth_option_id >= 0)),
    CONSTRAINT phpbb_acl_roles_data_role_id_check CHECK ((role_id >= 0))
);


ALTER TABLE public.phpbb_acl_roles_data OWNER TO phpbb;

--
-- Name: phpbb_acl_users; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_acl_users (
    user_id integer DEFAULT 0 NOT NULL,
    forum_id integer DEFAULT 0 NOT NULL,
    auth_option_id integer DEFAULT 0 NOT NULL,
    auth_role_id integer DEFAULT 0 NOT NULL,
    auth_setting smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_acl_users_auth_option_id_check CHECK ((auth_option_id >= 0)),
    CONSTRAINT phpbb_acl_users_auth_role_id_check CHECK ((auth_role_id >= 0)),
    CONSTRAINT phpbb_acl_users_forum_id_check CHECK ((forum_id >= 0)),
    CONSTRAINT phpbb_acl_users_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_acl_users OWNER TO phpbb;

--
-- Name: phpbb_attachments_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_attachments_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_attachments_seq OWNER TO phpbb;

--
-- Name: phpbb_attachments; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_attachments (
    attach_id integer DEFAULT nextval('public.phpbb_attachments_seq'::regclass) NOT NULL,
    post_msg_id integer DEFAULT 0 NOT NULL,
    topic_id integer DEFAULT 0 NOT NULL,
    in_message smallint DEFAULT '0'::smallint NOT NULL,
    poster_id integer DEFAULT 0 NOT NULL,
    is_orphan smallint DEFAULT '1'::smallint NOT NULL,
    physical_filename character varying(255) DEFAULT ''::character varying NOT NULL,
    real_filename character varying(255) DEFAULT ''::character varying NOT NULL,
    download_count integer DEFAULT 0 NOT NULL,
    attach_comment character varying(4000) DEFAULT ''::character varying NOT NULL,
    extension character varying(100) DEFAULT ''::character varying NOT NULL,
    mimetype character varying(100) DEFAULT ''::character varying NOT NULL,
    filesize integer DEFAULT 0 NOT NULL,
    filetime integer DEFAULT 0 NOT NULL,
    thumbnail smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_attachments_attach_id_check CHECK ((attach_id >= 0)),
    CONSTRAINT phpbb_attachments_download_count_check CHECK ((download_count >= 0)),
    CONSTRAINT phpbb_attachments_filesize_check CHECK ((filesize >= 0)),
    CONSTRAINT phpbb_attachments_filetime_check CHECK ((filetime >= 0)),
    CONSTRAINT phpbb_attachments_in_message_check CHECK ((in_message >= 0)),
    CONSTRAINT phpbb_attachments_is_orphan_check CHECK ((is_orphan >= 0)),
    CONSTRAINT phpbb_attachments_post_msg_id_check CHECK ((post_msg_id >= 0)),
    CONSTRAINT phpbb_attachments_poster_id_check CHECK ((poster_id >= 0)),
    CONSTRAINT phpbb_attachments_thumbnail_check CHECK ((thumbnail >= 0)),
    CONSTRAINT phpbb_attachments_topic_id_check CHECK ((topic_id >= 0))
);


ALTER TABLE public.phpbb_attachments OWNER TO phpbb;

--
-- Name: phpbb_banlist_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_banlist_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_banlist_seq OWNER TO phpbb;

--
-- Name: phpbb_banlist; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_banlist (
    ban_id integer DEFAULT nextval('public.phpbb_banlist_seq'::regclass) NOT NULL,
    ban_userid integer DEFAULT 0 NOT NULL,
    ban_ip character varying(40) DEFAULT ''::character varying NOT NULL,
    ban_email character varying(100) DEFAULT ''::character varying NOT NULL,
    ban_start integer DEFAULT 0 NOT NULL,
    ban_end integer DEFAULT 0 NOT NULL,
    ban_exclude smallint DEFAULT '0'::smallint NOT NULL,
    ban_reason character varying(255) DEFAULT ''::character varying NOT NULL,
    ban_give_reason character varying(255) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_banlist_ban_end_check CHECK ((ban_end >= 0)),
    CONSTRAINT phpbb_banlist_ban_exclude_check CHECK ((ban_exclude >= 0)),
    CONSTRAINT phpbb_banlist_ban_id_check CHECK ((ban_id >= 0)),
    CONSTRAINT phpbb_banlist_ban_start_check CHECK ((ban_start >= 0)),
    CONSTRAINT phpbb_banlist_ban_userid_check CHECK ((ban_userid >= 0))
);


ALTER TABLE public.phpbb_banlist OWNER TO phpbb;

--
-- Name: phpbb_bbcodes; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_bbcodes (
    bbcode_id smallint DEFAULT '0'::smallint NOT NULL,
    bbcode_tag character varying(16) DEFAULT ''::character varying NOT NULL,
    bbcode_helpline character varying(255) DEFAULT ''::character varying NOT NULL,
    display_on_posting smallint DEFAULT '0'::smallint NOT NULL,
    bbcode_match character varying(4000) DEFAULT ''::character varying NOT NULL,
    bbcode_tpl text DEFAULT ''::text NOT NULL,
    first_pass_match text DEFAULT ''::text NOT NULL,
    first_pass_replace text DEFAULT ''::text NOT NULL,
    second_pass_match text DEFAULT ''::text NOT NULL,
    second_pass_replace text DEFAULT ''::text NOT NULL,
    CONSTRAINT phpbb_bbcodes_bbcode_id_check CHECK ((bbcode_id >= 0)),
    CONSTRAINT phpbb_bbcodes_display_on_posting_check CHECK ((display_on_posting >= 0))
);


ALTER TABLE public.phpbb_bbcodes OWNER TO phpbb;

--
-- Name: phpbb_bookmarks; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_bookmarks (
    topic_id integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_bookmarks_topic_id_check CHECK ((topic_id >= 0)),
    CONSTRAINT phpbb_bookmarks_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_bookmarks OWNER TO phpbb;

--
-- Name: phpbb_bots_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_bots_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_bots_seq OWNER TO phpbb;

--
-- Name: phpbb_bots; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_bots (
    bot_id integer DEFAULT nextval('public.phpbb_bots_seq'::regclass) NOT NULL,
    bot_active smallint DEFAULT '1'::smallint NOT NULL,
    bot_name character varying(255) DEFAULT ''::character varying NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    bot_agent character varying(255) DEFAULT ''::character varying NOT NULL,
    bot_ip character varying(255) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_bots_bot_active_check CHECK ((bot_active >= 0)),
    CONSTRAINT phpbb_bots_bot_id_check CHECK ((bot_id >= 0)),
    CONSTRAINT phpbb_bots_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_bots OWNER TO phpbb;

--
-- Name: phpbb_config; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_config (
    config_name character varying(255) DEFAULT ''::character varying NOT NULL,
    config_value character varying(255) DEFAULT ''::character varying NOT NULL,
    is_dynamic smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_config_is_dynamic_check CHECK ((is_dynamic >= 0))
);


ALTER TABLE public.phpbb_config OWNER TO phpbb;

--
-- Name: phpbb_config_text; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_config_text (
    config_name character varying(255) DEFAULT ''::character varying NOT NULL,
    config_value text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.phpbb_config_text OWNER TO phpbb;

--
-- Name: phpbb_confirm; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_confirm (
    confirm_id character(32) DEFAULT ''::bpchar NOT NULL,
    session_id character(32) DEFAULT ''::bpchar NOT NULL,
    confirm_type smallint DEFAULT '0'::smallint NOT NULL,
    code character varying(8) DEFAULT ''::character varying NOT NULL,
    seed integer DEFAULT 0 NOT NULL,
    attempts integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_confirm_attempts_check CHECK ((attempts >= 0)),
    CONSTRAINT phpbb_confirm_seed_check CHECK ((seed >= 0))
);


ALTER TABLE public.phpbb_confirm OWNER TO phpbb;

--
-- Name: phpbb_disallow_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_disallow_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_disallow_seq OWNER TO phpbb;

--
-- Name: phpbb_disallow; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_disallow (
    disallow_id integer DEFAULT nextval('public.phpbb_disallow_seq'::regclass) NOT NULL,
    disallow_username character varying(255) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_disallow_disallow_id_check CHECK ((disallow_id >= 0))
);


ALTER TABLE public.phpbb_disallow OWNER TO phpbb;

--
-- Name: phpbb_drafts_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_drafts_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_drafts_seq OWNER TO phpbb;

--
-- Name: phpbb_drafts; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_drafts (
    draft_id integer DEFAULT nextval('public.phpbb_drafts_seq'::regclass) NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    topic_id integer DEFAULT 0 NOT NULL,
    forum_id integer DEFAULT 0 NOT NULL,
    save_time integer DEFAULT 0 NOT NULL,
    draft_subject character varying(255) DEFAULT ''::character varying NOT NULL,
    draft_message text DEFAULT ''::text NOT NULL,
    CONSTRAINT phpbb_drafts_draft_id_check CHECK ((draft_id >= 0)),
    CONSTRAINT phpbb_drafts_forum_id_check CHECK ((forum_id >= 0)),
    CONSTRAINT phpbb_drafts_save_time_check CHECK ((save_time >= 0)),
    CONSTRAINT phpbb_drafts_topic_id_check CHECK ((topic_id >= 0)),
    CONSTRAINT phpbb_drafts_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_drafts OWNER TO phpbb;

--
-- Name: phpbb_ext; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_ext (
    ext_name character varying(255) DEFAULT ''::character varying NOT NULL,
    ext_active smallint DEFAULT '0'::smallint NOT NULL,
    ext_state character varying(8000) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_ext_ext_active_check CHECK ((ext_active >= 0))
);


ALTER TABLE public.phpbb_ext OWNER TO phpbb;

--
-- Name: phpbb_extension_groups_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_extension_groups_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_extension_groups_seq OWNER TO phpbb;

--
-- Name: phpbb_extension_groups; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_extension_groups (
    group_id integer DEFAULT nextval('public.phpbb_extension_groups_seq'::regclass) NOT NULL,
    group_name character varying(255) DEFAULT ''::character varying NOT NULL,
    cat_id smallint DEFAULT '0'::smallint NOT NULL,
    allow_group smallint DEFAULT '0'::smallint NOT NULL,
    download_mode smallint DEFAULT '1'::smallint NOT NULL,
    upload_icon character varying(255) DEFAULT ''::character varying NOT NULL,
    max_filesize integer DEFAULT 0 NOT NULL,
    allowed_forums character varying(8000) DEFAULT ''::character varying NOT NULL,
    allow_in_pm smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_extension_groups_allow_group_check CHECK ((allow_group >= 0)),
    CONSTRAINT phpbb_extension_groups_allow_in_pm_check CHECK ((allow_in_pm >= 0)),
    CONSTRAINT phpbb_extension_groups_download_mode_check CHECK ((download_mode >= 0)),
    CONSTRAINT phpbb_extension_groups_group_id_check CHECK ((group_id >= 0)),
    CONSTRAINT phpbb_extension_groups_max_filesize_check CHECK ((max_filesize >= 0))
);


ALTER TABLE public.phpbb_extension_groups OWNER TO phpbb;

--
-- Name: phpbb_extensions_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_extensions_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_extensions_seq OWNER TO phpbb;

--
-- Name: phpbb_extensions; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_extensions (
    extension_id integer DEFAULT nextval('public.phpbb_extensions_seq'::regclass) NOT NULL,
    group_id integer DEFAULT 0 NOT NULL,
    extension character varying(100) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_extensions_extension_id_check CHECK ((extension_id >= 0)),
    CONSTRAINT phpbb_extensions_group_id_check CHECK ((group_id >= 0))
);


ALTER TABLE public.phpbb_extensions OWNER TO phpbb;

--
-- Name: phpbb_forums_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_forums_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_forums_seq OWNER TO phpbb;

--
-- Name: phpbb_forums; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_forums (
    forum_id integer DEFAULT nextval('public.phpbb_forums_seq'::regclass) NOT NULL,
    parent_id integer DEFAULT 0 NOT NULL,
    left_id integer DEFAULT 0 NOT NULL,
    right_id integer DEFAULT 0 NOT NULL,
    forum_parents text DEFAULT ''::text NOT NULL,
    forum_name character varying(255) DEFAULT ''::character varying NOT NULL,
    forum_desc character varying(4000) DEFAULT ''::character varying NOT NULL,
    forum_desc_bitfield character varying(255) DEFAULT ''::character varying NOT NULL,
    forum_desc_options integer DEFAULT 7 NOT NULL,
    forum_desc_uid character varying(8) DEFAULT ''::character varying NOT NULL,
    forum_link character varying(255) DEFAULT ''::character varying NOT NULL,
    forum_password character varying(255) DEFAULT ''::character varying NOT NULL,
    forum_style integer DEFAULT 0 NOT NULL,
    forum_image character varying(255) DEFAULT ''::character varying NOT NULL,
    forum_rules character varying(4000) DEFAULT ''::character varying NOT NULL,
    forum_rules_link character varying(255) DEFAULT ''::character varying NOT NULL,
    forum_rules_bitfield character varying(255) DEFAULT ''::character varying NOT NULL,
    forum_rules_options integer DEFAULT 7 NOT NULL,
    forum_rules_uid character varying(8) DEFAULT ''::character varying NOT NULL,
    forum_topics_per_page smallint DEFAULT '0'::smallint NOT NULL,
    forum_type smallint DEFAULT '0'::smallint NOT NULL,
    forum_status smallint DEFAULT '0'::smallint NOT NULL,
    forum_last_post_id integer DEFAULT 0 NOT NULL,
    forum_last_poster_id integer DEFAULT 0 NOT NULL,
    forum_last_post_subject character varying(255) DEFAULT ''::character varying NOT NULL,
    forum_last_post_time integer DEFAULT 0 NOT NULL,
    forum_last_poster_name character varying(255) DEFAULT ''::character varying NOT NULL,
    forum_last_poster_colour character varying(6) DEFAULT ''::character varying NOT NULL,
    forum_flags smallint DEFAULT '32'::smallint NOT NULL,
    display_on_index smallint DEFAULT '1'::smallint NOT NULL,
    enable_indexing smallint DEFAULT '1'::smallint NOT NULL,
    enable_icons smallint DEFAULT '1'::smallint NOT NULL,
    enable_prune smallint DEFAULT '0'::smallint NOT NULL,
    prune_next integer DEFAULT 0 NOT NULL,
    prune_days integer DEFAULT 0 NOT NULL,
    prune_viewed integer DEFAULT 0 NOT NULL,
    prune_freq integer DEFAULT 0 NOT NULL,
    display_subforum_list smallint DEFAULT '1'::smallint NOT NULL,
    display_subforum_limit smallint DEFAULT '0'::smallint NOT NULL,
    forum_options integer DEFAULT 0 NOT NULL,
    forum_posts_approved integer DEFAULT 0 NOT NULL,
    forum_posts_unapproved integer DEFAULT 0 NOT NULL,
    forum_posts_softdeleted integer DEFAULT 0 NOT NULL,
    forum_topics_approved integer DEFAULT 0 NOT NULL,
    forum_topics_unapproved integer DEFAULT 0 NOT NULL,
    forum_topics_softdeleted integer DEFAULT 0 NOT NULL,
    enable_shadow_prune smallint DEFAULT '0'::smallint NOT NULL,
    prune_shadow_days integer DEFAULT 7 NOT NULL,
    prune_shadow_freq integer DEFAULT 1 NOT NULL,
    prune_shadow_next integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_forums_display_on_index_check CHECK ((display_on_index >= 0)),
    CONSTRAINT phpbb_forums_display_subforum_limit_check CHECK ((display_subforum_limit >= 0)),
    CONSTRAINT phpbb_forums_display_subforum_list_check CHECK ((display_subforum_list >= 0)),
    CONSTRAINT phpbb_forums_enable_icons_check CHECK ((enable_icons >= 0)),
    CONSTRAINT phpbb_forums_enable_indexing_check CHECK ((enable_indexing >= 0)),
    CONSTRAINT phpbb_forums_enable_prune_check CHECK ((enable_prune >= 0)),
    CONSTRAINT phpbb_forums_enable_shadow_prune_check CHECK ((enable_shadow_prune >= 0)),
    CONSTRAINT phpbb_forums_forum_desc_options_check CHECK ((forum_desc_options >= 0)),
    CONSTRAINT phpbb_forums_forum_id_check CHECK ((forum_id >= 0)),
    CONSTRAINT phpbb_forums_forum_last_post_id_check CHECK ((forum_last_post_id >= 0)),
    CONSTRAINT phpbb_forums_forum_last_post_time_check CHECK ((forum_last_post_time >= 0)),
    CONSTRAINT phpbb_forums_forum_last_poster_id_check CHECK ((forum_last_poster_id >= 0)),
    CONSTRAINT phpbb_forums_forum_options_check CHECK ((forum_options >= 0)),
    CONSTRAINT phpbb_forums_forum_posts_approved_check CHECK ((forum_posts_approved >= 0)),
    CONSTRAINT phpbb_forums_forum_posts_softdeleted_check CHECK ((forum_posts_softdeleted >= 0)),
    CONSTRAINT phpbb_forums_forum_posts_unapproved_check CHECK ((forum_posts_unapproved >= 0)),
    CONSTRAINT phpbb_forums_forum_rules_options_check CHECK ((forum_rules_options >= 0)),
    CONSTRAINT phpbb_forums_forum_style_check CHECK ((forum_style >= 0)),
    CONSTRAINT phpbb_forums_forum_topics_approved_check CHECK ((forum_topics_approved >= 0)),
    CONSTRAINT phpbb_forums_forum_topics_per_page_check CHECK ((forum_topics_per_page >= 0)),
    CONSTRAINT phpbb_forums_forum_topics_softdeleted_check CHECK ((forum_topics_softdeleted >= 0)),
    CONSTRAINT phpbb_forums_forum_topics_unapproved_check CHECK ((forum_topics_unapproved >= 0)),
    CONSTRAINT phpbb_forums_left_id_check CHECK ((left_id >= 0)),
    CONSTRAINT phpbb_forums_parent_id_check CHECK ((parent_id >= 0)),
    CONSTRAINT phpbb_forums_prune_days_check CHECK ((prune_days >= 0)),
    CONSTRAINT phpbb_forums_prune_freq_check CHECK ((prune_freq >= 0)),
    CONSTRAINT phpbb_forums_prune_next_check CHECK ((prune_next >= 0)),
    CONSTRAINT phpbb_forums_prune_shadow_days_check CHECK ((prune_shadow_days >= 0)),
    CONSTRAINT phpbb_forums_prune_shadow_freq_check CHECK ((prune_shadow_freq >= 0)),
    CONSTRAINT phpbb_forums_prune_viewed_check CHECK ((prune_viewed >= 0)),
    CONSTRAINT phpbb_forums_right_id_check CHECK ((right_id >= 0))
);


ALTER TABLE public.phpbb_forums OWNER TO phpbb;

--
-- Name: phpbb_forums_access; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_forums_access (
    forum_id integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    session_id character(32) DEFAULT ''::bpchar NOT NULL,
    CONSTRAINT phpbb_forums_access_forum_id_check CHECK ((forum_id >= 0)),
    CONSTRAINT phpbb_forums_access_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_forums_access OWNER TO phpbb;

--
-- Name: phpbb_forums_track; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_forums_track (
    user_id integer DEFAULT 0 NOT NULL,
    forum_id integer DEFAULT 0 NOT NULL,
    mark_time integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_forums_track_forum_id_check CHECK ((forum_id >= 0)),
    CONSTRAINT phpbb_forums_track_mark_time_check CHECK ((mark_time >= 0)),
    CONSTRAINT phpbb_forums_track_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_forums_track OWNER TO phpbb;

--
-- Name: phpbb_forums_watch; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_forums_watch (
    forum_id integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    notify_status smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_forums_watch_forum_id_check CHECK ((forum_id >= 0)),
    CONSTRAINT phpbb_forums_watch_notify_status_check CHECK ((notify_status >= 0)),
    CONSTRAINT phpbb_forums_watch_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_forums_watch OWNER TO phpbb;

--
-- Name: phpbb_groups_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_groups_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_groups_seq OWNER TO phpbb;

--
-- Name: phpbb_groups; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_groups (
    group_id integer DEFAULT nextval('public.phpbb_groups_seq'::regclass) NOT NULL,
    group_type smallint DEFAULT '1'::smallint NOT NULL,
    group_founder_manage smallint DEFAULT '0'::smallint NOT NULL,
    group_skip_auth smallint DEFAULT '0'::smallint NOT NULL,
    group_name public.varchar_ci DEFAULT ''::character varying NOT NULL,
    group_desc character varying(4000) DEFAULT ''::character varying NOT NULL,
    group_desc_bitfield character varying(255) DEFAULT ''::character varying NOT NULL,
    group_desc_options integer DEFAULT 7 NOT NULL,
    group_desc_uid character varying(8) DEFAULT ''::character varying NOT NULL,
    group_display smallint DEFAULT '0'::smallint NOT NULL,
    group_avatar character varying(255) DEFAULT ''::character varying NOT NULL,
    group_avatar_type character varying(255) DEFAULT ''::character varying NOT NULL,
    group_avatar_width smallint DEFAULT '0'::smallint NOT NULL,
    group_avatar_height smallint DEFAULT '0'::smallint NOT NULL,
    group_rank integer DEFAULT 0 NOT NULL,
    group_colour character varying(6) DEFAULT ''::character varying NOT NULL,
    group_sig_chars integer DEFAULT 0 NOT NULL,
    group_receive_pm smallint DEFAULT '0'::smallint NOT NULL,
    group_message_limit integer DEFAULT 0 NOT NULL,
    group_legend integer DEFAULT 0 NOT NULL,
    group_max_recipients integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_groups_group_avatar_height_check CHECK ((group_avatar_height >= 0)),
    CONSTRAINT phpbb_groups_group_avatar_width_check CHECK ((group_avatar_width >= 0)),
    CONSTRAINT phpbb_groups_group_desc_options_check CHECK ((group_desc_options >= 0)),
    CONSTRAINT phpbb_groups_group_display_check CHECK ((group_display >= 0)),
    CONSTRAINT phpbb_groups_group_founder_manage_check CHECK ((group_founder_manage >= 0)),
    CONSTRAINT phpbb_groups_group_id_check CHECK ((group_id >= 0)),
    CONSTRAINT phpbb_groups_group_legend_check CHECK ((group_legend >= 0)),
    CONSTRAINT phpbb_groups_group_max_recipients_check CHECK ((group_max_recipients >= 0)),
    CONSTRAINT phpbb_groups_group_message_limit_check CHECK ((group_message_limit >= 0)),
    CONSTRAINT phpbb_groups_group_rank_check CHECK ((group_rank >= 0)),
    CONSTRAINT phpbb_groups_group_receive_pm_check CHECK ((group_receive_pm >= 0)),
    CONSTRAINT phpbb_groups_group_sig_chars_check CHECK ((group_sig_chars >= 0)),
    CONSTRAINT phpbb_groups_group_skip_auth_check CHECK ((group_skip_auth >= 0))
);


ALTER TABLE public.phpbb_groups OWNER TO phpbb;

--
-- Name: phpbb_icons_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_icons_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_icons_seq OWNER TO phpbb;

--
-- Name: phpbb_icons; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_icons (
    icons_id integer DEFAULT nextval('public.phpbb_icons_seq'::regclass) NOT NULL,
    icons_url character varying(255) DEFAULT ''::character varying NOT NULL,
    icons_width smallint DEFAULT '0'::smallint NOT NULL,
    icons_height smallint DEFAULT '0'::smallint NOT NULL,
    icons_alt character varying(255) DEFAULT ''::character varying NOT NULL,
    icons_order integer DEFAULT 0 NOT NULL,
    display_on_posting smallint DEFAULT '1'::smallint NOT NULL,
    CONSTRAINT phpbb_icons_display_on_posting_check CHECK ((display_on_posting >= 0)),
    CONSTRAINT phpbb_icons_icons_id_check CHECK ((icons_id >= 0)),
    CONSTRAINT phpbb_icons_icons_order_check CHECK ((icons_order >= 0))
);


ALTER TABLE public.phpbb_icons OWNER TO phpbb;

--
-- Name: phpbb_lang_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_lang_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_lang_seq OWNER TO phpbb;

--
-- Name: phpbb_lang; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_lang (
    lang_id smallint DEFAULT nextval('public.phpbb_lang_seq'::regclass) NOT NULL,
    lang_iso character varying(30) DEFAULT ''::character varying NOT NULL,
    lang_dir character varying(30) DEFAULT ''::character varying NOT NULL,
    lang_english_name character varying(100) DEFAULT ''::character varying NOT NULL,
    lang_local_name character varying(255) DEFAULT ''::character varying NOT NULL,
    lang_author character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.phpbb_lang OWNER TO phpbb;

--
-- Name: phpbb_log_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_log_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_log_seq OWNER TO phpbb;

--
-- Name: phpbb_log; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_log (
    log_id integer DEFAULT nextval('public.phpbb_log_seq'::regclass) NOT NULL,
    log_type smallint DEFAULT '0'::smallint NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    forum_id integer DEFAULT 0 NOT NULL,
    topic_id integer DEFAULT 0 NOT NULL,
    post_id integer DEFAULT 0 NOT NULL,
    reportee_id integer DEFAULT 0 NOT NULL,
    log_ip character varying(40) DEFAULT ''::character varying NOT NULL,
    log_time integer DEFAULT 0 NOT NULL,
    log_operation character varying(4000) DEFAULT ''::character varying NOT NULL,
    log_data text DEFAULT ''::text NOT NULL,
    CONSTRAINT phpbb_log_forum_id_check CHECK ((forum_id >= 0)),
    CONSTRAINT phpbb_log_log_id_check CHECK ((log_id >= 0)),
    CONSTRAINT phpbb_log_log_time_check CHECK ((log_time >= 0)),
    CONSTRAINT phpbb_log_post_id_check CHECK ((post_id >= 0)),
    CONSTRAINT phpbb_log_reportee_id_check CHECK ((reportee_id >= 0)),
    CONSTRAINT phpbb_log_topic_id_check CHECK ((topic_id >= 0)),
    CONSTRAINT phpbb_log_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_log OWNER TO phpbb;

--
-- Name: phpbb_login_attempts; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_login_attempts (
    attempt_ip character varying(40) DEFAULT ''::character varying NOT NULL,
    attempt_browser character varying(150) DEFAULT ''::character varying NOT NULL,
    attempt_forwarded_for character varying(255) DEFAULT ''::character varying NOT NULL,
    attempt_time integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    username character varying(255) DEFAULT '0'::character varying NOT NULL,
    username_clean public.varchar_ci DEFAULT '0'::character varying NOT NULL,
    CONSTRAINT phpbb_login_attempts_attempt_time_check CHECK ((attempt_time >= 0)),
    CONSTRAINT phpbb_login_attempts_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_login_attempts OWNER TO phpbb;

--
-- Name: phpbb_migrations; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_migrations (
    migration_name character varying(255) DEFAULT ''::character varying NOT NULL,
    migration_depends_on character varying(8000) DEFAULT ''::character varying NOT NULL,
    migration_schema_done smallint DEFAULT '0'::smallint NOT NULL,
    migration_data_done smallint DEFAULT '0'::smallint NOT NULL,
    migration_data_state character varying(8000) DEFAULT ''::character varying NOT NULL,
    migration_start_time integer DEFAULT 0 NOT NULL,
    migration_end_time integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_migrations_migration_data_done_check CHECK ((migration_data_done >= 0)),
    CONSTRAINT phpbb_migrations_migration_end_time_check CHECK ((migration_end_time >= 0)),
    CONSTRAINT phpbb_migrations_migration_schema_done_check CHECK ((migration_schema_done >= 0)),
    CONSTRAINT phpbb_migrations_migration_start_time_check CHECK ((migration_start_time >= 0))
);


ALTER TABLE public.phpbb_migrations OWNER TO phpbb;

--
-- Name: phpbb_moderator_cache; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_moderator_cache (
    forum_id integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    username character varying(255) DEFAULT ''::character varying NOT NULL,
    group_id integer DEFAULT 0 NOT NULL,
    group_name character varying(255) DEFAULT ''::character varying NOT NULL,
    display_on_index smallint DEFAULT '1'::smallint NOT NULL,
    CONSTRAINT phpbb_moderator_cache_display_on_index_check CHECK ((display_on_index >= 0)),
    CONSTRAINT phpbb_moderator_cache_forum_id_check CHECK ((forum_id >= 0)),
    CONSTRAINT phpbb_moderator_cache_group_id_check CHECK ((group_id >= 0)),
    CONSTRAINT phpbb_moderator_cache_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_moderator_cache OWNER TO phpbb;

--
-- Name: phpbb_modules_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_modules_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_modules_seq OWNER TO phpbb;

--
-- Name: phpbb_modules; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_modules (
    module_id integer DEFAULT nextval('public.phpbb_modules_seq'::regclass) NOT NULL,
    module_enabled smallint DEFAULT '1'::smallint NOT NULL,
    module_display smallint DEFAULT '1'::smallint NOT NULL,
    module_basename character varying(255) DEFAULT ''::character varying NOT NULL,
    module_class character varying(10) DEFAULT ''::character varying NOT NULL,
    parent_id integer DEFAULT 0 NOT NULL,
    left_id integer DEFAULT 0 NOT NULL,
    right_id integer DEFAULT 0 NOT NULL,
    module_langname character varying(255) DEFAULT ''::character varying NOT NULL,
    module_mode character varying(255) DEFAULT ''::character varying NOT NULL,
    module_auth character varying(255) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_modules_left_id_check CHECK ((left_id >= 0)),
    CONSTRAINT phpbb_modules_module_display_check CHECK ((module_display >= 0)),
    CONSTRAINT phpbb_modules_module_enabled_check CHECK ((module_enabled >= 0)),
    CONSTRAINT phpbb_modules_module_id_check CHECK ((module_id >= 0)),
    CONSTRAINT phpbb_modules_parent_id_check CHECK ((parent_id >= 0)),
    CONSTRAINT phpbb_modules_right_id_check CHECK ((right_id >= 0))
);


ALTER TABLE public.phpbb_modules OWNER TO phpbb;

--
-- Name: phpbb_notification_emails; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_notification_emails (
    notification_type_id smallint DEFAULT '0'::smallint NOT NULL,
    item_id integer DEFAULT 0 NOT NULL,
    item_parent_id integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_notification_emails_item_id_check CHECK ((item_id >= 0)),
    CONSTRAINT phpbb_notification_emails_item_parent_id_check CHECK ((item_parent_id >= 0)),
    CONSTRAINT phpbb_notification_emails_notification_type_id_check CHECK ((notification_type_id >= 0)),
    CONSTRAINT phpbb_notification_emails_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_notification_emails OWNER TO phpbb;

--
-- Name: phpbb_notification_types_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_notification_types_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_notification_types_seq OWNER TO phpbb;

--
-- Name: phpbb_notification_types; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_notification_types (
    notification_type_id smallint DEFAULT nextval('public.phpbb_notification_types_seq'::regclass) NOT NULL,
    notification_type_name character varying(255) DEFAULT ''::character varying NOT NULL,
    notification_type_enabled smallint DEFAULT '1'::smallint NOT NULL,
    CONSTRAINT phpbb_notification_types_notification_type_enabled_check CHECK ((notification_type_enabled >= 0)),
    CONSTRAINT phpbb_notification_types_notification_type_id_check CHECK ((notification_type_id >= 0))
);


ALTER TABLE public.phpbb_notification_types OWNER TO phpbb;

--
-- Name: phpbb_notifications_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_notifications_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_notifications_seq OWNER TO phpbb;

--
-- Name: phpbb_notifications; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_notifications (
    notification_id integer DEFAULT nextval('public.phpbb_notifications_seq'::regclass) NOT NULL,
    notification_type_id smallint DEFAULT '0'::smallint NOT NULL,
    item_id integer DEFAULT 0 NOT NULL,
    item_parent_id integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    notification_read smallint DEFAULT '0'::smallint NOT NULL,
    notification_time integer DEFAULT 1 NOT NULL,
    notification_data character varying(4000) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_notifications_item_id_check CHECK ((item_id >= 0)),
    CONSTRAINT phpbb_notifications_item_parent_id_check CHECK ((item_parent_id >= 0)),
    CONSTRAINT phpbb_notifications_notification_id_check CHECK ((notification_id >= 0)),
    CONSTRAINT phpbb_notifications_notification_read_check CHECK ((notification_read >= 0)),
    CONSTRAINT phpbb_notifications_notification_time_check CHECK ((notification_time >= 0)),
    CONSTRAINT phpbb_notifications_notification_type_id_check CHECK ((notification_type_id >= 0)),
    CONSTRAINT phpbb_notifications_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_notifications OWNER TO phpbb;

--
-- Name: phpbb_oauth_accounts; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_oauth_accounts (
    user_id integer DEFAULT 0 NOT NULL,
    provider character varying(255) DEFAULT ''::character varying NOT NULL,
    oauth_provider_id character varying(4000) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_oauth_accounts_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_oauth_accounts OWNER TO phpbb;

--
-- Name: phpbb_oauth_states; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_oauth_states (
    user_id integer DEFAULT 0 NOT NULL,
    session_id character(32) DEFAULT ''::bpchar NOT NULL,
    provider character varying(255) DEFAULT ''::character varying NOT NULL,
    oauth_state character varying(255) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_oauth_states_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_oauth_states OWNER TO phpbb;

--
-- Name: phpbb_oauth_tokens; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_oauth_tokens (
    user_id integer DEFAULT 0 NOT NULL,
    session_id character(32) DEFAULT ''::bpchar NOT NULL,
    provider character varying(255) DEFAULT ''::character varying NOT NULL,
    oauth_token text DEFAULT ''::text NOT NULL,
    CONSTRAINT phpbb_oauth_tokens_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_oauth_tokens OWNER TO phpbb;

--
-- Name: phpbb_poll_options; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_poll_options (
    poll_option_id smallint DEFAULT '0'::smallint NOT NULL,
    topic_id integer DEFAULT 0 NOT NULL,
    poll_option_text character varying(4000) DEFAULT ''::character varying NOT NULL,
    poll_option_total integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_poll_options_poll_option_total_check CHECK ((poll_option_total >= 0)),
    CONSTRAINT phpbb_poll_options_topic_id_check CHECK ((topic_id >= 0))
);


ALTER TABLE public.phpbb_poll_options OWNER TO phpbb;

--
-- Name: phpbb_poll_votes; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_poll_votes (
    topic_id integer DEFAULT 0 NOT NULL,
    poll_option_id smallint DEFAULT '0'::smallint NOT NULL,
    vote_user_id integer DEFAULT 0 NOT NULL,
    vote_user_ip character varying(40) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_poll_votes_topic_id_check CHECK ((topic_id >= 0)),
    CONSTRAINT phpbb_poll_votes_vote_user_id_check CHECK ((vote_user_id >= 0))
);


ALTER TABLE public.phpbb_poll_votes OWNER TO phpbb;

--
-- Name: phpbb_posts_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_posts_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_posts_seq OWNER TO phpbb;

--
-- Name: phpbb_posts; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_posts (
    post_id integer DEFAULT nextval('public.phpbb_posts_seq'::regclass) NOT NULL,
    topic_id integer DEFAULT 0 NOT NULL,
    forum_id integer DEFAULT 0 NOT NULL,
    poster_id integer DEFAULT 0 NOT NULL,
    icon_id integer DEFAULT 0 NOT NULL,
    poster_ip character varying(40) DEFAULT ''::character varying NOT NULL,
    post_time integer DEFAULT 0 NOT NULL,
    post_reported smallint DEFAULT '0'::smallint NOT NULL,
    enable_bbcode smallint DEFAULT '1'::smallint NOT NULL,
    enable_smilies smallint DEFAULT '1'::smallint NOT NULL,
    enable_magic_url smallint DEFAULT '1'::smallint NOT NULL,
    enable_sig smallint DEFAULT '1'::smallint NOT NULL,
    post_username character varying(255) DEFAULT ''::character varying NOT NULL,
    post_subject character varying(255) DEFAULT ''::character varying NOT NULL,
    post_text text DEFAULT ''::text NOT NULL,
    post_checksum character varying(32) DEFAULT ''::character varying NOT NULL,
    post_attachment smallint DEFAULT '0'::smallint NOT NULL,
    bbcode_bitfield character varying(255) DEFAULT ''::character varying NOT NULL,
    bbcode_uid character varying(8) DEFAULT ''::character varying NOT NULL,
    post_postcount smallint DEFAULT '1'::smallint NOT NULL,
    post_edit_time integer DEFAULT 0 NOT NULL,
    post_edit_reason character varying(255) DEFAULT ''::character varying NOT NULL,
    post_edit_user integer DEFAULT 0 NOT NULL,
    post_edit_count smallint DEFAULT '0'::smallint NOT NULL,
    post_edit_locked smallint DEFAULT '0'::smallint NOT NULL,
    post_visibility smallint DEFAULT '0'::smallint NOT NULL,
    post_delete_time integer DEFAULT 0 NOT NULL,
    post_delete_reason character varying(255) DEFAULT ''::character varying NOT NULL,
    post_delete_user integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_posts_enable_bbcode_check CHECK ((enable_bbcode >= 0)),
    CONSTRAINT phpbb_posts_enable_magic_url_check CHECK ((enable_magic_url >= 0)),
    CONSTRAINT phpbb_posts_enable_sig_check CHECK ((enable_sig >= 0)),
    CONSTRAINT phpbb_posts_enable_smilies_check CHECK ((enable_smilies >= 0)),
    CONSTRAINT phpbb_posts_forum_id_check CHECK ((forum_id >= 0)),
    CONSTRAINT phpbb_posts_icon_id_check CHECK ((icon_id >= 0)),
    CONSTRAINT phpbb_posts_post_attachment_check CHECK ((post_attachment >= 0)),
    CONSTRAINT phpbb_posts_post_delete_time_check CHECK ((post_delete_time >= 0)),
    CONSTRAINT phpbb_posts_post_delete_user_check CHECK ((post_delete_user >= 0)),
    CONSTRAINT phpbb_posts_post_edit_count_check CHECK ((post_edit_count >= 0)),
    CONSTRAINT phpbb_posts_post_edit_locked_check CHECK ((post_edit_locked >= 0)),
    CONSTRAINT phpbb_posts_post_edit_time_check CHECK ((post_edit_time >= 0)),
    CONSTRAINT phpbb_posts_post_edit_user_check CHECK ((post_edit_user >= 0)),
    CONSTRAINT phpbb_posts_post_id_check CHECK ((post_id >= 0)),
    CONSTRAINT phpbb_posts_post_postcount_check CHECK ((post_postcount >= 0)),
    CONSTRAINT phpbb_posts_post_reported_check CHECK ((post_reported >= 0)),
    CONSTRAINT phpbb_posts_post_time_check CHECK ((post_time >= 0)),
    CONSTRAINT phpbb_posts_poster_id_check CHECK ((poster_id >= 0)),
    CONSTRAINT phpbb_posts_topic_id_check CHECK ((topic_id >= 0))
);


ALTER TABLE public.phpbb_posts OWNER TO phpbb;

--
-- Name: phpbb_privmsgs_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_privmsgs_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_privmsgs_seq OWNER TO phpbb;

--
-- Name: phpbb_privmsgs; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_privmsgs (
    msg_id integer DEFAULT nextval('public.phpbb_privmsgs_seq'::regclass) NOT NULL,
    root_level integer DEFAULT 0 NOT NULL,
    author_id integer DEFAULT 0 NOT NULL,
    icon_id integer DEFAULT 0 NOT NULL,
    author_ip character varying(40) DEFAULT ''::character varying NOT NULL,
    message_time integer DEFAULT 0 NOT NULL,
    enable_bbcode smallint DEFAULT '1'::smallint NOT NULL,
    enable_smilies smallint DEFAULT '1'::smallint NOT NULL,
    enable_magic_url smallint DEFAULT '1'::smallint NOT NULL,
    enable_sig smallint DEFAULT '1'::smallint NOT NULL,
    message_subject character varying(255) DEFAULT ''::character varying NOT NULL,
    message_text text DEFAULT ''::text NOT NULL,
    message_edit_reason character varying(255) DEFAULT ''::character varying NOT NULL,
    message_edit_user integer DEFAULT 0 NOT NULL,
    message_attachment smallint DEFAULT '0'::smallint NOT NULL,
    bbcode_bitfield character varying(255) DEFAULT ''::character varying NOT NULL,
    bbcode_uid character varying(8) DEFAULT ''::character varying NOT NULL,
    message_edit_time integer DEFAULT 0 NOT NULL,
    message_edit_count smallint DEFAULT '0'::smallint NOT NULL,
    to_address character varying(4000) DEFAULT ''::character varying NOT NULL,
    bcc_address character varying(4000) DEFAULT ''::character varying NOT NULL,
    message_reported smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_privmsgs_author_id_check CHECK ((author_id >= 0)),
    CONSTRAINT phpbb_privmsgs_enable_bbcode_check CHECK ((enable_bbcode >= 0)),
    CONSTRAINT phpbb_privmsgs_enable_magic_url_check CHECK ((enable_magic_url >= 0)),
    CONSTRAINT phpbb_privmsgs_enable_sig_check CHECK ((enable_sig >= 0)),
    CONSTRAINT phpbb_privmsgs_enable_smilies_check CHECK ((enable_smilies >= 0)),
    CONSTRAINT phpbb_privmsgs_icon_id_check CHECK ((icon_id >= 0)),
    CONSTRAINT phpbb_privmsgs_message_attachment_check CHECK ((message_attachment >= 0)),
    CONSTRAINT phpbb_privmsgs_message_edit_count_check CHECK ((message_edit_count >= 0)),
    CONSTRAINT phpbb_privmsgs_message_edit_time_check CHECK ((message_edit_time >= 0)),
    CONSTRAINT phpbb_privmsgs_message_edit_user_check CHECK ((message_edit_user >= 0)),
    CONSTRAINT phpbb_privmsgs_message_reported_check CHECK ((message_reported >= 0)),
    CONSTRAINT phpbb_privmsgs_message_time_check CHECK ((message_time >= 0)),
    CONSTRAINT phpbb_privmsgs_msg_id_check CHECK ((msg_id >= 0)),
    CONSTRAINT phpbb_privmsgs_root_level_check CHECK ((root_level >= 0))
);


ALTER TABLE public.phpbb_privmsgs OWNER TO phpbb;

--
-- Name: phpbb_privmsgs_folder_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_privmsgs_folder_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_privmsgs_folder_seq OWNER TO phpbb;

--
-- Name: phpbb_privmsgs_folder; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_privmsgs_folder (
    folder_id integer DEFAULT nextval('public.phpbb_privmsgs_folder_seq'::regclass) NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    folder_name character varying(255) DEFAULT ''::character varying NOT NULL,
    pm_count integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_privmsgs_folder_folder_id_check CHECK ((folder_id >= 0)),
    CONSTRAINT phpbb_privmsgs_folder_pm_count_check CHECK ((pm_count >= 0)),
    CONSTRAINT phpbb_privmsgs_folder_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_privmsgs_folder OWNER TO phpbb;

--
-- Name: phpbb_privmsgs_rules_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_privmsgs_rules_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_privmsgs_rules_seq OWNER TO phpbb;

--
-- Name: phpbb_privmsgs_rules; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_privmsgs_rules (
    rule_id integer DEFAULT nextval('public.phpbb_privmsgs_rules_seq'::regclass) NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    rule_check integer DEFAULT 0 NOT NULL,
    rule_connection integer DEFAULT 0 NOT NULL,
    rule_string character varying(255) DEFAULT ''::character varying NOT NULL,
    rule_user_id integer DEFAULT 0 NOT NULL,
    rule_group_id integer DEFAULT 0 NOT NULL,
    rule_action integer DEFAULT 0 NOT NULL,
    rule_folder_id integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_privmsgs_rules_rule_action_check CHECK ((rule_action >= 0)),
    CONSTRAINT phpbb_privmsgs_rules_rule_check_check CHECK ((rule_check >= 0)),
    CONSTRAINT phpbb_privmsgs_rules_rule_connection_check CHECK ((rule_connection >= 0)),
    CONSTRAINT phpbb_privmsgs_rules_rule_group_id_check CHECK ((rule_group_id >= 0)),
    CONSTRAINT phpbb_privmsgs_rules_rule_id_check CHECK ((rule_id >= 0)),
    CONSTRAINT phpbb_privmsgs_rules_rule_user_id_check CHECK ((rule_user_id >= 0)),
    CONSTRAINT phpbb_privmsgs_rules_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_privmsgs_rules OWNER TO phpbb;

--
-- Name: phpbb_privmsgs_to; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_privmsgs_to (
    msg_id integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    author_id integer DEFAULT 0 NOT NULL,
    pm_deleted smallint DEFAULT '0'::smallint NOT NULL,
    pm_new smallint DEFAULT '1'::smallint NOT NULL,
    pm_unread smallint DEFAULT '1'::smallint NOT NULL,
    pm_replied smallint DEFAULT '0'::smallint NOT NULL,
    pm_marked smallint DEFAULT '0'::smallint NOT NULL,
    pm_forwarded smallint DEFAULT '0'::smallint NOT NULL,
    folder_id integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_privmsgs_to_author_id_check CHECK ((author_id >= 0)),
    CONSTRAINT phpbb_privmsgs_to_msg_id_check CHECK ((msg_id >= 0)),
    CONSTRAINT phpbb_privmsgs_to_pm_deleted_check CHECK ((pm_deleted >= 0)),
    CONSTRAINT phpbb_privmsgs_to_pm_forwarded_check CHECK ((pm_forwarded >= 0)),
    CONSTRAINT phpbb_privmsgs_to_pm_marked_check CHECK ((pm_marked >= 0)),
    CONSTRAINT phpbb_privmsgs_to_pm_new_check CHECK ((pm_new >= 0)),
    CONSTRAINT phpbb_privmsgs_to_pm_replied_check CHECK ((pm_replied >= 0)),
    CONSTRAINT phpbb_privmsgs_to_pm_unread_check CHECK ((pm_unread >= 0)),
    CONSTRAINT phpbb_privmsgs_to_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_privmsgs_to OWNER TO phpbb;

--
-- Name: phpbb_profile_fields_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_profile_fields_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_profile_fields_seq OWNER TO phpbb;

--
-- Name: phpbb_profile_fields; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_profile_fields (
    field_id integer DEFAULT nextval('public.phpbb_profile_fields_seq'::regclass) NOT NULL,
    field_name character varying(255) DEFAULT ''::character varying NOT NULL,
    field_type character varying(100) DEFAULT ''::character varying NOT NULL,
    field_ident character varying(20) DEFAULT ''::character varying NOT NULL,
    field_length character varying(20) DEFAULT ''::character varying NOT NULL,
    field_minlen character varying(255) DEFAULT ''::character varying NOT NULL,
    field_maxlen character varying(255) DEFAULT ''::character varying NOT NULL,
    field_novalue character varying(255) DEFAULT ''::character varying NOT NULL,
    field_default_value character varying(255) DEFAULT ''::character varying NOT NULL,
    field_validation character varying(64) DEFAULT ''::character varying NOT NULL,
    field_required smallint DEFAULT '0'::smallint NOT NULL,
    field_show_on_reg smallint DEFAULT '0'::smallint NOT NULL,
    field_hide smallint DEFAULT '0'::smallint NOT NULL,
    field_no_view smallint DEFAULT '0'::smallint NOT NULL,
    field_active smallint DEFAULT '0'::smallint NOT NULL,
    field_order integer DEFAULT 0 NOT NULL,
    field_show_profile smallint DEFAULT '0'::smallint NOT NULL,
    field_show_on_vt smallint DEFAULT '0'::smallint NOT NULL,
    field_show_novalue smallint DEFAULT '0'::smallint NOT NULL,
    field_show_on_pm smallint DEFAULT '0'::smallint NOT NULL,
    field_show_on_ml smallint DEFAULT '0'::smallint NOT NULL,
    field_is_contact smallint DEFAULT '0'::smallint NOT NULL,
    field_contact_desc character varying(255) DEFAULT ''::character varying NOT NULL,
    field_contact_url character varying(255) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_profile_fields_field_active_check CHECK ((field_active >= 0)),
    CONSTRAINT phpbb_profile_fields_field_hide_check CHECK ((field_hide >= 0)),
    CONSTRAINT phpbb_profile_fields_field_id_check CHECK ((field_id >= 0)),
    CONSTRAINT phpbb_profile_fields_field_is_contact_check CHECK ((field_is_contact >= 0)),
    CONSTRAINT phpbb_profile_fields_field_no_view_check CHECK ((field_no_view >= 0)),
    CONSTRAINT phpbb_profile_fields_field_order_check CHECK ((field_order >= 0)),
    CONSTRAINT phpbb_profile_fields_field_required_check CHECK ((field_required >= 0)),
    CONSTRAINT phpbb_profile_fields_field_show_novalue_check CHECK ((field_show_novalue >= 0)),
    CONSTRAINT phpbb_profile_fields_field_show_on_ml_check CHECK ((field_show_on_ml >= 0)),
    CONSTRAINT phpbb_profile_fields_field_show_on_pm_check CHECK ((field_show_on_pm >= 0)),
    CONSTRAINT phpbb_profile_fields_field_show_on_reg_check CHECK ((field_show_on_reg >= 0)),
    CONSTRAINT phpbb_profile_fields_field_show_on_vt_check CHECK ((field_show_on_vt >= 0)),
    CONSTRAINT phpbb_profile_fields_field_show_profile_check CHECK ((field_show_profile >= 0))
);


ALTER TABLE public.phpbb_profile_fields OWNER TO phpbb;

--
-- Name: phpbb_profile_fields_data; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_profile_fields_data (
    user_id integer DEFAULT 0 NOT NULL,
    pf_phpbb_interests text DEFAULT ''::text NOT NULL,
    pf_phpbb_occupation text DEFAULT ''::text NOT NULL,
    pf_phpbb_location character varying(255) DEFAULT ''::character varying NOT NULL,
    pf_phpbb_youtube character varying(255) DEFAULT ''::character varying NOT NULL,
    pf_phpbb_facebook character varying(255) DEFAULT ''::character varying NOT NULL,
    pf_phpbb_icq character varying(255) DEFAULT ''::character varying NOT NULL,
    pf_phpbb_skype character varying(255) DEFAULT ''::character varying NOT NULL,
    pf_phpbb_twitter character varying(255) DEFAULT ''::character varying NOT NULL,
    pf_phpbb_website character varying(255) DEFAULT ''::character varying NOT NULL,
    pf_phpbb_yahoo character varying(255) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_profile_fields_data_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_profile_fields_data OWNER TO phpbb;

--
-- Name: phpbb_profile_fields_lang; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_profile_fields_lang (
    field_id integer DEFAULT 0 NOT NULL,
    lang_id integer DEFAULT 0 NOT NULL,
    option_id integer DEFAULT 0 NOT NULL,
    field_type character varying(100) DEFAULT ''::character varying NOT NULL,
    lang_value character varying(255) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_profile_fields_lang_field_id_check CHECK ((field_id >= 0)),
    CONSTRAINT phpbb_profile_fields_lang_lang_id_check CHECK ((lang_id >= 0)),
    CONSTRAINT phpbb_profile_fields_lang_option_id_check CHECK ((option_id >= 0))
);


ALTER TABLE public.phpbb_profile_fields_lang OWNER TO phpbb;

--
-- Name: phpbb_profile_lang; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_profile_lang (
    field_id integer DEFAULT 0 NOT NULL,
    lang_id integer DEFAULT 0 NOT NULL,
    lang_name character varying(255) DEFAULT ''::character varying NOT NULL,
    lang_explain character varying(4000) DEFAULT ''::character varying NOT NULL,
    lang_default_value character varying(255) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_profile_lang_field_id_check CHECK ((field_id >= 0)),
    CONSTRAINT phpbb_profile_lang_lang_id_check CHECK ((lang_id >= 0))
);


ALTER TABLE public.phpbb_profile_lang OWNER TO phpbb;

--
-- Name: phpbb_ranks_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_ranks_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_ranks_seq OWNER TO phpbb;

--
-- Name: phpbb_ranks; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_ranks (
    rank_id integer DEFAULT nextval('public.phpbb_ranks_seq'::regclass) NOT NULL,
    rank_title character varying(255) DEFAULT ''::character varying NOT NULL,
    rank_min integer DEFAULT 0 NOT NULL,
    rank_special smallint DEFAULT '0'::smallint NOT NULL,
    rank_image character varying(255) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_ranks_rank_id_check CHECK ((rank_id >= 0)),
    CONSTRAINT phpbb_ranks_rank_min_check CHECK ((rank_min >= 0)),
    CONSTRAINT phpbb_ranks_rank_special_check CHECK ((rank_special >= 0))
);


ALTER TABLE public.phpbb_ranks OWNER TO phpbb;

--
-- Name: phpbb_reports_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_reports_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_reports_seq OWNER TO phpbb;

--
-- Name: phpbb_reports; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_reports (
    report_id integer DEFAULT nextval('public.phpbb_reports_seq'::regclass) NOT NULL,
    reason_id smallint DEFAULT '0'::smallint NOT NULL,
    post_id integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    user_notify smallint DEFAULT '0'::smallint NOT NULL,
    report_closed smallint DEFAULT '0'::smallint NOT NULL,
    report_time integer DEFAULT 0 NOT NULL,
    report_text text DEFAULT ''::text NOT NULL,
    pm_id integer DEFAULT 0 NOT NULL,
    reported_post_enable_bbcode smallint DEFAULT '1'::smallint NOT NULL,
    reported_post_enable_smilies smallint DEFAULT '1'::smallint NOT NULL,
    reported_post_enable_magic_url smallint DEFAULT '1'::smallint NOT NULL,
    reported_post_text text DEFAULT ''::text NOT NULL,
    reported_post_uid character varying(8) DEFAULT ''::character varying NOT NULL,
    reported_post_bitfield character varying(255) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_reports_pm_id_check CHECK ((pm_id >= 0)),
    CONSTRAINT phpbb_reports_post_id_check CHECK ((post_id >= 0)),
    CONSTRAINT phpbb_reports_reason_id_check CHECK ((reason_id >= 0)),
    CONSTRAINT phpbb_reports_report_closed_check CHECK ((report_closed >= 0)),
    CONSTRAINT phpbb_reports_report_id_check CHECK ((report_id >= 0)),
    CONSTRAINT phpbb_reports_report_time_check CHECK ((report_time >= 0)),
    CONSTRAINT phpbb_reports_reported_post_enable_bbcode_check CHECK ((reported_post_enable_bbcode >= 0)),
    CONSTRAINT phpbb_reports_reported_post_enable_magic_url_check CHECK ((reported_post_enable_magic_url >= 0)),
    CONSTRAINT phpbb_reports_reported_post_enable_smilies_check CHECK ((reported_post_enable_smilies >= 0)),
    CONSTRAINT phpbb_reports_user_id_check CHECK ((user_id >= 0)),
    CONSTRAINT phpbb_reports_user_notify_check CHECK ((user_notify >= 0))
);


ALTER TABLE public.phpbb_reports OWNER TO phpbb;

--
-- Name: phpbb_reports_reasons_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_reports_reasons_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_reports_reasons_seq OWNER TO phpbb;

--
-- Name: phpbb_reports_reasons; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_reports_reasons (
    reason_id smallint DEFAULT nextval('public.phpbb_reports_reasons_seq'::regclass) NOT NULL,
    reason_title character varying(255) DEFAULT ''::character varying NOT NULL,
    reason_description text DEFAULT ''::text NOT NULL,
    reason_order smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_reports_reasons_reason_id_check CHECK ((reason_id >= 0)),
    CONSTRAINT phpbb_reports_reasons_reason_order_check CHECK ((reason_order >= 0))
);


ALTER TABLE public.phpbb_reports_reasons OWNER TO phpbb;

--
-- Name: phpbb_search_results; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_search_results (
    search_key character varying(32) DEFAULT ''::character varying NOT NULL,
    search_time integer DEFAULT 0 NOT NULL,
    search_keywords text DEFAULT ''::text NOT NULL,
    search_authors text DEFAULT ''::text NOT NULL,
    CONSTRAINT phpbb_search_results_search_time_check CHECK ((search_time >= 0))
);


ALTER TABLE public.phpbb_search_results OWNER TO phpbb;

--
-- Name: phpbb_search_wordlist_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_search_wordlist_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_search_wordlist_seq OWNER TO phpbb;

--
-- Name: phpbb_search_wordlist; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_search_wordlist (
    word_id integer DEFAULT nextval('public.phpbb_search_wordlist_seq'::regclass) NOT NULL,
    word_text character varying(255) DEFAULT ''::character varying NOT NULL,
    word_common smallint DEFAULT '0'::smallint NOT NULL,
    word_count integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_search_wordlist_word_common_check CHECK ((word_common >= 0)),
    CONSTRAINT phpbb_search_wordlist_word_count_check CHECK ((word_count >= 0)),
    CONSTRAINT phpbb_search_wordlist_word_id_check CHECK ((word_id >= 0))
);


ALTER TABLE public.phpbb_search_wordlist OWNER TO phpbb;

--
-- Name: phpbb_search_wordmatch; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_search_wordmatch (
    post_id integer DEFAULT 0 NOT NULL,
    word_id integer DEFAULT 0 NOT NULL,
    title_match smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_search_wordmatch_post_id_check CHECK ((post_id >= 0)),
    CONSTRAINT phpbb_search_wordmatch_title_match_check CHECK ((title_match >= 0)),
    CONSTRAINT phpbb_search_wordmatch_word_id_check CHECK ((word_id >= 0))
);


ALTER TABLE public.phpbb_search_wordmatch OWNER TO phpbb;

--
-- Name: phpbb_sessions; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_sessions (
    session_id character(32) DEFAULT ''::bpchar NOT NULL,
    session_user_id integer DEFAULT 0 NOT NULL,
    session_last_visit integer DEFAULT 0 NOT NULL,
    session_start integer DEFAULT 0 NOT NULL,
    session_time integer DEFAULT 0 NOT NULL,
    session_ip character varying(40) DEFAULT ''::character varying NOT NULL,
    session_browser character varying(150) DEFAULT ''::character varying NOT NULL,
    session_forwarded_for character varying(255) DEFAULT ''::character varying NOT NULL,
    session_page character varying(255) DEFAULT ''::character varying NOT NULL,
    session_viewonline smallint DEFAULT '1'::smallint NOT NULL,
    session_autologin smallint DEFAULT '0'::smallint NOT NULL,
    session_admin smallint DEFAULT '0'::smallint NOT NULL,
    session_forum_id integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_sessions_session_admin_check CHECK ((session_admin >= 0)),
    CONSTRAINT phpbb_sessions_session_autologin_check CHECK ((session_autologin >= 0)),
    CONSTRAINT phpbb_sessions_session_forum_id_check CHECK ((session_forum_id >= 0)),
    CONSTRAINT phpbb_sessions_session_last_visit_check CHECK ((session_last_visit >= 0)),
    CONSTRAINT phpbb_sessions_session_start_check CHECK ((session_start >= 0)),
    CONSTRAINT phpbb_sessions_session_time_check CHECK ((session_time >= 0)),
    CONSTRAINT phpbb_sessions_session_user_id_check CHECK ((session_user_id >= 0)),
    CONSTRAINT phpbb_sessions_session_viewonline_check CHECK ((session_viewonline >= 0))
);


ALTER TABLE public.phpbb_sessions OWNER TO phpbb;

--
-- Name: phpbb_sessions_keys; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_sessions_keys (
    key_id character(32) DEFAULT ''::bpchar NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    last_ip character varying(40) DEFAULT ''::character varying NOT NULL,
    last_login integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_sessions_keys_last_login_check CHECK ((last_login >= 0)),
    CONSTRAINT phpbb_sessions_keys_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_sessions_keys OWNER TO phpbb;

--
-- Name: phpbb_sitelist_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_sitelist_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_sitelist_seq OWNER TO phpbb;

--
-- Name: phpbb_sitelist; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_sitelist (
    site_id integer DEFAULT nextval('public.phpbb_sitelist_seq'::regclass) NOT NULL,
    site_ip character varying(40) DEFAULT ''::character varying NOT NULL,
    site_hostname character varying(255) DEFAULT ''::character varying NOT NULL,
    ip_exclude smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_sitelist_ip_exclude_check CHECK ((ip_exclude >= 0)),
    CONSTRAINT phpbb_sitelist_site_id_check CHECK ((site_id >= 0))
);


ALTER TABLE public.phpbb_sitelist OWNER TO phpbb;

--
-- Name: phpbb_smilies_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_smilies_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_smilies_seq OWNER TO phpbb;

--
-- Name: phpbb_smilies; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_smilies (
    smiley_id integer DEFAULT nextval('public.phpbb_smilies_seq'::regclass) NOT NULL,
    code character varying(50) DEFAULT ''::character varying NOT NULL,
    emotion character varying(255) DEFAULT ''::character varying NOT NULL,
    smiley_url character varying(50) DEFAULT ''::character varying NOT NULL,
    smiley_width smallint DEFAULT '0'::smallint NOT NULL,
    smiley_height smallint DEFAULT '0'::smallint NOT NULL,
    smiley_order integer DEFAULT 0 NOT NULL,
    display_on_posting smallint DEFAULT '1'::smallint NOT NULL,
    CONSTRAINT phpbb_smilies_display_on_posting_check CHECK ((display_on_posting >= 0)),
    CONSTRAINT phpbb_smilies_smiley_height_check CHECK ((smiley_height >= 0)),
    CONSTRAINT phpbb_smilies_smiley_id_check CHECK ((smiley_id >= 0)),
    CONSTRAINT phpbb_smilies_smiley_order_check CHECK ((smiley_order >= 0)),
    CONSTRAINT phpbb_smilies_smiley_width_check CHECK ((smiley_width >= 0))
);


ALTER TABLE public.phpbb_smilies OWNER TO phpbb;

--
-- Name: phpbb_styles_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_styles_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_styles_seq OWNER TO phpbb;

--
-- Name: phpbb_styles; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_styles (
    style_id integer DEFAULT nextval('public.phpbb_styles_seq'::regclass) NOT NULL,
    style_name character varying(255) DEFAULT ''::character varying NOT NULL,
    style_copyright character varying(255) DEFAULT ''::character varying NOT NULL,
    style_active smallint DEFAULT '1'::smallint NOT NULL,
    style_path character varying(100) DEFAULT ''::character varying NOT NULL,
    bbcode_bitfield character varying(255) DEFAULT 'kNg='::character varying NOT NULL,
    style_parent_id integer DEFAULT 0 NOT NULL,
    style_parent_tree character varying(8000) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_styles_style_active_check CHECK ((style_active >= 0)),
    CONSTRAINT phpbb_styles_style_id_check CHECK ((style_id >= 0)),
    CONSTRAINT phpbb_styles_style_parent_id_check CHECK ((style_parent_id >= 0))
);


ALTER TABLE public.phpbb_styles OWNER TO phpbb;

--
-- Name: phpbb_teampage_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_teampage_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_teampage_seq OWNER TO phpbb;

--
-- Name: phpbb_teampage; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_teampage (
    teampage_id integer DEFAULT nextval('public.phpbb_teampage_seq'::regclass) NOT NULL,
    group_id integer DEFAULT 0 NOT NULL,
    teampage_name character varying(255) DEFAULT ''::character varying NOT NULL,
    teampage_position integer DEFAULT 0 NOT NULL,
    teampage_parent integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_teampage_group_id_check CHECK ((group_id >= 0)),
    CONSTRAINT phpbb_teampage_teampage_id_check CHECK ((teampage_id >= 0)),
    CONSTRAINT phpbb_teampage_teampage_parent_check CHECK ((teampage_parent >= 0)),
    CONSTRAINT phpbb_teampage_teampage_position_check CHECK ((teampage_position >= 0))
);


ALTER TABLE public.phpbb_teampage OWNER TO phpbb;

--
-- Name: phpbb_topics_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_topics_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_topics_seq OWNER TO phpbb;

--
-- Name: phpbb_topics; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_topics (
    topic_id integer DEFAULT nextval('public.phpbb_topics_seq'::regclass) NOT NULL,
    forum_id integer DEFAULT 0 NOT NULL,
    icon_id integer DEFAULT 0 NOT NULL,
    topic_attachment smallint DEFAULT '0'::smallint NOT NULL,
    topic_reported smallint DEFAULT '0'::smallint NOT NULL,
    topic_title character varying(255) DEFAULT ''::character varying NOT NULL,
    topic_poster integer DEFAULT 0 NOT NULL,
    topic_time integer DEFAULT 0 NOT NULL,
    topic_time_limit integer DEFAULT 0 NOT NULL,
    topic_views integer DEFAULT 0 NOT NULL,
    topic_status smallint DEFAULT '0'::smallint NOT NULL,
    topic_type smallint DEFAULT '0'::smallint NOT NULL,
    topic_first_post_id integer DEFAULT 0 NOT NULL,
    topic_first_poster_name character varying(255) DEFAULT ''::character varying NOT NULL,
    topic_first_poster_colour character varying(6) DEFAULT ''::character varying NOT NULL,
    topic_last_post_id integer DEFAULT 0 NOT NULL,
    topic_last_poster_id integer DEFAULT 0 NOT NULL,
    topic_last_poster_name character varying(255) DEFAULT ''::character varying NOT NULL,
    topic_last_poster_colour character varying(6) DEFAULT ''::character varying NOT NULL,
    topic_last_post_subject character varying(255) DEFAULT ''::character varying NOT NULL,
    topic_last_post_time integer DEFAULT 0 NOT NULL,
    topic_last_view_time integer DEFAULT 0 NOT NULL,
    topic_moved_id integer DEFAULT 0 NOT NULL,
    topic_bumped smallint DEFAULT '0'::smallint NOT NULL,
    topic_bumper integer DEFAULT 0 NOT NULL,
    poll_title character varying(255) DEFAULT ''::character varying NOT NULL,
    poll_start integer DEFAULT 0 NOT NULL,
    poll_length integer DEFAULT 0 NOT NULL,
    poll_max_options smallint DEFAULT '1'::smallint NOT NULL,
    poll_last_vote integer DEFAULT 0 NOT NULL,
    poll_vote_change smallint DEFAULT '0'::smallint NOT NULL,
    topic_visibility smallint DEFAULT '0'::smallint NOT NULL,
    topic_delete_time integer DEFAULT 0 NOT NULL,
    topic_delete_reason character varying(255) DEFAULT ''::character varying NOT NULL,
    topic_delete_user integer DEFAULT 0 NOT NULL,
    topic_posts_approved integer DEFAULT 0 NOT NULL,
    topic_posts_unapproved integer DEFAULT 0 NOT NULL,
    topic_posts_softdeleted integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_topics_forum_id_check CHECK ((forum_id >= 0)),
    CONSTRAINT phpbb_topics_icon_id_check CHECK ((icon_id >= 0)),
    CONSTRAINT phpbb_topics_poll_last_vote_check CHECK ((poll_last_vote >= 0)),
    CONSTRAINT phpbb_topics_poll_length_check CHECK ((poll_length >= 0)),
    CONSTRAINT phpbb_topics_poll_start_check CHECK ((poll_start >= 0)),
    CONSTRAINT phpbb_topics_poll_vote_change_check CHECK ((poll_vote_change >= 0)),
    CONSTRAINT phpbb_topics_topic_attachment_check CHECK ((topic_attachment >= 0)),
    CONSTRAINT phpbb_topics_topic_bumped_check CHECK ((topic_bumped >= 0)),
    CONSTRAINT phpbb_topics_topic_bumper_check CHECK ((topic_bumper >= 0)),
    CONSTRAINT phpbb_topics_topic_delete_time_check CHECK ((topic_delete_time >= 0)),
    CONSTRAINT phpbb_topics_topic_delete_user_check CHECK ((topic_delete_user >= 0)),
    CONSTRAINT phpbb_topics_topic_first_post_id_check CHECK ((topic_first_post_id >= 0)),
    CONSTRAINT phpbb_topics_topic_id_check CHECK ((topic_id >= 0)),
    CONSTRAINT phpbb_topics_topic_last_post_id_check CHECK ((topic_last_post_id >= 0)),
    CONSTRAINT phpbb_topics_topic_last_post_time_check CHECK ((topic_last_post_time >= 0)),
    CONSTRAINT phpbb_topics_topic_last_poster_id_check CHECK ((topic_last_poster_id >= 0)),
    CONSTRAINT phpbb_topics_topic_last_view_time_check CHECK ((topic_last_view_time >= 0)),
    CONSTRAINT phpbb_topics_topic_moved_id_check CHECK ((topic_moved_id >= 0)),
    CONSTRAINT phpbb_topics_topic_poster_check CHECK ((topic_poster >= 0)),
    CONSTRAINT phpbb_topics_topic_posts_approved_check CHECK ((topic_posts_approved >= 0)),
    CONSTRAINT phpbb_topics_topic_posts_softdeleted_check CHECK ((topic_posts_softdeleted >= 0)),
    CONSTRAINT phpbb_topics_topic_posts_unapproved_check CHECK ((topic_posts_unapproved >= 0)),
    CONSTRAINT phpbb_topics_topic_reported_check CHECK ((topic_reported >= 0)),
    CONSTRAINT phpbb_topics_topic_time_check CHECK ((topic_time >= 0)),
    CONSTRAINT phpbb_topics_topic_time_limit_check CHECK ((topic_time_limit >= 0)),
    CONSTRAINT phpbb_topics_topic_views_check CHECK ((topic_views >= 0))
);


ALTER TABLE public.phpbb_topics OWNER TO phpbb;

--
-- Name: phpbb_topics_posted; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_topics_posted (
    user_id integer DEFAULT 0 NOT NULL,
    topic_id integer DEFAULT 0 NOT NULL,
    topic_posted smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_topics_posted_topic_id_check CHECK ((topic_id >= 0)),
    CONSTRAINT phpbb_topics_posted_topic_posted_check CHECK ((topic_posted >= 0)),
    CONSTRAINT phpbb_topics_posted_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_topics_posted OWNER TO phpbb;

--
-- Name: phpbb_topics_track; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_topics_track (
    user_id integer DEFAULT 0 NOT NULL,
    topic_id integer DEFAULT 0 NOT NULL,
    forum_id integer DEFAULT 0 NOT NULL,
    mark_time integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_topics_track_forum_id_check CHECK ((forum_id >= 0)),
    CONSTRAINT phpbb_topics_track_mark_time_check CHECK ((mark_time >= 0)),
    CONSTRAINT phpbb_topics_track_topic_id_check CHECK ((topic_id >= 0)),
    CONSTRAINT phpbb_topics_track_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_topics_track OWNER TO phpbb;

--
-- Name: phpbb_topics_watch; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_topics_watch (
    topic_id integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    notify_status smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_topics_watch_notify_status_check CHECK ((notify_status >= 0)),
    CONSTRAINT phpbb_topics_watch_topic_id_check CHECK ((topic_id >= 0)),
    CONSTRAINT phpbb_topics_watch_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_topics_watch OWNER TO phpbb;

--
-- Name: phpbb_user_group; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_user_group (
    group_id integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    group_leader smallint DEFAULT '0'::smallint NOT NULL,
    user_pending smallint DEFAULT '1'::smallint NOT NULL,
    CONSTRAINT phpbb_user_group_group_id_check CHECK ((group_id >= 0)),
    CONSTRAINT phpbb_user_group_group_leader_check CHECK ((group_leader >= 0)),
    CONSTRAINT phpbb_user_group_user_id_check CHECK ((user_id >= 0)),
    CONSTRAINT phpbb_user_group_user_pending_check CHECK ((user_pending >= 0))
);


ALTER TABLE public.phpbb_user_group OWNER TO phpbb;

--
-- Name: phpbb_user_notifications; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_user_notifications (
    item_type character varying(165) DEFAULT ''::character varying NOT NULL,
    item_id integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    method character varying(165) DEFAULT ''::character varying NOT NULL,
    notify smallint DEFAULT '1'::smallint NOT NULL,
    CONSTRAINT phpbb_user_notifications_item_id_check CHECK ((item_id >= 0)),
    CONSTRAINT phpbb_user_notifications_notify_check CHECK ((notify >= 0)),
    CONSTRAINT phpbb_user_notifications_user_id_check CHECK ((user_id >= 0))
);


ALTER TABLE public.phpbb_user_notifications OWNER TO phpbb;

--
-- Name: phpbb_users_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_users_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_users_seq OWNER TO phpbb;

--
-- Name: phpbb_users; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_users (
    user_id integer DEFAULT nextval('public.phpbb_users_seq'::regclass) NOT NULL,
    user_type smallint DEFAULT '0'::smallint NOT NULL,
    group_id integer DEFAULT 3 NOT NULL,
    user_permissions text DEFAULT ''::text NOT NULL,
    user_perm_from integer DEFAULT 0 NOT NULL,
    user_ip character varying(40) DEFAULT ''::character varying NOT NULL,
    user_regdate integer DEFAULT 0 NOT NULL,
    username public.varchar_ci DEFAULT ''::character varying NOT NULL,
    username_clean public.varchar_ci DEFAULT ''::character varying NOT NULL,
    user_password character varying(255) DEFAULT ''::character varying NOT NULL,
    user_passchg integer DEFAULT 0 NOT NULL,
    user_email character varying(100) DEFAULT ''::character varying NOT NULL,
    user_birthday character varying(10) DEFAULT ''::character varying NOT NULL,
    user_lastvisit integer DEFAULT 0 NOT NULL,
    user_lastmark integer DEFAULT 0 NOT NULL,
    user_lastpost_time integer DEFAULT 0 NOT NULL,
    user_lastpage character varying(200) DEFAULT ''::character varying NOT NULL,
    user_last_confirm_key character varying(10) DEFAULT ''::character varying NOT NULL,
    user_last_search integer DEFAULT 0 NOT NULL,
    user_warnings smallint DEFAULT '0'::smallint NOT NULL,
    user_last_warning integer DEFAULT 0 NOT NULL,
    user_login_attempts smallint DEFAULT '0'::smallint NOT NULL,
    user_inactive_reason smallint DEFAULT '0'::smallint NOT NULL,
    user_inactive_time integer DEFAULT 0 NOT NULL,
    user_posts integer DEFAULT 0 NOT NULL,
    user_lang character varying(30) DEFAULT ''::character varying NOT NULL,
    user_timezone character varying(100) DEFAULT ''::character varying NOT NULL,
    user_dateformat character varying(64) DEFAULT 'd M Y H:i'::character varying NOT NULL,
    user_style integer DEFAULT 0 NOT NULL,
    user_rank integer DEFAULT 0 NOT NULL,
    user_colour character varying(6) DEFAULT ''::character varying NOT NULL,
    user_new_privmsg integer DEFAULT 0 NOT NULL,
    user_unread_privmsg integer DEFAULT 0 NOT NULL,
    user_last_privmsg integer DEFAULT 0 NOT NULL,
    user_message_rules smallint DEFAULT '0'::smallint NOT NULL,
    user_full_folder integer DEFAULT '-3'::integer NOT NULL,
    user_emailtime integer DEFAULT 0 NOT NULL,
    user_topic_show_days smallint DEFAULT '0'::smallint NOT NULL,
    user_topic_sortby_type character varying(1) DEFAULT 't'::character varying NOT NULL,
    user_topic_sortby_dir character varying(1) DEFAULT 'd'::character varying NOT NULL,
    user_post_show_days smallint DEFAULT '0'::smallint NOT NULL,
    user_post_sortby_type character varying(1) DEFAULT 't'::character varying NOT NULL,
    user_post_sortby_dir character varying(1) DEFAULT 'a'::character varying NOT NULL,
    user_notify smallint DEFAULT '0'::smallint NOT NULL,
    user_notify_pm smallint DEFAULT '1'::smallint NOT NULL,
    user_notify_type smallint DEFAULT '0'::smallint NOT NULL,
    user_allow_pm smallint DEFAULT '1'::smallint NOT NULL,
    user_allow_viewonline smallint DEFAULT '1'::smallint NOT NULL,
    user_allow_viewemail smallint DEFAULT '1'::smallint NOT NULL,
    user_allow_massemail smallint DEFAULT '1'::smallint NOT NULL,
    user_options integer DEFAULT 230271 NOT NULL,
    user_avatar character varying(255) DEFAULT ''::character varying NOT NULL,
    user_avatar_type character varying(255) DEFAULT ''::character varying NOT NULL,
    user_avatar_width smallint DEFAULT '0'::smallint NOT NULL,
    user_avatar_height smallint DEFAULT '0'::smallint NOT NULL,
    user_sig text DEFAULT ''::text NOT NULL,
    user_sig_bbcode_uid character varying(8) DEFAULT ''::character varying NOT NULL,
    user_sig_bbcode_bitfield character varying(255) DEFAULT ''::character varying NOT NULL,
    user_jabber character varying(255) DEFAULT ''::character varying NOT NULL,
    user_actkey character varying(32) DEFAULT ''::character varying NOT NULL,
    reset_token character varying(64) DEFAULT ''::character varying NOT NULL,
    reset_token_expiration integer DEFAULT 0 NOT NULL,
    user_newpasswd character varying(255) DEFAULT ''::character varying NOT NULL,
    user_form_salt character varying(32) DEFAULT ''::character varying NOT NULL,
    user_new smallint DEFAULT '1'::smallint NOT NULL,
    user_reminded smallint DEFAULT '0'::smallint NOT NULL,
    user_reminded_time integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_users_group_id_check CHECK ((group_id >= 0)),
    CONSTRAINT phpbb_users_reset_token_expiration_check CHECK ((reset_token_expiration >= 0)),
    CONSTRAINT phpbb_users_user_allow_massemail_check CHECK ((user_allow_massemail >= 0)),
    CONSTRAINT phpbb_users_user_allow_pm_check CHECK ((user_allow_pm >= 0)),
    CONSTRAINT phpbb_users_user_allow_viewemail_check CHECK ((user_allow_viewemail >= 0)),
    CONSTRAINT phpbb_users_user_allow_viewonline_check CHECK ((user_allow_viewonline >= 0)),
    CONSTRAINT phpbb_users_user_avatar_height_check CHECK ((user_avatar_height >= 0)),
    CONSTRAINT phpbb_users_user_avatar_width_check CHECK ((user_avatar_width >= 0)),
    CONSTRAINT phpbb_users_user_emailtime_check CHECK ((user_emailtime >= 0)),
    CONSTRAINT phpbb_users_user_id_check CHECK ((user_id >= 0)),
    CONSTRAINT phpbb_users_user_inactive_time_check CHECK ((user_inactive_time >= 0)),
    CONSTRAINT phpbb_users_user_last_privmsg_check CHECK ((user_last_privmsg >= 0)),
    CONSTRAINT phpbb_users_user_last_search_check CHECK ((user_last_search >= 0)),
    CONSTRAINT phpbb_users_user_last_warning_check CHECK ((user_last_warning >= 0)),
    CONSTRAINT phpbb_users_user_lastmark_check CHECK ((user_lastmark >= 0)),
    CONSTRAINT phpbb_users_user_lastpost_time_check CHECK ((user_lastpost_time >= 0)),
    CONSTRAINT phpbb_users_user_lastvisit_check CHECK ((user_lastvisit >= 0)),
    CONSTRAINT phpbb_users_user_message_rules_check CHECK ((user_message_rules >= 0)),
    CONSTRAINT phpbb_users_user_new_check CHECK ((user_new >= 0)),
    CONSTRAINT phpbb_users_user_notify_check CHECK ((user_notify >= 0)),
    CONSTRAINT phpbb_users_user_notify_pm_check CHECK ((user_notify_pm >= 0)),
    CONSTRAINT phpbb_users_user_options_check CHECK ((user_options >= 0)),
    CONSTRAINT phpbb_users_user_passchg_check CHECK ((user_passchg >= 0)),
    CONSTRAINT phpbb_users_user_perm_from_check CHECK ((user_perm_from >= 0)),
    CONSTRAINT phpbb_users_user_post_show_days_check CHECK ((user_post_show_days >= 0)),
    CONSTRAINT phpbb_users_user_posts_check CHECK ((user_posts >= 0)),
    CONSTRAINT phpbb_users_user_rank_check CHECK ((user_rank >= 0)),
    CONSTRAINT phpbb_users_user_regdate_check CHECK ((user_regdate >= 0)),
    CONSTRAINT phpbb_users_user_reminded_time_check CHECK ((user_reminded_time >= 0)),
    CONSTRAINT phpbb_users_user_style_check CHECK ((user_style >= 0)),
    CONSTRAINT phpbb_users_user_topic_show_days_check CHECK ((user_topic_show_days >= 0))
);


ALTER TABLE public.phpbb_users OWNER TO phpbb;

--
-- Name: phpbb_warnings_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_warnings_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_warnings_seq OWNER TO phpbb;

--
-- Name: phpbb_warnings; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_warnings (
    warning_id integer DEFAULT nextval('public.phpbb_warnings_seq'::regclass) NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    post_id integer DEFAULT 0 NOT NULL,
    log_id integer DEFAULT 0 NOT NULL,
    warning_time integer DEFAULT 0 NOT NULL,
    CONSTRAINT phpbb_warnings_log_id_check CHECK ((log_id >= 0)),
    CONSTRAINT phpbb_warnings_post_id_check CHECK ((post_id >= 0)),
    CONSTRAINT phpbb_warnings_user_id_check CHECK ((user_id >= 0)),
    CONSTRAINT phpbb_warnings_warning_id_check CHECK ((warning_id >= 0)),
    CONSTRAINT phpbb_warnings_warning_time_check CHECK ((warning_time >= 0))
);


ALTER TABLE public.phpbb_warnings OWNER TO phpbb;

--
-- Name: phpbb_words_seq; Type: SEQUENCE; Schema: public; Owner: phpbb
--

CREATE SEQUENCE public.phpbb_words_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phpbb_words_seq OWNER TO phpbb;

--
-- Name: phpbb_words; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_words (
    word_id integer DEFAULT nextval('public.phpbb_words_seq'::regclass) NOT NULL,
    word character varying(255) DEFAULT ''::character varying NOT NULL,
    replacement character varying(255) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT phpbb_words_word_id_check CHECK ((word_id >= 0))
);


ALTER TABLE public.phpbb_words OWNER TO phpbb;

--
-- Name: phpbb_zebra; Type: TABLE; Schema: public; Owner: phpbb
--

CREATE TABLE public.phpbb_zebra (
    user_id integer DEFAULT 0 NOT NULL,
    zebra_id integer DEFAULT 0 NOT NULL,
    friend smallint DEFAULT '0'::smallint NOT NULL,
    foe smallint DEFAULT '0'::smallint NOT NULL,
    CONSTRAINT phpbb_zebra_foe_check CHECK ((foe >= 0)),
    CONSTRAINT phpbb_zebra_friend_check CHECK ((friend >= 0)),
    CONSTRAINT phpbb_zebra_user_id_check CHECK ((user_id >= 0)),
    CONSTRAINT phpbb_zebra_zebra_id_check CHECK ((zebra_id >= 0))
);


ALTER TABLE public.phpbb_zebra OWNER TO phpbb;

--
-- Data for Name: phpbb_acl_groups; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_acl_groups (group_id, forum_id, auth_option_id, auth_role_id, auth_setting) FROM stdin;
1	0	91	0	1
1	0	100	0	1
1	0	119	0	1
5	0	0	5	0
5	0	0	1	0
2	0	0	6	0
3	0	0	6	0
4	0	0	5	0
4	0	0	10	0
1	1	0	17	0
2	1	0	17	0
3	1	0	17	0
6	1	0	17	0
1	2	0	17	0
2	2	0	15	0
3	2	0	15	0
4	2	0	21	0
5	2	0	14	0
5	2	0	10	0
6	2	0	19	0
7	0	0	23	0
7	2	0	24	0
\.


--
-- Data for Name: phpbb_acl_options; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_acl_options (auth_option_id, auth_option, is_global, is_local, founder_only) FROM stdin;
1	f_	0	1	0
2	f_announce	0	1	0
3	f_announce_global	0	1	0
4	f_attach	0	1	0
5	f_bbcode	0	1	0
6	f_bump	0	1	0
7	f_delete	0	1	0
8	f_download	0	1	0
9	f_edit	0	1	0
10	f_email	0	1	0
11	f_flash	0	1	0
12	f_icons	0	1	0
13	f_ignoreflood	0	1	0
14	f_img	0	1	0
15	f_list	0	1	0
16	f_list_topics	0	1	0
17	f_noapprove	0	1	0
18	f_poll	0	1	0
19	f_post	0	1	0
20	f_postcount	0	1	0
21	f_print	0	1	0
22	f_read	0	1	0
23	f_reply	0	1	0
24	f_report	0	1	0
25	f_search	0	1	0
26	f_sigs	0	1	0
27	f_smilies	0	1	0
28	f_sticky	0	1	0
29	f_subscribe	0	1	0
30	f_user_lock	0	1	0
31	f_vote	0	1	0
32	f_votechg	0	1	0
33	f_softdelete	0	1	0
34	m_	1	1	0
35	m_approve	1	1	0
36	m_chgposter	1	1	0
37	m_delete	1	1	0
38	m_edit	1	1	0
39	m_info	1	1	0
40	m_lock	1	1	0
41	m_merge	1	1	0
42	m_move	1	1	0
43	m_report	1	1	0
44	m_split	1	1	0
45	m_softdelete	1	1	0
46	m_ban	1	0	0
47	m_pm_report	1	0	0
48	m_warn	1	0	0
49	a_	1	0	0
50	a_aauth	1	0	0
51	a_attach	1	0	0
52	a_authgroups	1	0	0
53	a_authusers	1	0	0
54	a_backup	1	0	0
55	a_ban	1	0	0
56	a_bbcode	1	0	0
57	a_board	1	0	0
58	a_bots	1	0	0
59	a_clearlogs	1	0	0
60	a_email	1	0	0
61	a_extensions	1	0	0
62	a_fauth	1	0	0
63	a_forum	1	0	0
64	a_forumadd	1	0	0
65	a_forumdel	1	0	0
66	a_group	1	0	0
67	a_groupadd	1	0	0
68	a_groupdel	1	0	0
69	a_icons	1	0	0
70	a_jabber	1	0	0
71	a_language	1	0	0
72	a_mauth	1	0	0
73	a_modules	1	0	0
74	a_names	1	0	0
75	a_phpinfo	1	0	0
76	a_profile	1	0	0
77	a_prune	1	0	0
78	a_ranks	1	0	0
79	a_reasons	1	0	0
80	a_roles	1	0	0
81	a_search	1	0	0
82	a_server	1	0	0
83	a_styles	1	0	0
84	a_switchperm	1	0	0
85	a_uauth	1	0	0
86	a_user	1	0	0
87	a_userdel	1	0	0
88	a_viewauth	1	0	0
89	a_viewlogs	1	0	0
90	a_words	1	0	0
91	u_	1	0	0
92	u_attach	1	0	0
93	u_chgavatar	1	0	0
94	u_chgcensors	1	0	0
95	u_chgemail	1	0	0
96	u_chggrp	1	0	0
97	u_chgname	1	0	0
98	u_chgpasswd	1	0	0
99	u_chgprofileinfo	1	0	0
100	u_download	1	0	0
101	u_emoji	1	0	0
102	u_hideonline	1	0	0
103	u_ignoreflood	1	0	0
104	u_masspm	1	0	0
105	u_masspm_group	1	0	0
106	u_pm_attach	1	0	0
107	u_pm_bbcode	1	0	0
108	u_pm_delete	1	0	0
109	u_pm_download	1	0	0
110	u_pm_edit	1	0	0
111	u_pm_emailpm	1	0	0
112	u_pm_flash	1	0	0
113	u_pm_forward	1	0	0
114	u_pm_img	1	0	0
115	u_pm_printpm	1	0	0
116	u_pm_smilies	1	0	0
117	u_readpm	1	0	0
118	u_savedrafts	1	0	0
119	u_search	1	0	0
120	u_sendemail	1	0	0
121	u_sendim	1	0	0
122	u_sendpm	1	0	0
123	u_sig	1	0	0
124	u_viewonline	1	0	0
125	u_viewprofile	1	0	0
\.


--
-- Data for Name: phpbb_acl_roles; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_acl_roles (role_id, role_name, role_description, role_type, role_order) FROM stdin;
1	ROLE_ADMIN_STANDARD	ROLE_DESCRIPTION_ADMIN_STANDARD	a_	1
2	ROLE_ADMIN_FORUM	ROLE_DESCRIPTION_ADMIN_FORUM	a_	3
3	ROLE_ADMIN_USERGROUP	ROLE_DESCRIPTION_ADMIN_USERGROUP	a_	4
4	ROLE_ADMIN_FULL	ROLE_DESCRIPTION_ADMIN_FULL	a_	2
5	ROLE_USER_FULL	ROLE_DESCRIPTION_USER_FULL	u_	3
6	ROLE_USER_STANDARD	ROLE_DESCRIPTION_USER_STANDARD	u_	1
7	ROLE_USER_LIMITED	ROLE_DESCRIPTION_USER_LIMITED	u_	2
8	ROLE_USER_NOPM	ROLE_DESCRIPTION_USER_NOPM	u_	4
9	ROLE_USER_NOAVATAR	ROLE_DESCRIPTION_USER_NOAVATAR	u_	5
10	ROLE_MOD_FULL	ROLE_DESCRIPTION_MOD_FULL	m_	3
11	ROLE_MOD_STANDARD	ROLE_DESCRIPTION_MOD_STANDARD	m_	1
12	ROLE_MOD_SIMPLE	ROLE_DESCRIPTION_MOD_SIMPLE	m_	2
13	ROLE_MOD_QUEUE	ROLE_DESCRIPTION_MOD_QUEUE	m_	4
14	ROLE_FORUM_FULL	ROLE_DESCRIPTION_FORUM_FULL	f_	7
15	ROLE_FORUM_STANDARD	ROLE_DESCRIPTION_FORUM_STANDARD	f_	5
16	ROLE_FORUM_NOACCESS	ROLE_DESCRIPTION_FORUM_NOACCESS	f_	1
17	ROLE_FORUM_READONLY	ROLE_DESCRIPTION_FORUM_READONLY	f_	2
18	ROLE_FORUM_LIMITED	ROLE_DESCRIPTION_FORUM_LIMITED	f_	3
19	ROLE_FORUM_BOT	ROLE_DESCRIPTION_FORUM_BOT	f_	9
20	ROLE_FORUM_ONQUEUE	ROLE_DESCRIPTION_FORUM_ONQUEUE	f_	8
21	ROLE_FORUM_POLLS	ROLE_DESCRIPTION_FORUM_POLLS	f_	6
22	ROLE_FORUM_LIMITED_POLLS	ROLE_DESCRIPTION_FORUM_LIMITED_POLLS	f_	4
23	ROLE_USER_NEW_MEMBER	ROLE_DESCRIPTION_USER_NEW_MEMBER	u_	6
24	ROLE_FORUM_NEW_MEMBER	ROLE_DESCRIPTION_FORUM_NEW_MEMBER	f_	10
\.


--
-- Data for Name: phpbb_acl_roles_data; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_acl_roles_data (role_id, auth_option_id, auth_setting) FROM stdin;
1	49	1
1	51	1
1	52	1
1	53	1
1	55	1
1	56	1
1	57	1
1	61	1
1	62	1
1	63	1
1	64	1
1	65	1
1	66	1
1	67	1
1	68	1
1	69	1
1	72	1
1	74	1
1	76	1
1	77	1
1	78	1
1	79	1
1	85	1
1	86	1
1	87	1
1	88	1
1	89	1
1	90	1
2	49	1
2	52	1
2	53	1
2	62	1
2	63	1
2	64	1
2	65	1
2	72	1
2	77	1
2	85	1
2	88	1
2	89	1
3	49	1
3	52	1
3	53	1
3	55	1
3	66	1
3	67	1
3	68	1
3	78	1
3	85	1
3	86	1
3	88	1
3	89	1
4	49	1
4	50	1
4	51	1
4	52	1
4	53	1
4	54	1
4	55	1
4	56	1
4	57	1
4	58	1
4	59	1
4	60	1
4	61	1
4	62	1
4	63	1
4	64	1
4	65	1
4	66	1
4	67	1
4	68	1
4	69	1
4	70	1
4	71	1
4	72	1
4	73	1
4	74	1
4	75	1
4	76	1
4	77	1
4	78	1
4	79	1
4	80	1
4	81	1
4	82	1
4	83	1
4	84	1
4	85	1
4	86	1
4	87	1
4	88	1
4	89	1
4	90	1
5	91	1
5	92	1
5	93	1
5	94	1
5	95	1
5	96	1
5	97	1
5	98	1
5	99	1
5	100	1
5	101	1
5	102	1
5	103	1
5	104	1
5	105	1
5	106	1
5	107	1
5	108	1
5	109	1
5	110	1
5	111	1
5	112	1
5	113	1
5	114	1
5	115	1
5	116	1
5	117	1
5	118	1
5	119	1
5	120	1
5	121	1
5	122	1
5	123	1
5	124	1
5	125	1
6	91	1
6	92	1
6	93	1
6	94	1
6	95	1
6	98	1
6	99	1
6	100	1
6	101	1
6	102	1
6	104	1
6	105	1
6	106	1
6	107	1
6	108	1
6	109	1
6	110	1
6	111	1
6	114	1
6	115	1
6	116	1
6	117	1
6	118	1
6	119	1
6	120	1
6	121	1
6	122	1
6	123	1
6	125	1
7	91	1
7	93	1
7	94	1
7	95	1
7	98	1
7	99	1
7	100	1
7	101	1
7	102	1
7	107	1
7	108	1
7	109	1
7	110	1
7	113	1
7	114	1
7	115	1
7	116	1
7	117	1
7	122	1
7	123	1
7	125	1
8	91	1
8	93	1
8	94	1
8	95	1
8	98	1
8	100	1
8	102	1
8	123	1
8	125	1
8	104	0
8	105	0
8	117	0
8	122	0
9	91	1
9	94	1
9	95	1
9	98	1
9	99	1
9	100	1
9	101	1
9	102	1
9	107	1
9	108	1
9	109	1
9	110	1
9	113	1
9	114	1
9	115	1
9	116	1
9	117	1
9	122	1
9	123	1
9	125	1
9	93	0
10	34	1
10	35	1
10	36	1
10	37	1
10	38	1
10	39	1
10	40	1
10	41	1
10	42	1
10	43	1
10	44	1
10	45	1
10	46	1
10	47	1
10	48	1
11	34	1
11	35	1
11	37	1
11	38	1
11	39	1
11	40	1
11	41	1
11	42	1
11	43	1
11	44	1
11	45	1
11	47	1
11	48	1
12	34	1
12	37	1
12	38	1
12	39	1
12	43	1
12	45	1
12	47	1
13	34	1
13	35	1
13	38	1
14	1	1
14	2	1
14	3	1
14	4	1
14	5	1
14	6	1
14	7	1
14	8	1
14	9	1
14	10	1
14	11	1
14	12	1
14	13	1
14	14	1
14	15	1
14	16	1
14	17	1
14	18	1
14	19	1
14	20	1
14	21	1
14	22	1
14	23	1
14	24	1
14	25	1
14	26	1
14	27	1
14	28	1
14	29	1
14	30	1
14	31	1
14	32	1
14	33	1
15	1	1
15	4	1
15	5	1
15	6	1
15	7	1
15	8	1
15	9	1
15	10	1
15	12	1
15	14	1
15	15	1
15	16	1
15	17	1
15	19	1
15	20	1
15	21	1
15	22	1
15	23	1
15	24	1
15	25	1
15	26	1
15	27	1
15	29	1
15	31	1
15	32	1
15	33	1
16	1	0
17	1	1
17	8	1
17	15	1
17	16	1
17	21	1
17	22	1
17	25	1
17	29	1
18	1	1
18	5	1
18	8	1
18	9	1
18	10	1
18	14	1
18	15	1
18	16	1
18	17	1
18	19	1
18	20	1
18	21	1
18	22	1
18	23	1
18	24	1
18	25	1
18	26	1
18	27	1
18	29	1
18	31	1
18	33	1
19	1	1
19	8	1
19	15	1
19	16	1
19	21	1
19	22	1
20	1	1
20	4	1
20	5	1
20	8	1
20	9	1
20	10	1
20	14	1
20	15	1
20	16	1
20	19	1
20	20	1
20	21	1
20	22	1
20	23	1
20	24	1
20	25	1
20	26	1
20	27	1
20	29	1
20	31	1
20	33	1
20	17	0
21	1	1
21	4	1
21	5	1
21	6	1
21	7	1
21	8	1
21	9	1
21	10	1
21	12	1
21	14	1
21	15	1
21	16	1
21	17	1
21	18	1
21	19	1
21	20	1
21	21	1
21	22	1
21	23	1
21	24	1
21	25	1
21	26	1
21	27	1
21	29	1
21	31	1
21	32	1
21	33	1
22	1	1
22	5	1
22	8	1
22	9	1
22	10	1
22	14	1
22	15	1
22	16	1
22	17	1
22	18	1
22	19	1
22	20	1
22	21	1
22	22	1
22	23	1
22	24	1
22	25	1
22	26	1
22	27	1
22	29	1
22	31	1
22	33	1
23	99	0
23	104	0
23	105	0
23	122	0
24	17	0
\.


--
-- Data for Name: phpbb_acl_users; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_acl_users (user_id, forum_id, auth_option_id, auth_role_id, auth_setting) FROM stdin;
2	0	0	5	0
\.


--
-- Data for Name: phpbb_attachments; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_attachments (attach_id, post_msg_id, topic_id, in_message, poster_id, is_orphan, physical_filename, real_filename, download_count, attach_comment, extension, mimetype, filesize, filetime, thumbnail) FROM stdin;
\.


--
-- Data for Name: phpbb_banlist; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_banlist (ban_id, ban_userid, ban_ip, ban_email, ban_start, ban_end, ban_exclude, ban_reason, ban_give_reason) FROM stdin;
\.


--
-- Data for Name: phpbb_bbcodes; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_bbcodes (bbcode_id, bbcode_tag, bbcode_helpline, display_on_posting, bbcode_match, bbcode_tpl, first_pass_match, first_pass_replace, second_pass_match, second_pass_replace) FROM stdin;
\.


--
-- Data for Name: phpbb_bookmarks; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_bookmarks (topic_id, user_id) FROM stdin;
\.


--
-- Data for Name: phpbb_bots; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_bots (bot_id, bot_active, bot_name, user_id, bot_agent, bot_ip) FROM stdin;
1	1	AdsBot [Google]	3	AdsBot-Google	
2	1	Alexa [Bot]	4	ia_archiver	
3	1	Alta Vista [Bot]	5	Scooter/	
4	1	Ask Jeeves [Bot]	6	Ask Jeeves	
5	1	Baidu [Spider]	7	Baiduspider	
6	1	Bing [Bot]	8	bingbot/	
7	1	DuckDuckGo [Bot]	9	DuckDuckBot/	
8	1	Exabot [Bot]	10	Exabot	
9	1	FAST Enterprise [Crawler]	11	FAST Enterprise Crawler	
10	1	FAST WebCrawler [Crawler]	12	FAST-WebCrawler/	
11	1	Francis [Bot]	13	http://www.neomo.de/	
12	1	Gigabot [Bot]	14	Gigabot/	
13	1	Google Adsense [Bot]	15	Mediapartners-Google	
14	1	Google Desktop	16	Google Desktop	
15	1	Google Feedfetcher	17	Feedfetcher-Google	
16	1	Google [Bot]	18	Googlebot	
17	1	Heise IT-Markt [Crawler]	19	heise-IT-Markt-Crawler	
18	1	Heritrix [Crawler]	20	heritrix/1.	
19	1	IBM Research [Bot]	21	ibm.com/cs/crawler	
20	1	ICCrawler - ICjobs	22	ICCrawler - ICjobs	
21	1	ichiro [Crawler]	23	ichiro/	
22	1	Majestic-12 [Bot]	24	MJ12bot/	
23	1	Metager [Bot]	25	MetagerBot/	
24	1	MSN NewsBlogs	26	msnbot-NewsBlogs/	
25	1	MSN [Bot]	27	msnbot/	
26	1	MSNbot Media	28	msnbot-media/	
27	1	Nutch [Bot]	29	http://lucene.apache.org/nutch/	
28	1	Online link [Validator]	30	online link validator	
29	1	psbot [Picsearch]	31	psbot/0	
30	1	Sensis [Crawler]	32	Sensis Web Crawler	
31	1	SEO Crawler	33	SEO search Crawler/	
32	1	Seoma [Crawler]	34	Seoma [SEO Crawler]	
33	1	SEOSearch [Crawler]	35	SEOsearch/	
34	1	Snappy [Bot]	36	Snappy/1.1 ( http://www.urltrends.com/ )	
35	1	Steeler [Crawler]	37	http://www.tkl.iis.u-tokyo.ac.jp/~crawler/	
36	1	Telekom [Bot]	38	crawleradmin.t-info@telekom.de	
37	1	TurnitinBot [Bot]	39	TurnitinBot/	
38	1	Voyager [Bot]	40	voyager/	
39	1	W3 [Sitesearch]	41	W3 SiteSearch Crawler	
40	1	W3C [Linkcheck]	42	W3C-checklink/	
41	1	W3C [Validator]	43	W3C_Validator	
42	1	YaCy [Bot]	44	yacybot	
43	1	Yahoo MMCrawler [Bot]	45	Yahoo-MMCrawler/	
44	1	Yahoo Slurp [Bot]	46	Yahoo! DE Slurp	
45	1	Yahoo [Bot]	47	Yahoo! Slurp	
46	1	YahooSeeker [Bot]	48	YahooSeeker/	
\.


--
-- Data for Name: phpbb_config; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_config (config_name, config_value, is_dynamic) FROM stdin;
active_sessions	0	0
allow_autologin	1	0
allow_avatar	1	0
allow_avatar_gravatar	0	0
allow_avatar_local	0	0
allow_avatar_remote	0	0
allow_avatar_remote_upload	0	0
allow_avatar_upload	1	0
allow_bbcode	1	0
allow_birthdays	0	0
allow_board_notifications	1	0
allow_bookmarks	1	0
allow_cdn	0	0
allow_emailreuse	0	0
allow_forum_notify	1	0
allow_live_searches	1	0
allow_mass_pm	1	0
allow_name_chars	USERNAME_CHARS_ANY	0
allow_namechange	0	0
allow_nocensors	0	0
allow_password_reset	1	0
allow_pm_report	1	0
allow_post_flash	1	0
allow_post_links	1	0
allow_privmsg	1	0
allow_quick_reply	1	0
allow_sig	1	0
allow_sig_bbcode	1	0
allow_sig_flash	0	0
allow_sig_img	1	0
allow_sig_links	1	0
allow_sig_pm	1	0
allow_sig_smilies	1	0
allow_smilies	1	0
allow_topic_notify	1	0
allowed_schemes_links	http,https,ftp	0
auth_bbcode_pm	1	0
auth_flash_pm	0	0
auth_img_pm	1	0
auth_method	db	0
auth_smilies_pm	1	0
avatar_filesize	6144	0
avatar_gallery_path	images/avatars/gallery	0
avatar_max_height	90	0
avatar_max_width	90	0
avatar_min_height	20	0
avatar_min_width	20	0
avatar_path	images/avatars/upload	0
board_contact_name		0
board_email_form	0	0
board_email_sig	Thanks, The Management	0
board_hide_emails	1	0
browser_check	1	0
bump_interval	10	0
bump_type	d	0
cache_gc	7200	0
captcha_gd_3d_noise	1	0
captcha_gd_fonts	1	0
captcha_gd_foreground_noise	0	0
captcha_gd_wave	0	0
captcha_gd_x_grid	25	0
captcha_gd_y_grid	25	0
check_dnsbl	0	0
chg_passforce	0	0
confirm_refresh	1	0
contact_admin_form_enable	1	0
cookie_notice	0	0
cookie_path	/	0
coppa_enable	0	0
coppa_fax		0
coppa_mail		0
database_gc	604800	0
default_search_return_chars	300	0
delete_time	0	0
display_last_edited	1	0
display_last_subject	1	0
display_unapproved_posts	1	0
edit_time	0	0
email_check_mx	1	0
email_force_sender	0	0
email_max_chunk_size	50	0
email_package_size	20	0
enable_accurate_pm_button	1	0
enable_confirm	1	0
enable_mod_rewrite	0	0
enable_pm_icons	1	0
enable_post_confirm	1	0
enable_queue_trigger	0	0
enable_update_hashes	1	0
extension_force_unstable	0	0
feed_enable	1	0
feed_forum	1	0
feed_http_auth	0	0
feed_item_statistics	1	0
feed_limit	10	0
feed_limit_post	15	0
feed_limit_topic	10	0
feed_overall	1	0
feed_overall_forums	0	0
feed_overall_forums_limit	15	0
feed_overall_topics	0	0
feed_overall_topics_limit	15	0
feed_topic	1	0
feed_topics_active	0	0
feed_topics_new	1	0
flood_interval	15	0
form_token_lifetime	7200	0
form_token_mintime	0	0
form_token_sid_guests	1	0
forward_pm	1	0
forwarded_for_check	0	0
full_folder_action	2	0
fulltext_mysql_max_word_len	254	0
fulltext_mysql_min_word_len	4	0
fulltext_native_common_thres	5	0
fulltext_native_load_upd	1	0
fulltext_native_max_chars	14	0
fulltext_native_min_chars	3	0
fulltext_postgres_max_word_len	254	0
fulltext_postgres_min_word_len	4	0
fulltext_postgres_ts_name	simple	0
fulltext_sphinx_indexer_mem_limit	512	0
fulltext_sphinx_stopwords	0	0
gzip_compress	0	0
hot_threshold	25	0
board_contact	admin@local.host	0
cookie_domain		0
email_enable		0
cookie_secure	1	0
force_server_vars	0	0
avatar_salt	dd07a2dbf3967881e160b259789f11fb	0
captcha_plugin	core.captcha.plugins.gd	0
captcha_gd	1	0
cookie_name	phpbb3_amn86	0
assets_version	2	0
help_send_statistics		0
help_send_statistics_time	1628342829	0
board_index_text		0
board_disable	0	0
board_disable_msg		0
board_timezone	UTC	0
default_style	2	0
icons_path	images/icons	0
ip_check	3	0
ip_login_limit_max	50	0
allow_pm_attach	0	0
display_order	0	0
check_attachment_content	1	0
ip_login_limit_time	21600	0
ip_login_limit_use_forwarded	0	0
jab_allow_self_signed	0	0
jab_enable	0	0
jab_host		0
jab_package_size	20	0
jab_password		0
jab_port	5222	0
jab_use_ssl	0	0
jab_username		0
jab_verify_peer	1	0
jab_verify_peer_name	1	0
ldap_base_dn		0
ldap_email		0
ldap_password		0
ldap_port		0
ldap_server		0
ldap_uid		0
ldap_user		0
ldap_user_filter		0
legend_sort_groupname	0	0
limit_load	0	0
limit_search_load	0	0
load_anon_lastread	0	0
load_birthdays	1	0
load_cpf_memberlist	1	0
load_cpf_pm	1	0
load_cpf_viewprofile	1	0
load_cpf_viewtopic	1	0
load_db_lastread	1	0
load_db_track	1	0
load_font_awesome_url	https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css	0
load_jquery_url	//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js	0
load_jumpbox	1	0
load_moderators	1	0
load_notifications	1	0
load_online	1	0
load_online_guests	1	0
load_online_time	5	0
load_onlinetrack	1	0
load_search	1	0
load_tplcompile	0	0
load_unreads_search	1	0
load_user_activity	1	0
load_user_activity_limit	5000	0
max_autologin_time	0	0
max_login_attempts	3	0
max_name_chars	20	0
max_num_search_keywords	10	0
max_poll_options	10	0
max_post_chars	60000	0
max_post_font_size	200	0
max_post_img_height	0	0
max_post_img_width	0	0
max_post_smilies	0	0
max_post_urls	0	0
max_quote_depth	3	0
max_reg_attempts	5	0
max_sig_chars	255	0
max_sig_font_size	200	0
max_sig_img_height	0	0
max_sig_img_width	0	0
max_sig_smilies	0	0
max_sig_urls	5	0
mime_triggers	body|head|html|img|plaintext|a href|pre|script|table|title	0
min_name_chars	3	0
min_pass_chars	6	0
min_post_chars	1	0
min_search_author_chars	3	0
new_member_group_default	0	0
new_member_post_limit	3	0
pass_complex	PASS_TYPE_ANY	0
pm_edit_time	0	0
pm_max_boxes	4	0
pm_max_msgs	50	0
pm_max_recipients	0	0
posts_per_page	10	0
print_pm	1	0
queue_interval	60	0
queue_trigger_posts	3	0
ranks_path	images/ranks	0
read_notification_expire_days	30	0
read_notification_gc	86400	0
recaptcha_v3_domain	google.com	0
recaptcha_v3_key		0
recaptcha_v3_method	post	0
recaptcha_v3_secret		0
recaptcha_v3_threshold_default	0.5	0
recaptcha_v3_threshold_login	0.5	0
recaptcha_v3_threshold_post	0.5	0
recaptcha_v3_threshold_register	0.5	0
recaptcha_v3_threshold_report	0.5	0
remote_upload_verify	0	0
require_activation	0	0
search_anonymous_interval	0	0
search_block_size	250	0
search_gc	7200	0
search_interval	0	0
search_store_results	1800	0
search_type	\\phpbb\\search\\fulltext_native	0
session_gc	3600	0
session_length	3600	0
smilies_path	images/smilies	0
smilies_per_page	50	0
smtp_allow_self_signed	0	0
smtp_verify_peer	1	0
smtp_verify_peer_name	1	0
teampage_forums	1	0
teampage_memberships	1	0
text_reparser.pm_text_cron_interval	10	0
text_reparser.pm_text_last_cron	0	0
text_reparser.poll_option_cron_interval	10	0
server_port	80	0
smtp_delivery	0	0
smtp_host		0
smtp_port		0
script_path	/	0
server_protocol	http://	0
plupload_salt	ebec4e18e24c8fc8451c5f412abb5c70	0
site_desc		0
site_home_url		0
referer_validation	0	0
sitename	Forum	0
site_home_text		0
override_user_style	1	0
text_reparser.poll_option_last_cron	0	0
text_reparser.poll_title_cron_interval	10	0
text_reparser.poll_title_last_cron	0	0
text_reparser.post_text_cron_interval	10	0
img_max_width	0	0
img_max_height	0	0
img_link_width	0	0
max_attachments	3	0
max_attachments_pm	1	0
max_filesize	26214400	0
secure_downloads	0	0
secure_allow_deny	1	0
secure_allow_empty_referer	1	0
img_display_inlined	1	0
img_create_thumbnail	0	0
img_max_thumb_width	400	0
img_min_thumb_filesize	12000	0
img_strip_metadata	0	0
img_quality	85	0
text_reparser.post_text_last_cron	0	0
text_reparser.user_signature_cron_interval	10	0
text_reparser.user_signature_last_cron	0	0
topics_per_page	25	0
tpl_allow_php	0	0
update_hashes_last_cron	0	0
update_hashes_lock	0	0
upload_icons_path	images/upload_icons	0
use_system_cron	0	0
version	3.3.4	0
warnings_gc	14400	0
cache_last_gc	0	1
cron_lock	0	1
database_last_gc	0	1
last_queue_run	0	1
newest_user_colour	AA0000	1
newest_user_id	2	1
num_files	0	1
num_posts	1	1
num_topics	1	1
num_users	1	1
plupload_last_gc	0	1
rand_seed	0	1
rand_seed_last_update	0	1
read_notification_last_gc	0	1
reparse_lock	0	1
search_indexing_state		1
search_last_gc	0	1
session_last_gc	0	1
upload_dir_size	0	1
warnings_last_gc	0	1
board_startdate	1628342817	0
server_name	forum.example.com	0
board_email	admin@local.host	0
smtp_auth_method	PLAIN	0
smtp_username		1
smtp_password		1
newest_username	admin	1
dbms_version	13.3 (Debian 13.3-1.pgdg100+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 8.3.0-6) 8.3.0, 64-bit	0
allow_viglink_phpbb	1	0
phpbb_viglink_api_key	e4fd14f5d7f2bb6d80b8f8da1354718c	0
viglink_convert_account_url		0
viglink_api_siteid	d41d8cd98f00b204e9800998ecf8427e	0
viglink_ask_admin_last	0	0
questionnaire_unique_id	0bvvxv2v5gkf0qjv	0
viglink_enabled		0
viglink_ask_admin	1628342829	0
default_lang	en	0
default_dateformat	D M d, Y g:i a	0
warnings_expire_days	90	0
record_online_users	1	1
record_online_date	1628344900	1
viglink_last_gc	1628344918	1
img_link_height	0	0
allow_attachments	1	0
upload_path	files	0
attachment_quota	0	0
max_filesize_pm	262144	0
\.


--
-- Data for Name: phpbb_config_text; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_config_text (config_name, config_value) FROM stdin;
contact_admin_info	
contact_admin_info_uid	
contact_admin_info_bitfield	
contact_admin_info_flags	7
\.


--
-- Data for Name: phpbb_confirm; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_confirm (confirm_id, session_id, confirm_type, code, seed, attempts) FROM stdin;
\.


--
-- Data for Name: phpbb_disallow; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_disallow (disallow_id, disallow_username) FROM stdin;
\.


--
-- Data for Name: phpbb_drafts; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_drafts (draft_id, user_id, topic_id, forum_id, save_time, draft_subject, draft_message) FROM stdin;
\.


--
-- Data for Name: phpbb_ext; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_ext (ext_name, ext_active, ext_state) FROM stdin;
phpbb/viglink	1	b:0;
\.


--
-- Data for Name: phpbb_extension_groups; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_extension_groups (group_id, group_name, cat_id, allow_group, download_mode, upload_icon, max_filesize, allowed_forums, allow_in_pm) FROM stdin;
1	IMAGES	1	1	1		0		0
2	ARCHIVES	0	1	1		0		0
4	DOCUMENTS	0	1	1		0		0
3	PLAIN_TEXT	0	1	1		0		0
5	DOWNLOADABLE_FILES	0	1	1		0		0
\.


--
-- Data for Name: phpbb_extensions; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_extensions (extension_id, group_id, extension) FROM stdin;
1	1	gif
2	1	png
3	1	jpeg
4	1	jpg
5	1	tif
6	1	tiff
7	1	tga
8	2	gtar
9	2	gz
10	2	tar
11	2	zip
12	2	rar
13	2	ace
14	2	torrent
15	2	tgz
16	2	bz2
17	2	7z
50	5	mp3
51	5	mpeg
52	5	mpg
53	5	ogg
54	5	ogm
29	4	xls
30	4	xlsx
31	4	xlsm
32	4	xlsb
33	4	doc
34	4	docx
35	4	docm
36	4	dot
37	4	dotx
38	4	dotm
39	4	pdf
40	4	ai
41	4	ps
42	4	ppt
43	4	pptx
44	4	pptm
45	4	odg
46	4	odp
47	4	ods
48	4	odt
49	4	rtf
18	3	txt
19	3	c
20	3	h
21	3	cpp
22	3	hpp
23	3	diz
24	3	csv
25	3	ini
26	3	log
27	3	js
28	3	xml
\.


--
-- Data for Name: phpbb_forums; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_forums (forum_id, parent_id, left_id, right_id, forum_parents, forum_name, forum_desc, forum_desc_bitfield, forum_desc_options, forum_desc_uid, forum_link, forum_password, forum_style, forum_image, forum_rules, forum_rules_link, forum_rules_bitfield, forum_rules_options, forum_rules_uid, forum_topics_per_page, forum_type, forum_status, forum_last_post_id, forum_last_poster_id, forum_last_post_subject, forum_last_post_time, forum_last_poster_name, forum_last_poster_colour, forum_flags, display_on_index, enable_indexing, enable_icons, enable_prune, prune_next, prune_days, prune_viewed, prune_freq, display_subforum_list, display_subforum_limit, forum_options, forum_posts_approved, forum_posts_unapproved, forum_posts_softdeleted, forum_topics_approved, forum_topics_unapproved, forum_topics_softdeleted, enable_shadow_prune, prune_shadow_days, prune_shadow_freq, prune_shadow_next) FROM stdin;
1	0	1	4		Your first category			7				0					7		0	0	0	1	2		1628342817	admin	AA0000	32	1	1	1	0	0	0	0	0	1	0	0	0	0	0	0	0	0	0	7	1	0
2	1	2	3		Your first forum	Description of your first forum.		7				0					7		0	1	0	1	2	Welcome to phpBB3	1628342817	admin	AA0000	48	1	1	1	0	0	7	7	1	1	0	0	1	0	0	1	0	0	0	7	1	0
\.


--
-- Data for Name: phpbb_forums_access; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_forums_access (forum_id, user_id, session_id) FROM stdin;
\.


--
-- Data for Name: phpbb_forums_track; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_forums_track (user_id, forum_id, mark_time) FROM stdin;
\.


--
-- Data for Name: phpbb_forums_watch; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_forums_watch (forum_id, user_id, notify_status) FROM stdin;
\.


--
-- Data for Name: phpbb_groups; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_groups (group_id, group_type, group_founder_manage, group_skip_auth, group_name, group_desc, group_desc_bitfield, group_desc_options, group_desc_uid, group_display, group_avatar, group_avatar_type, group_avatar_width, group_avatar_height, group_rank, group_colour, group_sig_chars, group_receive_pm, group_message_limit, group_legend, group_max_recipients) FROM stdin;
1	3	0	0	GUESTS			7		0			0	0	0		0	0	0	0	5
2	3	0	0	REGISTERED			7		0			0	0	0		0	0	0	0	5
3	3	0	0	REGISTERED_COPPA			7		0			0	0	0		0	0	0	0	5
4	3	0	0	GLOBAL_MODERATORS			7		0			0	0	0	00AA00	0	0	0	2	0
5	3	1	0	ADMINISTRATORS			7		0			0	0	0	AA0000	0	0	0	1	0
6	3	0	0	BOTS			7		0			0	0	0	9E8DA7	0	0	0	0	5
7	3	0	0	NEWLY_REGISTERED			7		0			0	0	0		0	0	0	0	5
\.


--
-- Data for Name: phpbb_icons; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_icons (icons_id, icons_url, icons_width, icons_height, icons_alt, icons_order, display_on_posting) FROM stdin;
1	misc/fire.gif	16	16		1	1
2	smile/redface.gif	16	16		9	1
3	smile/mrgreen.gif	16	16		10	1
4	misc/heart.gif	16	16		4	1
5	misc/star.gif	16	16		2	1
6	misc/radioactive.gif	16	16		3	1
7	misc/thinking.gif	16	16		5	1
8	smile/info.gif	16	16		8	1
9	smile/question.gif	16	16		6	1
10	smile/alert.gif	16	16		7	1
\.


--
-- Data for Name: phpbb_lang; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_lang (lang_id, lang_iso, lang_dir, lang_english_name, lang_local_name, lang_author) FROM stdin;
1	en	en	British English	British English	phpBB Limited
\.


--
-- Data for Name: phpbb_log; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_log (log_id, log_type, user_id, forum_id, topic_id, post_id, reportee_id, log_ip, log_time, log_operation, log_data) FROM stdin;
1	0	1	0	0	0	0	172.18.0.4	1628342823	LOG_MODULE_ADD	a:1:{i:0;s:20:"ACP_VIGLINK_SETTINGS";}
2	0	1	0	0	0	0		1628342823	LOG_EXT_ENABLE	a:1:{i:0;s:13:"phpbb/viglink";}
3	0	2	0	0	0	0	172.18.0.4	1628342823	LOG_INSTALL_INSTALLED	a:1:{i:0;s:5:"3.3.4";}
4	0	2	0	0	0	0	172.18.0.4	1628342853	LOG_CONFIG_ATTACH	
5	0	2	0	0	0	0	172.18.0.4	1628342888	LOG_STYLE_ADD	a:1:{i:0;s:8:"ProLight";}
6	0	2	0	0	0	0	172.18.0.4	1628342888	LOG_STYLE_ADD	a:1:{i:0;s:24:"Prosilver (Dark Edition)";}
7	0	2	0	0	0	0	172.18.0.4	1628342904	LOG_CONFIG_SETTINGS	
8	0	2	0	0	0	0	172.18.0.4	1628344916	LOG_ADMIN_AUTH_SUCCESS	
9	0	2	0	0	0	0	172.18.0.4	1628344958	LOG_ATTACH_EXTGROUP_EDIT	a:1:{i:0;s:9:"Documents";}
10	0	2	0	0	0	0	172.18.0.4	1628344973	LOG_ATTACH_EXTGROUP_EDIT	a:1:{i:0;s:10:"Plain Text";}
11	0	2	0	0	0	0	172.18.0.4	1628344986	LOG_ATTACH_EXTGROUP_EDIT	a:1:{i:0;s:18:"Downloadable Files";}
12	0	2	0	0	0	0	172.18.0.4	1628345002	LOG_CONFIG_ATTACH	
\.


--
-- Data for Name: phpbb_login_attempts; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_login_attempts (attempt_ip, attempt_browser, attempt_forwarded_for, attempt_time, user_id, username, username_clean) FROM stdin;
\.


--
-- Data for Name: phpbb_migrations; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_migrations (migration_name, migration_depends_on, migration_schema_done, migration_data_done, migration_data_state, migration_start_time, migration_end_time) FROM stdin;
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_6_rc1	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_5";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_6	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_6_rc4";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_4_rc1	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_3";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_10_rc2	a:1:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_10_rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\local_url_bbcode	a:1:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_12_rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_12_rc2	a:1:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_12_rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_14	a:1:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_14_rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_1_rc1	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_0";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_10	a:1:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_10_rc3";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_9_rc4	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_9_rc3";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_10_rc3	a:1:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_10_rc2";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_13	a:1:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_13_rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_13_rc1	a:1:{i:0;s:44:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_12";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_7_rc2	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_7_rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_11_rc1	a:1:{i:0;s:44:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_10";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_2_rc2	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_2_rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_9_rc1	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_8";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_11_rc2	a:1:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_11_rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_12_rc1	a:1:{i:0;s:44:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_11";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_9_rc2	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_9_rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_12_rc3	a:1:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_12_rc2";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_8	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_8_rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_8_rc1	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_7_pl1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_7_pl1	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_7";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_2	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_2_rc2";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_7	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_7_rc2";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_14_rc1	a:1:{i:0;s:44:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_13";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_6_rc4	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_6_rc3";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_10_rc1	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_9";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_5	a:1:{i:0;s:52:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_5_rc1part2";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_12	a:1:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_12_rc3";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_11	a:1:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_11_rc2";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_9	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_9_rc4";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_3	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_3_rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_6_rc2	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_6_rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_0	a:0:{}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_6_rc3	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_6_rc2";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_1	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_1_rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_3_rc1	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_2";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_5_rc1part2	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_5_rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_13_pl1	a:1:{i:0;s:44:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_13";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_4	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_4_rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_9_rc3	a:1:{i:0;s:47:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_9_rc2";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_2_rc1	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_7_rc1	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_6";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v30x\\release_3_0_5_rc1	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_4";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\f_list_topics_permission_add	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v321";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\v323rc1	a:2:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v322";i:1;s:55:"\\phpbb\\db\\migration\\data\\v32x\\enable_accurate_pm_button";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\v323	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v32x\\v323rc2";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\user_notifications_table_unique_index	a:1:{i:0;s:72:"\\phpbb\\db\\migration\\data\\v32x\\user_notifications_table_remove_duplicates";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\merge_duplicate_bbcodes	a:0:{}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v33x\\v333rc1	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v33x\\v332";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\v324rc1	a:2:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v323";i:1;s:56:"\\phpbb\\db\\migration\\data\\v32x\\forum_topics_per_page_type";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\v3210rc2	a:2:{i:0;s:68:"\\phpbb\\db\\migration\\data\\v32x\\font_awesome_update_cdn_fix_depends_on";i:1;s:38:"\\phpbb\\db\\migration\\data\\v32x\\v3210rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\add_missing_config	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v329";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\cookie_notice_p2	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v320\\v320";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\v328	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v32x\\v328rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\font_awesome_update_cdn_fix_depends_on	a:2:{i:0;s:53:"\\phpbb\\db\\migration\\data\\v32x\\font_awesome_update_cdn";i:1;s:48:"\\phpbb\\db\\migration\\data\\v32x\\add_missing_config";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\disable_remote_avatar	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v325";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\enable_accurate_pm_button	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v322";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\user_notifications_table_index_p2	a:1:{i:0;s:63:"\\phpbb\\db\\migration\\data\\v32x\\user_notifications_table_index_p1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\forum_topics_per_page_type	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v323";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\v321	a:2:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v31x\\v3111";i:1;s:37:"\\phpbb\\db\\migration\\data\\v32x\\v321rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\v326	a:3:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v32x\\v326rc1";i:1;s:51:"\\phpbb\\db\\migration\\data\\v32x\\disable_remote_avatar";i:2;s:47:"\\phpbb\\db\\migration\\data\\v32x\\smtp_dynamic_data";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\font_awesome_update_cdn	a:1:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v32x\\add_missing_config";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\user_notifications_table_remove_duplicates	a:1:{i:0;s:65:"\\phpbb\\db\\migration\\data\\v32x\\user_notifications_table_temp_index";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\update_prosilver_bitfield	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v321";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\v325	a:2:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v32x\\v325rc1";i:1;s:43:"\\phpbb\\db\\migration\\data\\v32x\\jquery_update";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\v321rc1	a:4:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v320\\v320";i:1;s:38:"\\phpbb\\db\\migration\\data\\v31x\\v3111rc1";i:2;s:54:"\\phpbb\\db\\migration\\data\\v32x\\load_user_activity_limit";i:3;s:67:"\\phpbb\\db\\migration\\data\\v32x\\user_notifications_table_unique_index";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\v324	a:2:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v32x\\v324rc1";i:1;s:44:"\\phpbb\\db\\migration\\data\\v32x\\remove_imagick";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\fix_user_styles	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v320\\v320";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\load_user_activity_limit	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v320\\v320";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\add_plupload_config	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v329";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\user_notifications_table_index_p1	a:1:{i:0;s:46:"\\phpbb\\db\\migration\\data\\v32x\\cookie_notice_p2";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\timezone_p3	a:1:{i:0;s:38:"\\phpbb\\db\\migration\\data\\v310\\timezone";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\remove_imagick	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v32x\\v324rc1";}	1	1		1628342820	1628342820
\\phpbb\\db\\migration\\data\\v32x\\v322	a:2:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v31x\\v3112";i:1;s:37:"\\phpbb\\db\\migration\\data\\v32x\\v322rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\v3210rc1	a:3:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v329";i:1;s:49:"\\phpbb\\db\\migration\\data\\v32x\\add_plupload_config";i:2;s:53:"\\phpbb\\db\\migration\\data\\v32x\\font_awesome_update_cdn";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\email_force_sender	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v321";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\user_emoji_permission	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v32x\\v329rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\v3211	a:1:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v32x\\v3210";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\v327rc1	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v326";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\v323rc2	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v32x\\v323rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\jquery_update	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v32x\\v325rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\v322rc1	a:6:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v321";i:1;s:45:"\\phpbb\\db\\migration\\data\\v32x\\fix_user_styles";i:2;s:55:"\\phpbb\\db\\migration\\data\\v32x\\update_prosilver_bitfield";i:3;s:48:"\\phpbb\\db\\migration\\data\\v32x\\email_force_sender";i:4;s:58:"\\phpbb\\db\\migration\\data\\v32x\\f_list_topics_permission_add";i:5;s:53:"\\phpbb\\db\\migration\\data\\v32x\\merge_duplicate_bbcodes";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\user_notifications_table_reduce_column_sizes	a:1:{i:0;s:63:"\\phpbb\\db\\migration\\data\\v32x\\user_notifications_table_index_p3";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\v326rc1	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v325";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\v328rc1	a:2:{i:0;s:41:"\\phpbb\\db\\migration\\data\\v32x\\timezone_p3";i:1;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v327";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\v325rc1	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v324";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\v3210	a:1:{i:0;s:38:"\\phpbb\\db\\migration\\data\\v32x\\v3210rc2";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\user_notifications_table_index_p3	a:1:{i:0;s:63:"\\phpbb\\db\\migration\\data\\v32x\\user_notifications_table_index_p2";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\smtp_dynamic_data	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v32x\\v326rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\user_notifications_table_temp_index	a:1:{i:0;s:74:"\\phpbb\\db\\migration\\data\\v32x\\user_notifications_table_reduce_column_sizes";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\v327	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v32x\\v327rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\v329rc1	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v328";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v32x\\v329	a:2:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v32x\\v329rc1";i:1;s:51:"\\phpbb\\db\\migration\\data\\v32x\\user_emoji_permission";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\google_recaptcha_v3	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v330\\v330";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\remove_profilefield_aol	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v33x\\v331";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\fix_display_unapproved_posts_config	a:1:{i:0;s:65:"\\phpbb\\db\\migration\\data\\v330\\add_display_unapproved_posts_config";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\font_awesome_5_rollback	a:1:{i:0;s:51:"\\phpbb\\db\\migration\\data\\v33x\\font_awesome_5_update";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\add_notification_emails_table	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v330\\v330";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\v332rc1	a:1:{i:0;s:53:"\\phpbb\\db\\migration\\data\\v33x\\remove_profilefield_aol";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\v333	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v33x\\v333rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\font_awesome_5_update	a:2:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v330\\v330";i:1;s:53:"\\phpbb\\db\\migration\\data\\v32x\\font_awesome_update_cdn";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\v332	a:2:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v33x\\v332rc1";i:1;s:35:"\\phpbb\\db\\migration\\data\\v32x\\v3211";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\v334rc1	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v33x\\v333";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\v331rc1	a:8:{i:0;s:59:"\\phpbb\\db\\migration\\data\\v33x\\add_notification_emails_table";i:1;s:65:"\\phpbb\\db\\migration\\data\\v33x\\fix_display_unapproved_posts_config";i:2;s:40:"\\phpbb\\db\\migration\\data\\v33x\\bot_update";i:3;s:51:"\\phpbb\\db\\migration\\data\\v33x\\font_awesome_5_update";i:4;s:50:"\\phpbb\\db\\migration\\data\\v33x\\profilefield_cleanup";i:5;s:49:"\\phpbb\\db\\migration\\data\\v33x\\google_recaptcha_v3";i:6;s:57:"\\phpbb\\db\\migration\\data\\v33x\\default_search_return_chars";i:7;s:38:"\\phpbb\\db\\migration\\data\\v32x\\v3210rc2";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\v334	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v33x\\v334rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\profilefield_cleanup	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v330\\v330";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\v331	a:4:{i:0;s:53:"\\phpbb\\db\\migration\\data\\v33x\\font_awesome_5_rollback";i:1;s:43:"\\phpbb\\db\\migration\\data\\v33x\\jquery_update";i:2;s:35:"\\phpbb\\db\\migration\\data\\v32x\\v3210";i:3;s:37:"\\phpbb\\db\\migration\\data\\v33x\\v331rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\jquery_update	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v33x\\v331rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\default_search_return_chars	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v330\\v330";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v33x\\bot_update	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v330\\v330";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\report_id_auto_increment	a:1:{i:0;s:51:"\\phpbb\\db\\migration\\data\\v320\\default_data_type_ids";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\v320b1	a:4:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v31x\\v317pl1";i:1;s:36:"\\phpbb\\db\\migration\\data\\v320\\v320a2";i:2;s:57:"\\phpbb\\db\\migration\\data\\v31x\\increase_size_of_dateformat";i:3;s:51:"\\phpbb\\db\\migration\\data\\v320\\default_data_type_ids";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\v320a2	a:3:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v31x\\v317rc1";i:1;s:43:"\\phpbb\\db\\migration\\data\\v320\\text_reparser";i:2;s:36:"\\phpbb\\db\\migration\\data\\v320\\v320a1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\v320rc2	a:3:{i:0;s:57:"\\phpbb\\db\\migration\\data\\v31x\\remove_duplicate_migrations";i:1;s:48:"\\phpbb\\db\\migration\\data\\v31x\\add_log_time_index";i:2;s:44:"\\phpbb\\db\\migration\\data\\v320\\add_help_phpbb";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\icons_alt	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v320\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\v320a1	a:9:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v320\\dev";i:1;s:51:"\\phpbb\\db\\migration\\data\\v320\\allowed_schemes_links";i:2;s:56:"\\phpbb\\db\\migration\\data\\v320\\announce_global_permission";i:3;s:53:"\\phpbb\\db\\migration\\data\\v320\\remove_profilefield_wlm";i:4;s:49:"\\phpbb\\db\\migration\\data\\v320\\font_awesome_update";i:5;s:39:"\\phpbb\\db\\migration\\data\\v320\\icons_alt";i:6;s:41:"\\phpbb\\db\\migration\\data\\v320\\log_post_id";i:7;s:51:"\\phpbb\\db\\migration\\data\\v320\\remove_outdated_media";i:8;s:49:"\\phpbb\\db\\migration\\data\\v320\\notifications_board";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\announce_global_permission	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v320\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\dev	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v316";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\remote_upload_validation	a:1:{i:0;s:36:"\\phpbb\\db\\migration\\data\\v320\\v320a2";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\v320rc1	a:3:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v319";i:1;s:54:"\\phpbb\\db\\migration\\data\\v320\\report_id_auto_increment";i:2;s:36:"\\phpbb\\db\\migration\\data\\v320\\v320b2";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\log_post_id	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v320\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\font_awesome_update	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v320\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\default_data_type_ids	a:2:{i:0;s:36:"\\phpbb\\db\\migration\\data\\v320\\v320a2";i:1;s:42:"\\phpbb\\db\\migration\\data\\v320\\oauth_states";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\text_reparser	a:2:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v310\\contact_admin_form";i:1;s:51:"\\phpbb\\db\\migration\\data\\v320\\allowed_schemes_links";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\add_help_phpbb	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v320\\v320rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\notifications_board	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v320\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\oauth_states	a:1:{i:0;s:49:"\\phpbb\\db\\migration\\data\\v310\\auth_provider_oauth";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\cookie_notice	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v320\\v320rc2";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\remove_profilefield_wlm	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v320\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\remove_outdated_media	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v320\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\allowed_schemes_links	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v320\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\v320	a:2:{i:0;s:54:"\\phpbb\\db\\migration\\data\\v31x\\increase_size_of_emotion";i:1;s:43:"\\phpbb\\db\\migration\\data\\v320\\cookie_notice";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v320\\v320b2	a:3:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v318";i:1;s:36:"\\phpbb\\db\\migration\\data\\v320\\v320b1";i:2;s:54:"\\phpbb\\db\\migration\\data\\v320\\remote_upload_validation";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\notifications_use_full_name	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\rc3";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\extensions_version_check_force_unstable	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\migrations_table	a:0:{}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\notification_options_reconvert	a:1:{i:0;s:54:"\\phpbb\\db\\migration\\data\\v310\\notifications_schema_fix";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\live_searches_config	a:0:{}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_icq_cleanup	a:1:{i:0;s:46:"\\phpbb\\db\\migration\\data\\v310\\profilefield_icq";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_location	a:2:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v310\\profilefield_types";i:1;s:56:"\\phpbb\\db\\migration\\data\\v310\\profilefield_on_memberlist";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\passwords_p2	a:1:{i:0;s:39:"\\phpbb\\db\\migration\\data\\v310\\passwords";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\alpha3	a:4:{i:0;s:36:"\\phpbb\\db\\migration\\data\\v310\\alpha2";i:1;s:42:"\\phpbb\\db\\migration\\data\\v310\\avatar_types";i:2;s:39:"\\phpbb\\db\\migration\\data\\v310\\passwords";i:3;s:48:"\\phpbb\\db\\migration\\data\\v310\\profilefield_types";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\timezone	a:1:{i:0;s:44:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_11";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\alpha2	a:2:{i:0;s:36:"\\phpbb\\db\\migration\\data\\v310\\alpha1";i:1;s:51:"\\phpbb\\db\\migration\\data\\v310\\notifications_cron_p2";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\forgot_password	a:1:{i:0;s:44:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_11";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\extensions	a:1:{i:0;s:44:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_11";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_on_memberlist	a:1:{i:0;s:50:"\\phpbb\\db\\migration\\data\\v310\\profilefield_cleanup";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\signature_module_auth	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\search_type	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\boardindex	a:0:{}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\notifications_cron	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v310\\notifications";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_change_load_settings	a:1:{i:0;s:54:"\\phpbb\\db\\migration\\data\\v310\\profilefield_aol_cleanup";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\rc6	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\rc5";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_contact_field	a:1:{i:0;s:56:"\\phpbb\\db\\migration\\data\\v310\\profilefield_on_memberlist";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\notifications_cron_p2	a:1:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v310\\notifications_cron";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_show_novalue	a:1:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v310\\profilefield_types";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\passwords_convert_p2	a:1:{i:0;s:50:"\\phpbb\\db\\migration\\data\\v310\\passwords_convert_p1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\prune_shadow_topics	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\auth_provider_oauth2	a:1:{i:0;s:49:"\\phpbb\\db\\migration\\data\\v310\\auth_provider_oauth";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\namespaces	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\rc4	a:2:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\rc3";i:1;s:57:"\\phpbb\\db\\migration\\data\\v310\\notifications_use_full_name";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\jquery_update2	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v310\\jquery_update";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\alpha1	a:18:{i:0;s:46:"\\phpbb\\db\\migration\\data\\v30x\\local_url_bbcode";i:1;s:44:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_12";i:2;s:57:"\\phpbb\\db\\migration\\data\\v310\\acp_style_components_module";i:3;s:39:"\\phpbb\\db\\migration\\data\\v310\\allow_cdn";i:4;s:49:"\\phpbb\\db\\migration\\data\\v310\\auth_provider_oauth";i:5;s:37:"\\phpbb\\db\\migration\\data\\v310\\avatars";i:6;s:40:"\\phpbb\\db\\migration\\data\\v310\\boardindex";i:7;s:44:"\\phpbb\\db\\migration\\data\\v310\\config_db_text";i:8;s:45:"\\phpbb\\db\\migration\\data\\v310\\forgot_password";i:9;s:41:"\\phpbb\\db\\migration\\data\\v310\\mod_rewrite";i:10;s:49:"\\phpbb\\db\\migration\\data\\v310\\mysql_fulltext_drop";i:11;s:40:"\\phpbb\\db\\migration\\data\\v310\\namespaces";i:12;s:48:"\\phpbb\\db\\migration\\data\\v310\\notifications_cron";i:13;s:60:"\\phpbb\\db\\migration\\data\\v310\\notification_options_reconvert";i:14;s:38:"\\phpbb\\db\\migration\\data\\v310\\plupload";i:15;s:51:"\\phpbb\\db\\migration\\data\\v310\\signature_module_auth";i:16;s:52:"\\phpbb\\db\\migration\\data\\v310\\softdelete_mcp_modules";i:17;s:38:"\\phpbb\\db\\migration\\data\\v310\\teampage";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\allow_cdn	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v310\\jquery_update";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\avatar_types	a:2:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";i:1;s:37:"\\phpbb\\db\\migration\\data\\v310\\avatars";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\soft_delete_mod_convert	a:1:{i:0;s:36:"\\phpbb\\db\\migration\\data\\v310\\alpha3";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\dev	a:5:{i:0;s:40:"\\phpbb\\db\\migration\\data\\v310\\extensions";i:1;s:45:"\\phpbb\\db\\migration\\data\\v310\\style_update_p2";i:2;s:41:"\\phpbb\\db\\migration\\data\\v310\\timezone_p2";i:3;s:52:"\\phpbb\\db\\migration\\data\\v310\\reported_posts_display";i:4;s:46:"\\phpbb\\db\\migration\\data\\v310\\migrations_table";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\ucp_popuppm_module	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_aol	a:1:{i:0;s:56:"\\phpbb\\db\\migration\\data\\v310\\profilefield_yahoo_cleanup";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\beta1	a:7:{i:0;s:36:"\\phpbb\\db\\migration\\data\\v310\\alpha3";i:1;s:42:"\\phpbb\\db\\migration\\data\\v310\\passwords_p2";i:2;s:52:"\\phpbb\\db\\migration\\data\\v310\\postgres_fulltext_drop";i:3;s:63:"\\phpbb\\db\\migration\\data\\v310\\profilefield_change_load_settings";i:4;s:51:"\\phpbb\\db\\migration\\data\\v310\\profilefield_location";i:5;s:54:"\\phpbb\\db\\migration\\data\\v310\\soft_delete_mod_convert2";i:6;s:48:"\\phpbb\\db\\migration\\data\\v310\\ucp_popuppm_module";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\passwords_convert_p1	a:1:{i:0;s:42:"\\phpbb\\db\\migration\\data\\v310\\passwords_p2";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\timezone_p2	a:1:{i:0;s:38:"\\phpbb\\db\\migration\\data\\v310\\timezone";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_types	a:1:{i:0;s:36:"\\phpbb\\db\\migration\\data\\v310\\alpha2";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\acp_style_components_module	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\rc3	a:5:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\rc2";i:1;s:45:"\\phpbb\\db\\migration\\data\\v310\\captcha_plugins";i:2;s:53:"\\phpbb\\db\\migration\\data\\v310\\rename_too_long_indexes";i:3;s:41:"\\phpbb\\db\\migration\\data\\v310\\search_type";i:4;s:49:"\\phpbb\\db\\migration\\data\\v310\\topic_sort_username";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_twitter	a:3:{i:0;s:56:"\\phpbb\\db\\migration\\data\\v310\\profilefield_contact_field";i:1;s:55:"\\phpbb\\db\\migration\\data\\v310\\profilefield_show_novalue";i:2;s:48:"\\phpbb\\db\\migration\\data\\v310\\profilefield_types";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\board_contact_name	a:1:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v310\\beta2";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\acp_prune_users_module	a:1:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v310\\beta1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\auth_provider_oauth	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_0";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\teampage	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\softdelete_p2	a:2:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";i:1;s:43:"\\phpbb\\db\\migration\\data\\v310\\softdelete_p1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\reported_posts_display	a:1:{i:0;s:44:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_11";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_location_cleanup	a:1:{i:0;s:51:"\\phpbb\\db\\migration\\data\\v310\\profilefield_location";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_youtube	a:3:{i:0;s:56:"\\phpbb\\db\\migration\\data\\v310\\profilefield_contact_field";i:1;s:55:"\\phpbb\\db\\migration\\data\\v310\\profilefield_show_novalue";i:2;s:48:"\\phpbb\\db\\migration\\data\\v310\\profilefield_types";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\notifications_schema_fix	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v310\\notifications";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_wlm	a:1:{i:0;s:58:"\\phpbb\\db\\migration\\data\\v310\\profilefield_website_cleanup";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_interests	a:2:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v310\\profilefield_types";i:1;s:55:"\\phpbb\\db\\migration\\data\\v310\\profilefield_show_novalue";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\gold	a:2:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\rc6";i:1;s:40:"\\phpbb\\db\\migration\\data\\v310\\bot_update";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\soft_delete_mod_convert2	a:1:{i:0;s:53:"\\phpbb\\db\\migration\\data\\v310\\soft_delete_mod_convert";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\mysql_fulltext_drop	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_skype	a:3:{i:0;s:56:"\\phpbb\\db\\migration\\data\\v310\\profilefield_contact_field";i:1;s:55:"\\phpbb\\db\\migration\\data\\v310\\profilefield_show_novalue";i:2;s:48:"\\phpbb\\db\\migration\\data\\v310\\profilefield_types";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_cleanup	a:2:{i:0;s:52:"\\phpbb\\db\\migration\\data\\v310\\profilefield_interests";i:1;s:53:"\\phpbb\\db\\migration\\data\\v310\\profilefield_occupation";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\style_update_p2	a:1:{i:0;s:45:"\\phpbb\\db\\migration\\data\\v310\\style_update_p1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_aol_cleanup	a:1:{i:0;s:46:"\\phpbb\\db\\migration\\data\\v310\\profilefield_aol";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_icq	a:1:{i:0;s:56:"\\phpbb\\db\\migration\\data\\v310\\profilefield_contact_field";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\contact_admin_form	a:1:{i:0;s:44:"\\phpbb\\db\\migration\\data\\v310\\config_db_text";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\beta2	a:3:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v310\\beta1";i:1;s:52:"\\phpbb\\db\\migration\\data\\v310\\acp_prune_users_module";i:2;s:59:"\\phpbb\\db\\migration\\data\\v310\\profilefield_location_cleanup";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_yahoo	a:1:{i:0;s:54:"\\phpbb\\db\\migration\\data\\v310\\profilefield_wlm_cleanup";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\rc1	a:9:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v310\\beta4";i:1;s:54:"\\phpbb\\db\\migration\\data\\v310\\contact_admin_acp_module";i:2;s:48:"\\phpbb\\db\\migration\\data\\v310\\contact_admin_form";i:3;s:50:"\\phpbb\\db\\migration\\data\\v310\\passwords_convert_p2";i:4;s:51:"\\phpbb\\db\\migration\\data\\v310\\profilefield_facebook";i:5;s:53:"\\phpbb\\db\\migration\\data\\v310\\profilefield_googleplus";i:6;s:48:"\\phpbb\\db\\migration\\data\\v310\\profilefield_skype";i:7;s:50:"\\phpbb\\db\\migration\\data\\v310\\profilefield_twitter";i:8;s:50:"\\phpbb\\db\\migration\\data\\v310\\profilefield_youtube";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\rc5	a:3:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\rc4";i:1;s:66:"\\phpbb\\db\\migration\\data\\v310\\profilefield_field_validation_length";i:2;s:53:"\\phpbb\\db\\migration\\data\\v310\\remove_acp_styles_cache";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\rc2	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\jquery_update	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_wlm_cleanup	a:1:{i:0;s:46:"\\phpbb\\db\\migration\\data\\v310\\profilefield_wlm";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\config_db_text	a:1:{i:0;s:44:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_11";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\rename_too_long_indexes	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_0";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\captcha_plugins	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\rc2";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\softdelete_p1	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\reset_missing_captcha_plugin	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\contact_admin_acp_module	a:0:{}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\postgres_fulltext_drop	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_occupation	a:1:{i:0;s:52:"\\phpbb\\db\\migration\\data\\v310\\profilefield_interests";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_field_validation_length	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\rc3";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\notifications	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\beta4	a:3:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v310\\beta3";i:1;s:69:"\\phpbb\\db\\migration\\data\\v310\\extensions_version_check_force_unstable";i:2;s:58:"\\phpbb\\db\\migration\\data\\v310\\reset_missing_captcha_plugin";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\softdelete_mcp_modules	a:2:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";i:1;s:43:"\\phpbb\\db\\migration\\data\\v310\\softdelete_p2";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\mod_rewrite	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\plupload	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\avatars	a:1:{i:0;s:44:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_11";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\topic_sort_username	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\style_update_p1	a:1:{i:0;s:44:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_11";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_facebook	a:3:{i:0;s:56:"\\phpbb\\db\\migration\\data\\v310\\profilefield_contact_field";i:1;s:55:"\\phpbb\\db\\migration\\data\\v310\\profilefield_show_novalue";i:2;s:48:"\\phpbb\\db\\migration\\data\\v310\\profilefield_types";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\remove_acp_styles_cache	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\rc4";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_website	a:2:{i:0;s:56:"\\phpbb\\db\\migration\\data\\v310\\profilefield_on_memberlist";i:1;s:54:"\\phpbb\\db\\migration\\data\\v310\\profilefield_icq_cleanup";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\bot_update	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v310\\rc6";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_yahoo_cleanup	a:1:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v310\\profilefield_yahoo";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\beta3	a:6:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v310\\beta2";i:1;s:50:"\\phpbb\\db\\migration\\data\\v310\\auth_provider_oauth2";i:2;s:48:"\\phpbb\\db\\migration\\data\\v310\\board_contact_name";i:3;s:44:"\\phpbb\\db\\migration\\data\\v310\\jquery_update2";i:4;s:50:"\\phpbb\\db\\migration\\data\\v310\\live_searches_config";i:5;s:49:"\\phpbb\\db\\migration\\data\\v310\\prune_shadow_topics";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_googleplus	a:3:{i:0;s:56:"\\phpbb\\db\\migration\\data\\v310\\profilefield_contact_field";i:1;s:55:"\\phpbb\\db\\migration\\data\\v310\\profilefield_show_novalue";i:2;s:48:"\\phpbb\\db\\migration\\data\\v310\\profilefield_types";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\profilefield_website_cleanup	a:1:{i:0;s:50:"\\phpbb\\db\\migration\\data\\v310\\profilefield_website";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v310\\passwords	a:1:{i:0;s:44:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_11";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\style_update	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v310\\gold";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\plupload_last_gc_dynamic	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v312";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v312rc1	a:2:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v311";i:1;s:49:"\\phpbb\\db\\migration\\data\\v31x\\m_softdelete_global";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\update_custom_bbcodes_with_idn	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v312";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v311	a:2:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v310\\gold";i:1;s:42:"\\phpbb\\db\\migration\\data\\v31x\\style_update";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v315rc1	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v314";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\add_log_time_index	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v319";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\profilefield_yahoo_update_url	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v312";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v315	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v31x\\v315rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v3111rc1	a:8:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v31x\\v3110";i:1;s:48:"\\phpbb\\db\\migration\\data\\v31x\\add_log_time_index";i:2;s:54:"\\phpbb\\db\\migration\\data\\v31x\\increase_size_of_emotion";i:3;s:67:"\\phpbb\\db\\migration\\data\\v31x\\add_jabber_ssl_context_config_options";i:4;s:65:"\\phpbb\\db\\migration\\data\\v31x\\add_smtp_ssl_context_config_options";i:5;s:43:"\\phpbb\\db\\migration\\data\\v31x\\update_hashes";i:6;s:57:"\\phpbb\\db\\migration\\data\\v31x\\remove_duplicate_migrations";i:7;s:53:"\\phpbb\\db\\migration\\data\\v31x\\add_latest_topics_index";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v318rc1	a:2:{i:0;s:57:"\\phpbb\\db\\migration\\data\\v31x\\increase_size_of_dateformat";i:1;s:37:"\\phpbb\\db\\migration\\data\\v31x\\v317pl1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v313rc2	a:2:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_13_pl1";i:1;s:37:"\\phpbb\\db\\migration\\data\\v31x\\v313rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v316rc1	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v315";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v317pl1	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v317";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v3111	a:1:{i:0;s:38:"\\phpbb\\db\\migration\\data\\v31x\\v3111rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\increase_size_of_emotion	a:1:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v31x\\v3110";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v314rc2	a:2:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_14_rc1";i:1;s:37:"\\phpbb\\db\\migration\\data\\v31x\\v314rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\m_pm_report	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v31x\\v316rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v313rc1	a:5:{i:0;s:48:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_13_rc1";i:1;s:54:"\\phpbb\\db\\migration\\data\\v31x\\plupload_last_gc_dynamic";i:2;s:71:"\\phpbb\\db\\migration\\data\\v31x\\profilefield_remove_underscore_from_alpha";i:3;s:59:"\\phpbb\\db\\migration\\data\\v31x\\profilefield_yahoo_update_url";i:4;s:60:"\\phpbb\\db\\migration\\data\\v31x\\update_custom_bbcodes_with_idn";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v3110	a:1:{i:0;s:38:"\\phpbb\\db\\migration\\data\\v31x\\v3110rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v312	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v31x\\v312rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v3112	a:1:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v31x\\v3111";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v319rc1	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v318";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\update_hashes	a:1:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v31x\\v3110";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\increase_size_of_dateformat	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v317";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\add_jabber_ssl_context_config_options	a:1:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v31x\\v3110";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v317rc1	a:2:{i:0;s:41:"\\phpbb\\db\\migration\\data\\v31x\\m_pm_report";i:1;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v316";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v314rc1	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v313";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v3110rc1	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v319";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v318	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v31x\\v318rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v317	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v31x\\v317rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v316	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v31x\\v316rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v319	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v31x\\v319rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\m_softdelete_global	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v311";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\remove_duplicate_migrations	a:1:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v31x\\v3110";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v314	a:2:{i:0;s:44:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_14";i:1;s:37:"\\phpbb\\db\\migration\\data\\v31x\\v314rc2";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\profilefield_remove_underscore_from_alpha	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v311";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\v313	a:1:{i:0;s:37:"\\phpbb\\db\\migration\\data\\v31x\\v313rc2";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\add_latest_topics_index	a:1:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v31x\\v3110";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v31x\\add_smtp_ssl_context_config_options	a:1:{i:0;s:35:"\\phpbb\\db\\migration\\data\\v31x\\v3110";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v330\\v330b2	a:4:{i:0;s:65:"\\phpbb\\db\\migration\\data\\v330\\add_display_unapproved_posts_config";i:1;s:49:"\\phpbb\\db\\migration\\data\\v330\\forums_legend_limit";i:2;s:47:"\\phpbb\\db\\migration\\data\\v330\\remove_email_hash";i:3;s:36:"\\phpbb\\db\\migration\\data\\v330\\v330b1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v330\\v330	a:2:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v329";i:1;s:37:"\\phpbb\\db\\migration\\data\\v330\\v330rc1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v330\\remove_attachment_flash	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v330\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v330\\remove_max_pass_chars	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v330\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v330\\reset_password	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v330\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v330\\forums_legend_limit	a:1:{i:0;s:36:"\\phpbb\\db\\migration\\data\\v330\\v330b1";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v330\\remove_email_hash	a:1:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v30x\\release_3_0_0";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v330\\dev	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v327";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v330\\v330b1	a:6:{i:0;s:43:"\\phpbb\\db\\migration\\data\\v330\\jquery_update";i:1;s:44:"\\phpbb\\db\\migration\\data\\v330\\reset_password";i:2;s:53:"\\phpbb\\db\\migration\\data\\v330\\remove_attachment_flash";i:3;s:51:"\\phpbb\\db\\migration\\data\\v330\\remove_max_pass_chars";i:4;s:34:"\\phpbb\\db\\migration\\data\\v32x\\v328";i:5;s:33:"\\phpbb\\db\\migration\\data\\v330\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v330\\add_display_unapproved_posts_config	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v330\\dev";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v330\\v330rc1	a:1:{i:0;s:36:"\\phpbb\\db\\migration\\data\\v330\\v330b2";}	1	1		1628342821	1628342821
\\phpbb\\db\\migration\\data\\v330\\jquery_update	a:1:{i:0;s:33:"\\phpbb\\db\\migration\\data\\v330\\dev";}	1	1		1628342821	1628342821
\\phpbb\\viglink\\migrations\\viglink_data_v2	a:1:{i:0;s:38:"\\phpbb\\viglink\\migrations\\viglink_data";}	1	1		1628342823	1628342823
\\phpbb\\viglink\\migrations\\viglink_ask_admin	a:1:{i:0;s:41:"\\phpbb\\viglink\\migrations\\viglink_data_v2";}	1	1		1628342823	1628342823
\\phpbb\\viglink\\migrations\\viglink_ask_admin_wait	a:1:{i:0;s:43:"\\phpbb\\viglink\\migrations\\viglink_ask_admin";}	1	1		1628342823	1628342823
\\phpbb\\viglink\\migrations\\viglink_cron	a:1:{i:0;s:38:"\\phpbb\\viglink\\migrations\\viglink_data";}	1	1		0	0
\\phpbb\\viglink\\migrations\\viglink_data	a:1:{i:0;s:34:"\\phpbb\\db\\migration\\data\\v31x\\v312";}	1	1		1628342823	1628342823
\.


--
-- Data for Name: phpbb_moderator_cache; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_moderator_cache (forum_id, user_id, username, group_id, group_name, display_on_index) FROM stdin;
\.


--
-- Data for Name: phpbb_modules; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_modules (module_id, module_enabled, module_display, module_basename, module_class, parent_id, left_id, right_id, module_langname, module_mode, module_auth) FROM stdin;
4	1	1		acp	1	46	53	ACP_CLIENT_COMMUNICATION		
53	1	1	acp_board	acp	4	47	48	ACP_AUTH_SETTINGS	auth	acl_a_server
54	1	1	acp_board	acp	4	49	50	ACP_EMAIL_SETTINGS	email	acl_a_server
5	1	1		acp	1	54	67	ACP_SERVER_CONFIGURATION		
55	1	1	acp_board	acp	5	55	56	ACP_COOKIE_SETTINGS	cookie	acl_a_server
56	1	1	acp_board	acp	5	57	58	ACP_SERVER_SETTINGS	server	acl_a_server
57	1	1	acp_board	acp	5	59	60	ACP_SECURITY_SETTINGS	security	acl_a_server
58	1	1	acp_board	acp	5	61	62	ACP_LOAD_SETTINGS	load	acl_a_server
81	1	1	acp_main	acp	1	2	3	ACP_INDEX	main	
33	1	1	acp_attachments	acp	3	19	20	ACP_ATTACHMENT_SETTINGS	attach	acl_a_attach
43	1	1	acp_board	acp	3	21	22	ACP_BOARD_SETTINGS	settings	acl_a_board
44	1	1	acp_board	acp	3	23	24	ACP_BOARD_FEATURES	features	acl_a_board
45	1	1	acp_board	acp	3	25	26	ACP_AVATAR_SETTINGS	avatar	acl_a_board
46	1	1	acp_board	acp	3	27	28	ACP_MESSAGE_SETTINGS	message	acl_a_board
48	1	1	acp_board	acp	3	29	30	ACP_POST_SETTINGS	post	acl_a_board
50	1	1	acp_board	acp	3	31	32	ACP_SIGNATURE_SETTINGS	signature	acl_a_board
51	1	1	acp_board	acp	3	33	34	ACP_FEED_SETTINGS	feed	acl_a_board
52	1	1	acp_board	acp	3	35	36	ACP_REGISTER_SETTINGS	registration	acl_a_board
60	1	1	acp_captcha	acp	3	37	38	ACP_VC_SETTINGS	visual	acl_a_board
61	1	0	acp_captcha	acp	3	39	40	ACP_VC_CAPTCHA_DISPLAY	img	acl_a_board
2	1	1		acp	1	4	17	ACP_QUICK_ACCESS		
75	1	1	acp_jabber	acp	4	51	52	ACP_JABBER_SETTINGS	settings	acl_a_jabber
71	1	1	acp_help_phpbb	acp	5	63	64	ACP_HELP_PHPBB	help_phpbb	acl_a_server
115	1	1	acp_search	acp	5	65	66	ACP_SEARCH_SETTINGS	settings	acl_a_search
131	1	1	acp_words	acp	10	101	102	ACP_WORDS	words	acl_a_words
38	1	1	acp_attachments	acp	11	113	114	ACP_MANAGE_ATTACHMENTS	manage	acl_a_attach
12	1	1		acp	0	117	174	ACP_CAT_USERGROUP		
13	1	1		acp	12	118	153	ACP_CAT_USERS		
74	1	1	acp_inactive	acp	13	119	120	ACP_INACTIVE_USERS	list	acl_a_user
120	1	1	acp_users	acp	13	121	122	ACP_MANAGE_USERS	overview	acl_a_user
110	1	1	acp_profile	acp	13	127	128	ACP_CUSTOM_PROFILE_FIELDS	profile	acl_a_profile
3	1	1		acp	1	18	45	ACP_BOARD_CONFIGURATION		
132	1	1	acp_users	acp	2	5	6	ACP_MANAGE_USERS	overview	acl_a_user
62	1	1	acp_contact	acp	3	41	42	ACP_CONTACT_SETTINGS	contact	acl_a_board
68	1	1	acp_forums	acp	7	71	72	ACP_MANAGE_FORUMS	manage	acl_a_forum
133	1	1	acp_groups	acp	2	7	8	ACP_GROUPS_MANAGE	manage	acl_a_group
111	1	1	acp_prune	acp	7	73	74	ACP_PRUNE_FORUMS	forums	acl_a_prune
11	1	1		acp	9	104	115	ACP_ATTACHMENTS		
134	1	1	acp_forums	acp	2	9	10	ACP_MANAGE_FORUMS	manage	acl_a_forum
42	1	1	acp_bbcodes	acp	10	91	92	ACP_BBCODES	bbcodes	acl_a_bbcode
47	1	1	acp_board	acp	10	93	94	ACP_MESSAGE_SETTINGS	message	acl_a_board
49	1	1	acp_board	acp	10	95	96	ACP_POST_SETTINGS	post	acl_a_board
135	1	1	acp_logs	acp	2	11	12	ACP_MOD_LOGS	mod	acl_a_viewlogs
72	1	1	acp_icons	acp	10	97	98	ACP_ICONS	icons	acl_a_icons
73	1	1	acp_icons	acp	10	99	100	ACP_SMILIES	smilies	acl_a_icons
34	1	1	acp_attachments	acp	11	105	106	ACP_ATTACHMENT_SETTINGS	attach	acl_a_attach
35	1	1	acp_attachments	acp	11	107	108	ACP_MANAGE_EXTENSIONS	extensions	acl_a_attach
36	1	1	acp_attachments	acp	11	109	110	ACP_EXTENSION_GROUPS	ext_groups	acl_a_attach
37	1	1	acp_attachments	acp	11	111	112	ACP_ORPHAN_ATTACHMENTS	orphan	acl_a_attach
136	1	1	acp_bots	acp	2	13	14	ACP_BOTS	bots	acl_a_bots
95	1	1	acp_permissions	acp	13	123	124	ACP_USERS_PERMISSIONS	setting_user_global	acl_a_authusers && (acl_a_aauth || acl_a_mauth || acl_a_uauth)
97	1	1	acp_permissions	acp	13	125	126	ACP_USERS_FORUM_PERMISSIONS	setting_user_local	acl_a_authusers && (acl_a_mauth || acl_a_fauth)
112	1	1	acp_prune	acp	13	129	130	ACP_PRUNE_USERS	users	acl_a_userdel
113	1	1	acp_ranks	acp	13	131	132	ACP_MANAGE_RANKS	ranks	acl_a_ranks
121	1	0	acp_users	acp	13	133	134	ACP_USER_FEEDBACK	feedback	acl_a_user
122	1	0	acp_users	acp	13	135	136	ACP_USER_WARNINGS	warnings	acl_a_user
123	1	0	acp_users	acp	13	137	138	ACP_USER_PROFILE	profile	acl_a_user
124	1	0	acp_users	acp	13	139	140	ACP_USER_PREFS	prefs	acl_a_user
125	1	0	acp_users	acp	13	141	142	ACP_USER_AVATAR	avatar	acl_a_user
126	1	0	acp_users	acp	13	143	144	ACP_USER_RANK	rank	acl_a_user
129	1	0	acp_users	acp	13	149	150	ACP_USER_PERM	perm	acl_a_user && acl_a_viewauth
137	1	1	acp_php_info	acp	2	15	16	ACP_PHP_INFO	info	acl_a_phpinfo
130	1	0	acp_users	acp	13	151	152	ACP_USER_ATTACH	attach	acl_a_user
14	1	1		acp	12	154	163	ACP_GROUPS		
69	1	1	acp_groups	acp	14	155	156	ACP_GROUPS_MANAGE	manage	acl_a_group
99	1	1	acp_permissions	acp	14	159	160	ACP_GROUPS_PERMISSIONS	setting_group_global	acl_a_authgroups && (acl_a_aauth || acl_a_mauth || acl_a_uauth)
15	1	1		acp	12	164	173	ACP_USER_SECURITY		
39	1	1	acp_ban	acp	15	165	166	ACP_BAN_EMAILS	email	acl_a_ban
40	1	1	acp_ban	acp	15	167	168	ACP_BAN_IPS	ip	acl_a_ban
41	1	1	acp_ban	acp	15	169	170	ACP_BAN_USERNAMES	user	acl_a_ban
65	1	1	acp_disallow	acp	15	171	172	ACP_DISALLOW_USERNAMES	usernames	acl_a_names
16	1	1		acp	0	175	224	ACP_CAT_PERMISSIONS		
89	1	1	acp_permissions	acp	16	176	177	ACP_PERMISSIONS	intro	acl_a_authusers || acl_a_authgroups || acl_a_viewauth
17	1	1		acp	16	178	187	ACP_GLOBAL_PERMISSIONS		
94	1	1	acp_permissions	acp	17	179	180	ACP_USERS_PERMISSIONS	setting_user_global	acl_a_authusers && (acl_a_aauth || acl_a_mauth || acl_a_uauth)
98	1	1	acp_permissions	acp	17	181	182	ACP_GROUPS_PERMISSIONS	setting_group_global	acl_a_authgroups && (acl_a_aauth || acl_a_mauth || acl_a_uauth)
18	1	1		acp	16	188	199	ACP_FORUM_BASED_PERMISSIONS		
91	1	1	acp_permissions	acp	18	189	190	ACP_FORUM_PERMISSIONS	setting_forum_local	acl_a_fauth && (acl_a_authusers || acl_a_authgroups)
92	1	1	acp_permissions	acp	18	191	192	ACP_FORUM_PERMISSIONS_COPY	setting_forum_copy	acl_a_fauth && acl_a_authusers && acl_a_authgroups && acl_a_mauth
93	1	1	acp_permissions	acp	18	193	194	ACP_FORUM_MODERATORS	setting_mod_local	acl_a_mauth && (acl_a_authusers || acl_a_authgroups)
96	1	1	acp_permissions	acp	18	195	196	ACP_USERS_FORUM_PERMISSIONS	setting_user_local	acl_a_authusers && (acl_a_mauth || acl_a_fauth)
100	1	1	acp_permissions	acp	18	197	198	ACP_GROUPS_FORUM_PERMISSIONS	setting_group_local	acl_a_authgroups && (acl_a_mauth || acl_a_fauth)
138	1	1	acp_permissions	acp	8	77	78	ACP_FORUM_PERMISSIONS	setting_forum_local	acl_a_fauth && (acl_a_authusers || acl_a_authgroups)
139	1	1	acp_permissions	acp	8	79	80	ACP_FORUM_PERMISSIONS_COPY	setting_forum_copy	acl_a_fauth && acl_a_authusers && acl_a_authgroups && acl_a_mauth
70	1	1	acp_groups	acp	14	157	158	ACP_GROUPS_POSITION	position	acl_a_group
101	1	1	acp_permissions	acp	14	161	162	ACP_GROUPS_FORUM_PERMISSIONS	setting_group_local	acl_a_authgroups && (acl_a_mauth || acl_a_fauth)
102	1	1	acp_permissions	acp	17	183	184	ACP_ADMINISTRATORS	setting_admin_global	acl_a_aauth && (acl_a_authusers || acl_a_authgroups)
103	1	1	acp_permissions	acp	17	185	186	ACP_GLOBAL_MODERATORS	setting_mod_global	acl_a_mauth && (acl_a_authusers || acl_a_authgroups)
19	1	1		acp	16	200	209	ACP_PERMISSION_ROLES		
85	1	1	acp_permission_roles	acp	19	201	202	ACP_ADMIN_ROLES	admin_roles	acl_a_roles && acl_a_aauth
86	1	1	acp_permission_roles	acp	19	203	204	ACP_USER_ROLES	user_roles	acl_a_roles && acl_a_uauth
87	1	1	acp_permission_roles	acp	19	205	206	ACP_MOD_ROLES	mod_roles	acl_a_roles && acl_a_mauth
88	1	1	acp_permission_roles	acp	19	207	208	ACP_FORUM_ROLES	forum_roles	acl_a_roles && acl_a_fauth
20	1	1		acp	16	210	223	ACP_PERMISSION_MASKS		
90	1	0	acp_permissions	acp	20	211	212	ACP_PERMISSION_TRACE	trace	acl_a_viewauth
26	1	1		acp	25	242	251	ACP_FORUM_LOGS		
156	1	1	mcp_main	mcp	143	2	3	MCP_MAIN_FRONT	front	
157	1	1	mcp_main	mcp	143	4	5	MCP_MAIN_FORUM_VIEW	forum_view	acl_m_,$id
158	1	1	mcp_main	mcp	143	6	7	MCP_MAIN_TOPIC_VIEW	topic_view	acl_m_,$id
143	1	1		mcp	0	1	10	MCP_MAIN		
159	1	1	mcp_main	mcp	143	8	9	MCP_MAIN_POST_DETAILS	post_details	acl_m_,$id || (!$id && aclf_m_)
144	1	1		mcp	0	11	22	MCP_QUEUE		
165	1	1	mcp_queue	mcp	144	12	13	MCP_QUEUE_UNAPPROVED_TOPICS	unapproved_topics	aclf_m_approve
166	1	1	mcp_queue	mcp	144	14	15	MCP_QUEUE_UNAPPROVED_POSTS	unapproved_posts	aclf_m_approve
167	1	1	mcp_queue	mcp	144	16	17	MCP_QUEUE_DELETED_TOPICS	deleted_topics	aclf_m_approve
168	1	1	mcp_queue	mcp	144	18	19	MCP_QUEUE_DELETED_POSTS	deleted_posts	aclf_m_approve
169	1	1	mcp_queue	mcp	144	20	21	MCP_QUEUE_APPROVE_DETAILS	approve_details	acl_m_approve,$id || (!$id && aclf_m_approve)
146	1	1		mcp	0	37	42	MCP_NOTES		
160	1	1	mcp_notes	mcp	146	38	39	MCP_NOTES_FRONT	front	
161	1	1	mcp_notes	mcp	146	40	41	MCP_NOTES_USER	user_notes	
145	1	1		mcp	0	23	36	MCP_REPORTS		
173	1	1	mcp_warn	mcp	147	44	45	MCP_WARN_FRONT	front	aclf_m_warn
174	1	1	mcp_warn	mcp	147	46	47	MCP_WARN_LIST	list	aclf_m_warn
175	1	1	mcp_warn	mcp	147	48	49	MCP_WARN_USER	warn_user	aclf_m_warn
148	1	1		mcp	0	53	60	MCP_LOGS		
153	1	1	mcp_logs	mcp	148	54	55	MCP_LOGS_FRONT	front	acl_m_ || aclf_m_
154	1	1	mcp_logs	mcp	148	56	57	MCP_LOGS_FORUM_VIEW	forum_logs	acl_m_,$id
155	1	1	mcp_logs	mcp	148	58	59	MCP_LOGS_TOPIC_VIEW	topic_logs	acl_m_,$id
149	1	1		mcp	0	61	68	MCP_BAN		
150	1	1	mcp_ban	mcp	149	62	63	MCP_BAN_USERNAMES	user	acl_m_ban
151	1	1	mcp_ban	mcp	149	64	65	MCP_BAN_IPS	ip	acl_m_ban
152	1	1	mcp_ban	mcp	149	66	67	MCP_BAN_EMAILS	email	acl_m_ban
147	1	1		mcp	0	43	52	MCP_WARN		
176	1	1	mcp_warn	mcp	147	50	51	MCP_WARN_POST	warn_post	acl_m_warn && acl_f_read,$id
164	1	1	mcp_pm_reports	mcp	145	34	35	MCP_PM_REPORT_DETAILS	pm_report_details	acl_m_pm_report
163	1	1	mcp_pm_reports	mcp	145	32	33	MCP_PM_REPORTS_CLOSED	pm_reports_closed	acl_m_pm_report
162	1	1	mcp_pm_reports	mcp	145	30	31	MCP_PM_REPORTS_OPEN	pm_reports	acl_m_pm_report
170	1	1	mcp_reports	mcp	145	24	25	MCP_REPORTS_OPEN	reports	aclf_m_report
171	1	1	mcp_reports	mcp	145	26	27	MCP_REPORTS_CLOSED	reports_closed	aclf_m_report
172	1	1	mcp_reports	mcp	145	28	29	MCP_REPORT_DETAILS	report_details	acl_m_report,$id || (!$id && aclf_m_report)
177	1	1		ucp	0	1	14	UCP_MAIN		
192	1	1	ucp_notifications	ucp	177	12	13	UCP_NOTIFICATION_LIST	notification_list	cfg_allow_board_notifications
179	1	1		ucp	0	29	38	UCP_PREFS		
180	1	1	ucp_pm	ucp	0	39	48	UCP_PM		
193	1	0	ucp_pm	ucp	180	40	41	UCP_PM_VIEW	view	cfg_allow_privmsg
194	1	1	ucp_pm	ucp	180	42	43	UCP_PM_COMPOSE	compose	cfg_allow_privmsg
195	1	1	ucp_pm	ucp	180	44	45	UCP_PM_DRAFTS	drafts	cfg_allow_privmsg
196	1	1	ucp_pm	ucp	180	46	47	UCP_PM_OPTIONS	options	cfg_allow_privmsg
181	1	1		ucp	0	49	54	UCP_USERGROUPS		
185	1	1	ucp_groups	ucp	181	50	51	UCP_USERGROUPS_MEMBER	membership	
186	1	1	ucp_groups	ucp	181	52	53	UCP_USERGROUPS_MANAGE	manage	
178	1	1		ucp	0	15	28	UCP_PROFILE		
205	1	1	ucp_zebra	ucp	182	56	57	UCP_ZEBRA_FRIENDS	friends	
182	1	1		ucp	0	55	60	UCP_ZEBRA		
206	1	1	ucp_zebra	ucp	182	58	59	UCP_ZEBRA_FOES	foes	
183	1	1	ucp_attachments	ucp	177	10	11	UCP_MAIN_ATTACHMENTS	attachments	acl_u_attach
187	1	1	ucp_main	ucp	177	2	3	UCP_MAIN_FRONT	front	
188	1	1	ucp_main	ucp	177	4	5	UCP_MAIN_SUBSCRIBED	subscribed	
189	1	1	ucp_main	ucp	177	6	7	UCP_MAIN_BOOKMARKS	bookmarks	cfg_allow_bookmarks
190	1	1	ucp_main	ucp	177	8	9	UCP_MAIN_DRAFTS	drafts	
191	1	1	ucp_notifications	ucp	179	36	37	UCP_NOTIFICATION_OPTIONS	notification_options	
197	1	1	ucp_prefs	ucp	179	30	31	UCP_PREFS_PERSONAL	personal	
198	1	1	ucp_prefs	ucp	179	32	33	UCP_PREFS_POST	post	
199	1	1	ucp_prefs	ucp	179	34	35	UCP_PREFS_VIEW	view	
184	1	1	ucp_auth_link	ucp	178	26	27	UCP_AUTH_LINK_MANAGE	auth_link	authmethod_oauth
200	1	1	ucp_profile	ucp	178	16	17	UCP_PROFILE_PROFILE_INFO	profile_info	acl_u_chgprofileinfo
201	1	1	ucp_profile	ucp	178	18	19	UCP_PROFILE_SIGNATURE	signature	acl_u_sig
202	1	1	ucp_profile	ucp	178	20	21	UCP_PROFILE_AVATAR	avatar	cfg_allow_avatar
203	1	1	ucp_profile	ucp	178	22	23	UCP_PROFILE_REG_DETAILS	reg_details	
204	1	1	ucp_profile	ucp	178	24	25	UCP_PROFILE_AUTOLOGIN_KEYS	autologin_keys	
6	1	1		acp	0	69	88	ACP_CAT_FORUMS		
7	1	1		acp	6	70	75	ACP_MANAGE_FORUMS		
8	1	1		acp	6	76	87	ACP_FORUM_BASED_PERMISSIONS		
140	1	1	acp_permissions	acp	8	81	82	ACP_FORUM_MODERATORS	setting_mod_local	acl_a_mauth && (acl_a_authusers || acl_a_authgroups)
141	1	1	acp_permissions	acp	8	83	84	ACP_USERS_FORUM_PERMISSIONS	setting_user_local	acl_a_authusers && (acl_a_mauth || acl_a_fauth)
142	1	1	acp_permissions	acp	8	85	86	ACP_GROUPS_FORUM_PERMISSIONS	setting_group_local	acl_a_authgroups && (acl_a_mauth || acl_a_fauth)
9	1	1		acp	0	89	116	ACP_CAT_POSTING		
10	1	1		acp	9	90	103	ACP_MESSAGES		
127	1	0	acp_users	acp	13	145	146	ACP_USER_SIG	sig	acl_a_user
128	1	0	acp_users	acp	13	147	148	ACP_USER_GROUPS	groups	acl_a_user && acl_a_group
104	1	1	acp_permissions	acp	20	213	214	ACP_VIEW_ADMIN_PERMISSIONS	view_admin_global	acl_a_viewauth
105	1	1	acp_permissions	acp	20	215	216	ACP_VIEW_USER_PERMISSIONS	view_user_global	acl_a_viewauth
106	1	1	acp_permissions	acp	20	217	218	ACP_VIEW_GLOBAL_MOD_PERMISSIONS	view_mod_global	acl_a_viewauth
107	1	1	acp_permissions	acp	20	219	220	ACP_VIEW_FORUM_MOD_PERMISSIONS	view_mod_local	acl_a_viewauth
108	1	1	acp_permissions	acp	20	221	222	ACP_VIEW_FORUM_PERMISSIONS	view_forum_local	acl_a_viewauth
21	1	1		acp	0	225	240	ACP_CAT_CUSTOMISE		
23	1	1		acp	21	226	229	ACP_EXTENSION_MANAGEMENT		
67	1	1	acp_extensions	acp	23	227	228	ACP_EXTENSIONS	main	acl_a_extensions
22	1	1		acp	21	230	235	ACP_STYLE_MANAGEMENT		
117	1	1	acp_styles	acp	22	231	232	ACP_STYLES	style	acl_a_styles
118	1	1	acp_styles	acp	22	233	234	ACP_STYLES_INSTALL	install	acl_a_styles
24	1	1		acp	21	236	239	ACP_LANGUAGE		
76	1	1	acp_language	acp	24	237	238	ACP_LANGUAGE_PACKS	lang_packs	acl_a_language
25	1	1		acp	0	241	260	ACP_CAT_MAINTENANCE		
77	1	1	acp_logs	acp	26	243	244	ACP_ADMIN_LOGS	admin	acl_a_viewlogs
78	1	1	acp_logs	acp	26	245	246	ACP_MOD_LOGS	mod	acl_a_viewlogs
79	1	1	acp_logs	acp	26	247	248	ACP_USERS_LOGS	users	acl_a_viewlogs
80	1	1	acp_logs	acp	26	249	250	ACP_CRITICAL_LOGS	critical	acl_a_viewlogs
27	1	1		acp	25	252	259	ACP_CAT_DATABASE		
63	1	1	acp_database	acp	27	253	254	ACP_BACKUP	backup	acl_a_backup
64	1	1	acp_database	acp	27	255	256	ACP_RESTORE	restore	acl_a_backup
116	1	1	acp_search	acp	27	257	258	ACP_SEARCH_INDEX	index	acl_a_search
28	1	1		acp	0	261	284	ACP_CAT_SYSTEM		
29	1	1		acp	28	262	265	ACP_AUTOMATION		
119	1	1	acp_update	acp	29	263	264	ACP_VERSION_CHECK	version_check	acl_a_board
30	1	1		acp	28	266	275	ACP_GENERAL_TASKS		
59	1	1	acp_bots	acp	30	267	268	ACP_BOTS	bots	acl_a_bots
66	1	1	acp_email	acp	30	269	270	ACP_MASS_EMAIL	email	acl_a_email && cfg_email_enable
109	1	1	acp_php_info	acp	30	271	272	ACP_PHP_INFO	info	acl_a_phpinfo
114	1	1	acp_reasons	acp	30	273	274	ACP_MANAGE_REASONS	main	acl_a_reasons
31	1	1		acp	28	276	283	ACP_MODULE_MANAGEMENT		
82	1	1	acp_modules	acp	31	277	278	ACP	acp	acl_a_modules
83	1	1	acp_modules	acp	31	279	280	UCP	ucp	acl_a_modules
84	1	1	acp_modules	acp	31	281	282	MCP	mcp	acl_a_modules
32	1	1		acp	0	285	286	ACP_CAT_DOT_MODS		
1	1	1		acp	0	1	68	ACP_CAT_GENERAL		
207	1	1	\\phpbb\\viglink\\acp\\viglink_module	acp	3	43	44	ACP_VIGLINK_SETTINGS	settings	ext_phpbb/viglink && acl_a_board
\.


--
-- Data for Name: phpbb_notification_emails; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_notification_emails (notification_type_id, item_id, item_parent_id, user_id) FROM stdin;
\.


--
-- Data for Name: phpbb_notification_types; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_notification_types (notification_type_id, notification_type_name, notification_type_enabled) FROM stdin;
\.


--
-- Data for Name: phpbb_notifications; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_notifications (notification_id, notification_type_id, item_id, item_parent_id, user_id, notification_read, notification_time, notification_data) FROM stdin;
\.


--
-- Data for Name: phpbb_oauth_accounts; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_oauth_accounts (user_id, provider, oauth_provider_id) FROM stdin;
\.


--
-- Data for Name: phpbb_oauth_states; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_oauth_states (user_id, session_id, provider, oauth_state) FROM stdin;
\.


--
-- Data for Name: phpbb_oauth_tokens; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_oauth_tokens (user_id, session_id, provider, oauth_token) FROM stdin;
\.


--
-- Data for Name: phpbb_poll_options; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_poll_options (poll_option_id, topic_id, poll_option_text, poll_option_total) FROM stdin;
\.


--
-- Data for Name: phpbb_poll_votes; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_poll_votes (topic_id, poll_option_id, vote_user_id, vote_user_ip) FROM stdin;
\.


--
-- Data for Name: phpbb_posts; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_posts (post_id, topic_id, forum_id, poster_id, icon_id, poster_ip, post_time, post_reported, enable_bbcode, enable_smilies, enable_magic_url, enable_sig, post_username, post_subject, post_text, post_checksum, post_attachment, bbcode_bitfield, bbcode_uid, post_postcount, post_edit_time, post_edit_reason, post_edit_user, post_edit_count, post_edit_locked, post_visibility, post_delete_time, post_delete_reason, post_delete_user) FROM stdin;
1	1	2	2	0	172.18.0.4	1628342817	0	1	1	1	1		Welcome to phpBB3	<t>This is an example post in your phpBB3 installation. Everything seems to be working. You may delete this post if you like and continue to set up your board. During the installation process your first category and your first forum are assigned an appropriate set of permissions for the predefined usergroups administrators, bots, global moderators, guests, registered users and registered COPPA users. If you also choose to delete your first category and your first forum, do not forget to assign permissions for all these usergroups for all new categories and forums you create. It is recommended to rename your first category and your first forum and copy permissions from these while creating new categories and forums. Have fun!</t>	5dd683b17f641daf84c040bfefc58ce9	0			1	0		0	0	0	1	0		0
\.


--
-- Data for Name: phpbb_privmsgs; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_privmsgs (msg_id, root_level, author_id, icon_id, author_ip, message_time, enable_bbcode, enable_smilies, enable_magic_url, enable_sig, message_subject, message_text, message_edit_reason, message_edit_user, message_attachment, bbcode_bitfield, bbcode_uid, message_edit_time, message_edit_count, to_address, bcc_address, message_reported) FROM stdin;
\.


--
-- Data for Name: phpbb_privmsgs_folder; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_privmsgs_folder (folder_id, user_id, folder_name, pm_count) FROM stdin;
\.


--
-- Data for Name: phpbb_privmsgs_rules; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_privmsgs_rules (rule_id, user_id, rule_check, rule_connection, rule_string, rule_user_id, rule_group_id, rule_action, rule_folder_id) FROM stdin;
\.


--
-- Data for Name: phpbb_privmsgs_to; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_privmsgs_to (msg_id, user_id, author_id, pm_deleted, pm_new, pm_unread, pm_replied, pm_marked, pm_forwarded, folder_id) FROM stdin;
\.


--
-- Data for Name: phpbb_profile_fields; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_profile_fields (field_id, field_name, field_type, field_ident, field_length, field_minlen, field_maxlen, field_novalue, field_default_value, field_validation, field_required, field_show_on_reg, field_hide, field_no_view, field_active, field_order, field_show_profile, field_show_on_vt, field_show_novalue, field_show_on_pm, field_show_on_ml, field_is_contact, field_contact_desc, field_contact_url) FROM stdin;
1	phpbb_location	profilefields.type.string	phpbb_location	20	2	100			.*	0	0	0	0	1	1	1	1	0	1	1	0		
2	phpbb_website	profilefields.type.url	phpbb_website	40	12	255				0	0	0	0	1	2	1	1	0	1	1	1	VISIT_WEBSITE	%s
3	phpbb_interests	profilefields.type.text	phpbb_interests	3|30	2	500			.*	0	0	0	0	0	3	1	0	0	0	0	0		
4	phpbb_occupation	profilefields.type.text	phpbb_occupation	3|30	2	500			.*	0	0	0	0	0	4	1	0	0	0	0	0		
5	phpbb_icq	profilefields.type.string	phpbb_icq	20	3	15			[0-9]+	0	0	0	0	0	6	1	1	0	1	1	1	SEND_ICQ_MESSAGE	https://www.icq.com/people/%s/
6	phpbb_yahoo	profilefields.type.string	phpbb_yahoo	40	5	255			.*	0	0	0	0	0	8	1	1	0	1	1	1	SEND_YIM_MESSAGE	ymsgr:sendim?%s
7	phpbb_facebook	profilefields.type.string	phpbb_facebook	20	5	50			[\\w.]+	0	0	0	0	1	9	1	1	0	1	1	1	VIEW_FACEBOOK_PROFILE	http://facebook.com/%s/
8	phpbb_twitter	profilefields.type.string	phpbb_twitter	20	1	15			[\\w_]+	0	0	0	0	1	10	1	1	0	1	1	1	VIEW_TWITTER_PROFILE	http://twitter.com/%s
9	phpbb_skype	profilefields.type.string	phpbb_skype	20	6	32			[a-zA-Z][\\w\\.,\\-_]+	0	0	0	0	1	11	1	1	0	1	1	1	VIEW_SKYPE_PROFILE	skype:%s?userinfo
10	phpbb_youtube	profilefields.type.string	phpbb_youtube	20	3	60			[a-zA-Z][\\w\\.,\\-_]+	0	0	0	0	1	12	1	1	0	1	1	1	VIEW_YOUTUBE_CHANNEL	http://youtube.com/user/%s
\.


--
-- Data for Name: phpbb_profile_fields_data; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_profile_fields_data (user_id, pf_phpbb_interests, pf_phpbb_occupation, pf_phpbb_location, pf_phpbb_youtube, pf_phpbb_facebook, pf_phpbb_icq, pf_phpbb_skype, pf_phpbb_twitter, pf_phpbb_website, pf_phpbb_yahoo) FROM stdin;
\.


--
-- Data for Name: phpbb_profile_fields_lang; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_profile_fields_lang (field_id, lang_id, option_id, field_type, lang_value) FROM stdin;
\.


--
-- Data for Name: phpbb_profile_lang; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_profile_lang (field_id, lang_id, lang_name, lang_explain, lang_default_value) FROM stdin;
1	1	LOCATION		
2	1	WEBSITE		
3	1	INTERESTS		
4	1	OCCUPATION		
5	1	ICQ		
6	1	YAHOO		
7	1	FACEBOOK		
8	1	TWITTER		
9	1	SKYPE		
10	1	YOUTUBE		
\.


--
-- Data for Name: phpbb_ranks; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_ranks (rank_id, rank_title, rank_min, rank_special, rank_image) FROM stdin;
1	Site Admin	0	1	
\.


--
-- Data for Name: phpbb_reports; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_reports (report_id, reason_id, post_id, user_id, user_notify, report_closed, report_time, report_text, pm_id, reported_post_enable_bbcode, reported_post_enable_smilies, reported_post_enable_magic_url, reported_post_text, reported_post_uid, reported_post_bitfield) FROM stdin;
\.


--
-- Data for Name: phpbb_reports_reasons; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_reports_reasons (reason_id, reason_title, reason_description, reason_order) FROM stdin;
1	warez	The post contains links to illegal or pirated software.	1
2	spam	The reported post has the only purpose to advertise for a website or another product.	2
3	off_topic	The reported post is off topic.	3
4	other	The reported post does not fit into any other category, please use the further information field.	4
\.


--
-- Data for Name: phpbb_search_results; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_search_results (search_key, search_time, search_keywords, search_authors) FROM stdin;
\.


--
-- Data for Name: phpbb_search_wordlist; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_search_wordlist (word_id, word_text, word_common, word_count) FROM stdin;
1	this	0	1
2	is	0	1
3	an	0	1
4	example	0	1
5	post	0	1
6	in	0	1
7	your	0	1
9	installation	0	1
10	everything	0	1
11	seems	0	1
13	be	0	1
14	working	0	1
15	you	0	1
16	may	0	1
17	delete	0	1
18	if	0	1
19	like	0	1
20	and	0	1
21	continue	0	1
22	set	0	1
23	up	0	1
24	board	0	1
25	during	0	1
26	the	0	1
27	process	0	1
28	first	0	1
29	category	0	1
30	forum	0	1
31	are	0	1
32	assigned	0	1
33	appropriate	0	1
34	of	0	1
35	permissions	0	1
36	for	0	1
37	predefined	0	1
38	usergroups	0	1
39	administrators	0	1
40	bots	0	1
41	global	0	1
42	moderators	0	1
43	guests	0	1
44	registered	0	1
45	users	0	1
46	coppa	0	1
47	also	0	1
48	choose	0	1
49	do	0	1
50	not	0	1
51	forget	0	1
52	assign	0	1
53	all	0	1
54	these	0	1
55	new	0	1
56	categories	0	1
57	forums	0	1
58	create	0	1
59	it	0	1
60	recommended	0	1
61	rename	0	1
62	copy	0	1
63	from	0	1
64	while	0	1
65	creating	0	1
66	have	0	1
67	fun	0	1
68	welcome	0	1
8	phpbb3	0	2
12	to	0	2
\.


--
-- Data for Name: phpbb_search_wordmatch; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_search_wordmatch (post_id, word_id, title_match) FROM stdin;
1	1	0
1	2	0
1	3	0
1	4	0
1	5	0
1	6	0
1	7	0
1	8	0
1	9	0
1	10	0
1	11	0
1	12	0
1	13	0
1	14	0
1	15	0
1	16	0
1	17	0
1	18	0
1	19	0
1	20	0
1	21	0
1	22	0
1	23	0
1	24	0
1	25	0
1	26	0
1	27	0
1	28	0
1	29	0
1	30	0
1	31	0
1	32	0
1	33	0
1	34	0
1	35	0
1	36	0
1	37	0
1	38	0
1	39	0
1	40	0
1	41	0
1	42	0
1	43	0
1	44	0
1	45	0
1	46	0
1	47	0
1	48	0
1	49	0
1	50	0
1	51	0
1	52	0
1	53	0
1	54	0
1	55	0
1	56	0
1	57	0
1	58	0
1	59	0
1	60	0
1	61	0
1	62	0
1	63	0
1	64	0
1	65	0
1	66	0
1	67	0
1	68	1
1	8	1
1	12	1
\.


--
-- Data for Name: phpbb_sessions; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_sessions (session_id, session_user_id, session_last_visit, session_start, session_time, session_ip, session_browser, session_forwarded_for, session_page, session_viewonline, session_autologin, session_admin, session_forum_id) FROM stdin;
91b657584472ae9aadc6805c133aafb9	1	1628342821	1628342821	1628342821	172.18.0.4	Mozilla/5.0 (X11; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0		install/app.php/install	1	0	0	0
fb9c3e757045d4eb862f258e6fabeb8c	1	1628342823	1628342823	1628342823	172.18.0.4	Mozilla/5.0 (X11; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0		install/app.php/install	1	0	0	0
876d5d03ba99ce359d1a31e66040a65c	1	1628342917	1628342917	1628342917	172.18.0.4	Mozilla/5.0 (X11; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0		ucp.php?mode=logout	1	0	0	0
5dd7cee0b319dce290bd517a4beedd94	1	1628342917	1628342917	1628342917	172.18.0.4	Mozilla/5.0 (X11; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0		index.php	1	0	0	0
ec8d72b42ba287022d6cb56d713e117c	1	1628345012	1628345012	1628345012	172.18.0.4	Mozilla/5.0 (X11; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0		index.php	1	0	0	0
\.


--
-- Data for Name: phpbb_sessions_keys; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_sessions_keys (key_id, user_id, last_ip, last_login) FROM stdin;
\.


--
-- Data for Name: phpbb_sitelist; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_sitelist (site_id, site_ip, site_hostname, ip_exclude) FROM stdin;
\.


--
-- Data for Name: phpbb_smilies; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_smilies (smiley_id, code, emotion, smiley_url, smiley_width, smiley_height, smiley_order, display_on_posting) FROM stdin;
1	:D	Very Happy	icon_e_biggrin.gif	15	17	1	1
2	:-D	Very Happy	icon_e_biggrin.gif	15	17	2	1
3	:grin:	Very Happy	icon_e_biggrin.gif	15	17	3	1
4	:)	Smile	icon_e_smile.gif	15	17	4	1
5	:-)	Smile	icon_e_smile.gif	15	17	5	1
6	:smile:	Smile	icon_e_smile.gif	15	17	6	1
7	;)	Wink	icon_e_wink.gif	15	17	7	1
8	;-)	Wink	icon_e_wink.gif	15	17	8	1
9	:wink:	Wink	icon_e_wink.gif	15	17	9	1
10	:(	Sad	icon_e_sad.gif	15	17	10	1
11	:-(	Sad	icon_e_sad.gif	15	17	11	1
12	:sad:	Sad	icon_e_sad.gif	15	17	12	1
13	:o	Surprised	icon_e_surprised.gif	15	17	13	1
14	:-o	Surprised	icon_e_surprised.gif	15	17	14	1
15	:eek:	Surprised	icon_e_surprised.gif	15	17	15	1
16	:shock:	Shocked	icon_eek.gif	15	17	16	1
17	:?	Confused	icon_e_confused.gif	15	17	17	1
18	:-?	Confused	icon_e_confused.gif	15	17	18	1
19	:???:	Confused	icon_e_confused.gif	15	17	19	1
20	8-)	Cool	icon_cool.gif	15	17	20	1
21	:cool:	Cool	icon_cool.gif	15	17	21	1
22	:lol:	Laughing	icon_lol.gif	15	17	22	1
23	:x	Mad	icon_mad.gif	15	17	23	1
24	:-x	Mad	icon_mad.gif	15	17	24	1
25	:mad:	Mad	icon_mad.gif	15	17	25	1
26	:P	Razz	icon_razz.gif	15	17	26	1
27	:-P	Razz	icon_razz.gif	15	17	27	1
28	:razz:	Razz	icon_razz.gif	15	17	28	1
29	:oops:	Embarrassed	icon_redface.gif	15	17	29	1
30	:cry:	Crying or Very Sad	icon_cry.gif	15	17	30	1
31	:evil:	Evil or Very Mad	icon_evil.gif	15	17	31	1
32	:twisted:	Twisted Evil	icon_twisted.gif	15	17	32	1
33	:roll:	Rolling Eyes	icon_rolleyes.gif	15	17	33	1
34	:!:	Exclamation	icon_exclaim.gif	15	17	34	1
35	:?:	Question	icon_question.gif	15	17	35	1
36	:idea:	Idea	icon_idea.gif	15	17	36	1
37	:arrow:	Arrow	icon_arrow.gif	15	17	37	1
38	:|	Neutral	icon_neutral.gif	15	17	38	1
39	:-|	Neutral	icon_neutral.gif	15	17	39	1
40	:mrgreen:	Mr. Green	icon_mrgreen.gif	15	17	40	1
41	:geek:	Geek	icon_e_geek.gif	17	17	41	1
42	:ugeek:	Uber Geek	icon_e_ugeek.gif	17	18	42	1
\.


--
-- Data for Name: phpbb_styles; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_styles (style_id, style_name, style_copyright, style_active, style_path, bbcode_bitfield, style_parent_id, style_parent_tree) FROM stdin;
1	prosilver	&copy; phpBB Limited	1	prosilver	//g=	0	
2	ProLight	© Ian Bradley, 2018	1	prolight	//g=	1	prosilver
3	Prosilver (Dark Edition)	© PlanetStyles.net (Premium Forum Themes)	1	prosilver_dark	//g=	1	prosilver
\.


--
-- Data for Name: phpbb_teampage; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_teampage (teampage_id, group_id, teampage_name, teampage_position, teampage_parent) FROM stdin;
1	5		1	0
2	4		2	0
\.


--
-- Data for Name: phpbb_topics; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_topics (topic_id, forum_id, icon_id, topic_attachment, topic_reported, topic_title, topic_poster, topic_time, topic_time_limit, topic_views, topic_status, topic_type, topic_first_post_id, topic_first_poster_name, topic_first_poster_colour, topic_last_post_id, topic_last_poster_id, topic_last_poster_name, topic_last_poster_colour, topic_last_post_subject, topic_last_post_time, topic_last_view_time, topic_moved_id, topic_bumped, topic_bumper, poll_title, poll_start, poll_length, poll_max_options, poll_last_vote, poll_vote_change, topic_visibility, topic_delete_time, topic_delete_reason, topic_delete_user, topic_posts_approved, topic_posts_unapproved, topic_posts_softdeleted) FROM stdin;
1	2	0	0	0	Welcome to phpBB3	2	1628342817	0	0	0	0	1	admin	AA0000	1	2	admin	AA0000	Welcome to phpBB3	1628342817	972086460	0	0	0		0	0	1	0	0	1	0		0	1	0	0
\.


--
-- Data for Name: phpbb_topics_posted; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_topics_posted (user_id, topic_id, topic_posted) FROM stdin;
2	1	1
\.


--
-- Data for Name: phpbb_topics_track; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_topics_track (user_id, topic_id, forum_id, mark_time) FROM stdin;
\.


--
-- Data for Name: phpbb_topics_watch; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_topics_watch (topic_id, user_id, notify_status) FROM stdin;
\.


--
-- Data for Name: phpbb_user_group; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_user_group (group_id, user_id, group_leader, user_pending) FROM stdin;
1	1	0	0
2	2	0	0
4	2	0	0
5	2	1	0
6	3	0	0
6	4	0	0
6	5	0	0
6	6	0	0
6	7	0	0
6	8	0	0
6	9	0	0
6	10	0	0
6	11	0	0
6	12	0	0
6	13	0	0
6	14	0	0
6	15	0	0
6	16	0	0
6	17	0	0
6	18	0	0
6	19	0	0
6	20	0	0
6	21	0	0
6	22	0	0
6	23	0	0
6	24	0	0
6	25	0	0
6	26	0	0
6	27	0	0
6	28	0	0
6	29	0	0
6	30	0	0
6	31	0	0
6	32	0	0
6	33	0	0
6	34	0	0
6	35	0	0
6	36	0	0
6	37	0	0
6	38	0	0
6	39	0	0
6	40	0	0
6	41	0	0
6	42	0	0
6	43	0	0
6	44	0	0
6	45	0	0
6	46	0	0
6	47	0	0
6	48	0	0
\.


--
-- Data for Name: phpbb_user_notifications; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_user_notifications (item_type, item_id, user_id, method, notify) FROM stdin;
notification.type.post	0	2	notification.method.board	1
notification.type.post	0	2	notification.method.email	1
notification.type.topic	0	2	notification.method.board	1
notification.type.topic	0	2	notification.method.email	1
notification.type.forum	0	2	notification.method.board	1
notification.type.forum	0	2	notification.method.email	1
notification.type.post	0	3	notification.method.email	1
notification.type.topic	0	3	notification.method.email	1
notification.type.post	0	4	notification.method.email	1
notification.type.topic	0	4	notification.method.email	1
notification.type.post	0	5	notification.method.email	1
notification.type.topic	0	5	notification.method.email	1
notification.type.post	0	6	notification.method.email	1
notification.type.topic	0	6	notification.method.email	1
notification.type.post	0	7	notification.method.email	1
notification.type.topic	0	7	notification.method.email	1
notification.type.post	0	8	notification.method.email	1
notification.type.topic	0	8	notification.method.email	1
notification.type.post	0	9	notification.method.email	1
notification.type.topic	0	9	notification.method.email	1
notification.type.post	0	10	notification.method.email	1
notification.type.topic	0	10	notification.method.email	1
notification.type.post	0	11	notification.method.email	1
notification.type.topic	0	11	notification.method.email	1
notification.type.post	0	12	notification.method.email	1
notification.type.topic	0	12	notification.method.email	1
notification.type.post	0	13	notification.method.email	1
notification.type.topic	0	13	notification.method.email	1
notification.type.post	0	14	notification.method.email	1
notification.type.topic	0	14	notification.method.email	1
notification.type.post	0	15	notification.method.email	1
notification.type.topic	0	15	notification.method.email	1
notification.type.post	0	16	notification.method.email	1
notification.type.topic	0	16	notification.method.email	1
notification.type.post	0	17	notification.method.email	1
notification.type.topic	0	17	notification.method.email	1
notification.type.post	0	18	notification.method.email	1
notification.type.topic	0	18	notification.method.email	1
notification.type.post	0	19	notification.method.email	1
notification.type.topic	0	19	notification.method.email	1
notification.type.post	0	20	notification.method.email	1
notification.type.topic	0	20	notification.method.email	1
notification.type.post	0	21	notification.method.email	1
notification.type.topic	0	21	notification.method.email	1
notification.type.post	0	22	notification.method.email	1
notification.type.topic	0	22	notification.method.email	1
notification.type.post	0	23	notification.method.email	1
notification.type.topic	0	23	notification.method.email	1
notification.type.post	0	24	notification.method.email	1
notification.type.topic	0	24	notification.method.email	1
notification.type.post	0	25	notification.method.email	1
notification.type.topic	0	25	notification.method.email	1
notification.type.post	0	26	notification.method.email	1
notification.type.topic	0	26	notification.method.email	1
notification.type.post	0	27	notification.method.email	1
notification.type.topic	0	27	notification.method.email	1
notification.type.post	0	28	notification.method.email	1
notification.type.topic	0	28	notification.method.email	1
notification.type.post	0	29	notification.method.email	1
notification.type.topic	0	29	notification.method.email	1
notification.type.post	0	30	notification.method.email	1
notification.type.topic	0	30	notification.method.email	1
notification.type.post	0	31	notification.method.email	1
notification.type.topic	0	31	notification.method.email	1
notification.type.post	0	32	notification.method.email	1
notification.type.topic	0	32	notification.method.email	1
notification.type.post	0	33	notification.method.email	1
notification.type.topic	0	33	notification.method.email	1
notification.type.post	0	34	notification.method.email	1
notification.type.topic	0	34	notification.method.email	1
notification.type.post	0	35	notification.method.email	1
notification.type.topic	0	35	notification.method.email	1
notification.type.post	0	36	notification.method.email	1
notification.type.topic	0	36	notification.method.email	1
notification.type.post	0	37	notification.method.email	1
notification.type.topic	0	37	notification.method.email	1
notification.type.post	0	38	notification.method.email	1
notification.type.topic	0	38	notification.method.email	1
notification.type.post	0	39	notification.method.email	1
notification.type.topic	0	39	notification.method.email	1
notification.type.post	0	40	notification.method.email	1
notification.type.topic	0	40	notification.method.email	1
notification.type.post	0	41	notification.method.email	1
notification.type.topic	0	41	notification.method.email	1
notification.type.post	0	42	notification.method.email	1
notification.type.topic	0	42	notification.method.email	1
notification.type.post	0	43	notification.method.email	1
notification.type.topic	0	43	notification.method.email	1
notification.type.post	0	44	notification.method.email	1
notification.type.topic	0	44	notification.method.email	1
notification.type.post	0	45	notification.method.email	1
notification.type.topic	0	45	notification.method.email	1
notification.type.post	0	46	notification.method.email	1
notification.type.topic	0	46	notification.method.email	1
notification.type.post	0	47	notification.method.email	1
notification.type.topic	0	47	notification.method.email	1
notification.type.post	0	48	notification.method.email	1
notification.type.topic	0	48	notification.method.email	1
\.


--
-- Data for Name: phpbb_users; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_users (user_id, user_type, group_id, user_permissions, user_perm_from, user_ip, user_regdate, username, username_clean, user_password, user_passchg, user_email, user_birthday, user_lastvisit, user_lastmark, user_lastpost_time, user_lastpage, user_last_confirm_key, user_last_search, user_warnings, user_last_warning, user_login_attempts, user_inactive_reason, user_inactive_time, user_posts, user_lang, user_timezone, user_dateformat, user_style, user_rank, user_colour, user_new_privmsg, user_unread_privmsg, user_last_privmsg, user_message_rules, user_full_folder, user_emailtime, user_topic_show_days, user_topic_sortby_type, user_topic_sortby_dir, user_post_show_days, user_post_sortby_type, user_post_sortby_dir, user_notify, user_notify_pm, user_notify_type, user_allow_pm, user_allow_viewonline, user_allow_viewemail, user_allow_massemail, user_options, user_avatar, user_avatar_type, user_avatar_width, user_avatar_height, user_sig, user_sig_bbcode_uid, user_sig_bbcode_bitfield, user_jabber, user_actkey, reset_token, reset_token_expiration, user_newpasswd, user_form_salt, user_new, user_reminded, user_reminded_time) FROM stdin;
1	2	1	00000000000g13ydmo\nhwby9w000000\nhwby9w000000	0		1628342817	Anonymous	anonymous		0			0	0	0			0	0	0	0	0	0	0	en		d M Y H:i	2	0		0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	1	1	1	0	230271			0	0							0		1t3lgkfnm3b76a7u	1	0	0
3	2	6		0		1628342817	AdsBot [Google]	adsbot [google]		1628342817			0	1628342817	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		u3ba2tljmechv7o2	0	0	0
4	2	6		0		1628342817	Alexa [Bot]	alexa [bot]		1628342817			0	1628342817	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		o9swdb916sgjuy2z	0	0	0
5	2	6		0		1628342817	Alta Vista [Bot]	alta vista [bot]		1628342817			0	1628342817	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		480hviv8vkvqbo3x	0	0	0
6	2	6		0		1628342817	Ask Jeeves [Bot]	ask jeeves [bot]		1628342817			0	1628342817	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		b9jgr9ru3ibggnyp	0	0	0
7	2	6		0		1628342817	Baidu [Spider]	baidu [spider]		1628342817			0	1628342817	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		7thj67dbq43ul2c7	0	0	0
8	2	6		0		1628342817	Bing [Bot]	bing [bot]		1628342817			0	1628342817	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		f562wcdviv0mt95l	0	0	0
9	2	6		0		1628342817	DuckDuckGo [Bot]	duckduckgo [bot]		1628342817			0	1628342817	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		5ttg8rlprj5go45u	0	0	0
10	2	6		0		1628342818	Exabot [Bot]	exabot [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		ff6cxy8woldls82k	0	0	0
11	2	6		0		1628342818	FAST Enterprise [Crawler]	fast enterprise [crawler]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		3b9ai1tuc5w21tqg	0	0	0
12	2	6		0		1628342818	FAST WebCrawler [Crawler]	fast webcrawler [crawler]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		zdf8jj8csnwhxpka	0	0	0
13	2	6		0		1628342818	Francis [Bot]	francis [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		9gjxqibeq9td0zto	0	0	0
14	2	6		0		1628342818	Gigabot [Bot]	gigabot [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		kw26lsgx0xqzel5y	0	0	0
15	2	6		0		1628342818	Google Adsense [Bot]	google adsense [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		767dqaq9mpp0qxq7	0	0	0
16	2	6		0		1628342818	Google Desktop	google desktop		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		kd3dn5j1wga0d6wk	0	0	0
17	2	6		0		1628342818	Google Feedfetcher	google feedfetcher		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		pvfpeeqo16knrj1k	0	0	0
18	2	6		0		1628342818	Google [Bot]	google [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		pom85pfssxq0o94n	0	0	0
19	2	6		0		1628342818	Heise IT-Markt [Crawler]	heise it-markt [crawler]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		7mktoct4fz1vneu3	0	0	0
20	2	6		0		1628342818	Heritrix [Crawler]	heritrix [crawler]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		mmdb2tkdybikhzuk	0	0	0
21	2	6		0		1628342818	IBM Research [Bot]	ibm research [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		utsb4qv4r1q0be4a	0	0	0
22	2	6		0		1628342818	ICCrawler - ICjobs	iccrawler - icjobs		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		0t2pluti3hacf1tl	0	0	0
23	2	6		0		1628342818	ichiro [Crawler]	ichiro [crawler]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		0mcrh42vf6mggnlk	0	0	0
24	2	6		0		1628342818	Majestic-12 [Bot]	majestic-12 [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		syf0l3mnyx9qnzpj	0	0	0
25	2	6		0		1628342818	Metager [Bot]	metager [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		qqsjbew20f7bbzbg	0	0	0
26	2	6		0		1628342818	MSN NewsBlogs	msn newsblogs		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		tmcic7muxkgfb45v	0	0	0
27	2	6		0		1628342818	MSN [Bot]	msn [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		wvpske0zeunty8g2	0	0	0
28	2	6		0		1628342818	MSNbot Media	msnbot media		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		529qtnl2ykqfsey7	0	0	0
29	2	6		0		1628342818	Nutch [Bot]	nutch [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		w0h8fl3y76d8mps8	0	0	0
30	2	6		0		1628342818	Online link [Validator]	online link [validator]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		m8szsrprpwjf4kn5	0	0	0
31	2	6		0		1628342818	psbot [Picsearch]	psbot [picsearch]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		fgaod4f4jwkfkpr5	0	0	0
32	2	6		0		1628342818	Sensis [Crawler]	sensis [crawler]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		x2bygnt5jkinfezi	0	0	0
33	2	6		0		1628342818	SEO Crawler	seo crawler		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		p8vnz1v7logil80e	0	0	0
34	2	6		0		1628342818	Seoma [Crawler]	seoma [crawler]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		ks6ufe1unv8vbxpw	0	0	0
35	2	6		0		1628342818	SEOSearch [Crawler]	seosearch [crawler]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		a3m04pi3ek141npb	0	0	0
36	2	6		0		1628342818	Snappy [Bot]	snappy [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		86prvosuywkjusaz	0	0	0
37	2	6		0		1628342818	Steeler [Crawler]	steeler [crawler]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		mefftokq0rbpo89j	0	0	0
38	2	6		0		1628342818	Telekom [Bot]	telekom [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		py918vjx7ctrg4w7	0	0	0
39	2	6		0		1628342818	TurnitinBot [Bot]	turnitinbot [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		564iip3izqd1apo7	0	0	0
40	2	6		0		1628342818	Voyager [Bot]	voyager [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		1q4avy66sssd83a5	0	0	0
41	2	6		0		1628342818	W3 [Sitesearch]	w3 [sitesearch]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		w9f2vequstjrgf2p	0	0	0
42	2	6		0		1628342818	W3C [Linkcheck]	w3c [linkcheck]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		jagacqrufcz4itaj	0	0	0
43	2	6		0		1628342818	W3C [Validator]	w3c [validator]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		9wm3otwpygbd7fc6	0	0	0
44	2	6		0		1628342818	YaCy [Bot]	yacy [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		d8ikrmvgr6nywb0d	0	0	0
45	2	6		0		1628342818	Yahoo MMCrawler [Bot]	yahoo mmcrawler [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		etozrukhmswt6hrv	0	0	0
46	2	6		0		1628342818	Yahoo Slurp [Bot]	yahoo slurp [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		st6tevj2vmq5px9j	0	0	0
47	2	6		0		1628342818	Yahoo [Bot]	yahoo [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		43baqygzzz0xanm2	0	0	0
48	2	6		0		1628342818	YahooSeeker [Bot]	yahooseeker [bot]		1628342818			0	1628342818	0			0	0	0	0	0	0	0	en	UTC	D M d, Y g:i a	1	0	9E8DA7	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	0	1	1	0	230271			0	0							0		gr20luy7u6o9wb9g	0	0	0
2	3	5	zik0zjzik0zjzik0zi\nhwby9w000000\nzik0zjzih7uo	0	172.18.0.4	1628342817	admin	admin	$argon2id$v=19$m=65536,t=4,p=2$R2JmbG1XUmI3RldZdWY0bw$cpaoCiq/3FSjMnJr3eS9j45jU7bpRVrveX0+pu0N0Oo	0	admin@local.host		1628345011	0	0			0	0	0	0	0	0	1	en		D M d, Y g:i a	1	1	AA0000	0	0	0	0	-3	0	0	t	d	0	t	a	0	1	0	1	1	1	1	230271			0	0							0		ogh65tphfu0mz0rh	1	0	0
\.


--
-- Data for Name: phpbb_warnings; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_warnings (warning_id, user_id, post_id, log_id, warning_time) FROM stdin;
\.


--
-- Data for Name: phpbb_words; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_words (word_id, word, replacement) FROM stdin;
\.


--
-- Data for Name: phpbb_zebra; Type: TABLE DATA; Schema: public; Owner: phpbb
--

COPY public.phpbb_zebra (user_id, zebra_id, friend, foe) FROM stdin;
\.


--
-- Name: phpbb_acl_options_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_acl_options_seq', 125, true);


--
-- Name: phpbb_acl_roles_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_acl_roles_seq', 24, true);


--
-- Name: phpbb_attachments_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_attachments_seq', 1, false);


--
-- Name: phpbb_banlist_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_banlist_seq', 1, false);


--
-- Name: phpbb_bots_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_bots_seq', 46, true);


--
-- Name: phpbb_disallow_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_disallow_seq', 1, false);


--
-- Name: phpbb_drafts_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_drafts_seq', 1, false);


--
-- Name: phpbb_extension_groups_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_extension_groups_seq', 5, true);


--
-- Name: phpbb_extensions_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_extensions_seq', 54, true);


--
-- Name: phpbb_forums_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_forums_seq', 2, true);


--
-- Name: phpbb_groups_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_groups_seq', 7, true);


--
-- Name: phpbb_icons_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_icons_seq', 10, true);


--
-- Name: phpbb_lang_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_lang_seq', 1, true);


--
-- Name: phpbb_log_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_log_seq', 12, true);


--
-- Name: phpbb_modules_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_modules_seq', 207, true);


--
-- Name: phpbb_notification_types_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_notification_types_seq', 1, false);


--
-- Name: phpbb_notifications_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_notifications_seq', 1, false);


--
-- Name: phpbb_posts_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_posts_seq', 1, true);


--
-- Name: phpbb_privmsgs_folder_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_privmsgs_folder_seq', 1, false);


--
-- Name: phpbb_privmsgs_rules_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_privmsgs_rules_seq', 1, false);


--
-- Name: phpbb_privmsgs_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_privmsgs_seq', 1, false);


--
-- Name: phpbb_profile_fields_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_profile_fields_seq', 10, true);


--
-- Name: phpbb_ranks_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_ranks_seq', 1, true);


--
-- Name: phpbb_reports_reasons_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_reports_reasons_seq', 4, true);


--
-- Name: phpbb_reports_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_reports_seq', 1, false);


--
-- Name: phpbb_search_wordlist_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_search_wordlist_seq', 68, true);


--
-- Name: phpbb_sitelist_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_sitelist_seq', 1, false);


--
-- Name: phpbb_smilies_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_smilies_seq', 42, true);


--
-- Name: phpbb_styles_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_styles_seq', 3, true);


--
-- Name: phpbb_teampage_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_teampage_seq', 2, true);


--
-- Name: phpbb_topics_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_topics_seq', 1, true);


--
-- Name: phpbb_users_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_users_seq', 48, true);


--
-- Name: phpbb_warnings_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_warnings_seq', 1, false);


--
-- Name: phpbb_words_seq; Type: SEQUENCE SET; Schema: public; Owner: phpbb
--

SELECT pg_catalog.setval('public.phpbb_words_seq', 1, false);


--
-- Name: phpbb_acl_options phpbb_acl_options_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_acl_options
    ADD CONSTRAINT phpbb_acl_options_pkey PRIMARY KEY (auth_option_id);


--
-- Name: phpbb_acl_roles_data phpbb_acl_roles_data_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_acl_roles_data
    ADD CONSTRAINT phpbb_acl_roles_data_pkey PRIMARY KEY (role_id, auth_option_id);


--
-- Name: phpbb_acl_roles phpbb_acl_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_acl_roles
    ADD CONSTRAINT phpbb_acl_roles_pkey PRIMARY KEY (role_id);


--
-- Name: phpbb_attachments phpbb_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_attachments
    ADD CONSTRAINT phpbb_attachments_pkey PRIMARY KEY (attach_id);


--
-- Name: phpbb_banlist phpbb_banlist_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_banlist
    ADD CONSTRAINT phpbb_banlist_pkey PRIMARY KEY (ban_id);


--
-- Name: phpbb_bbcodes phpbb_bbcodes_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_bbcodes
    ADD CONSTRAINT phpbb_bbcodes_pkey PRIMARY KEY (bbcode_id);


--
-- Name: phpbb_bookmarks phpbb_bookmarks_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_bookmarks
    ADD CONSTRAINT phpbb_bookmarks_pkey PRIMARY KEY (topic_id, user_id);


--
-- Name: phpbb_bots phpbb_bots_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_bots
    ADD CONSTRAINT phpbb_bots_pkey PRIMARY KEY (bot_id);


--
-- Name: phpbb_config phpbb_config_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_config
    ADD CONSTRAINT phpbb_config_pkey PRIMARY KEY (config_name);


--
-- Name: phpbb_config_text phpbb_config_text_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_config_text
    ADD CONSTRAINT phpbb_config_text_pkey PRIMARY KEY (config_name);


--
-- Name: phpbb_confirm phpbb_confirm_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_confirm
    ADD CONSTRAINT phpbb_confirm_pkey PRIMARY KEY (session_id, confirm_id);


--
-- Name: phpbb_disallow phpbb_disallow_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_disallow
    ADD CONSTRAINT phpbb_disallow_pkey PRIMARY KEY (disallow_id);


--
-- Name: phpbb_drafts phpbb_drafts_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_drafts
    ADD CONSTRAINT phpbb_drafts_pkey PRIMARY KEY (draft_id);


--
-- Name: phpbb_extension_groups phpbb_extension_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_extension_groups
    ADD CONSTRAINT phpbb_extension_groups_pkey PRIMARY KEY (group_id);


--
-- Name: phpbb_extensions phpbb_extensions_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_extensions
    ADD CONSTRAINT phpbb_extensions_pkey PRIMARY KEY (extension_id);


--
-- Name: phpbb_forums_access phpbb_forums_access_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_forums_access
    ADD CONSTRAINT phpbb_forums_access_pkey PRIMARY KEY (forum_id, user_id, session_id);


--
-- Name: phpbb_forums phpbb_forums_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_forums
    ADD CONSTRAINT phpbb_forums_pkey PRIMARY KEY (forum_id);


--
-- Name: phpbb_forums_track phpbb_forums_track_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_forums_track
    ADD CONSTRAINT phpbb_forums_track_pkey PRIMARY KEY (user_id, forum_id);


--
-- Name: phpbb_groups phpbb_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_groups
    ADD CONSTRAINT phpbb_groups_pkey PRIMARY KEY (group_id);


--
-- Name: phpbb_icons phpbb_icons_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_icons
    ADD CONSTRAINT phpbb_icons_pkey PRIMARY KEY (icons_id);


--
-- Name: phpbb_lang phpbb_lang_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_lang
    ADD CONSTRAINT phpbb_lang_pkey PRIMARY KEY (lang_id);


--
-- Name: phpbb_log phpbb_log_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_log
    ADD CONSTRAINT phpbb_log_pkey PRIMARY KEY (log_id);


--
-- Name: phpbb_migrations phpbb_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_migrations
    ADD CONSTRAINT phpbb_migrations_pkey PRIMARY KEY (migration_name);


--
-- Name: phpbb_modules phpbb_modules_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_modules
    ADD CONSTRAINT phpbb_modules_pkey PRIMARY KEY (module_id);


--
-- Name: phpbb_notification_emails phpbb_notification_emails_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_notification_emails
    ADD CONSTRAINT phpbb_notification_emails_pkey PRIMARY KEY (notification_type_id, item_id, item_parent_id, user_id);


--
-- Name: phpbb_notification_types phpbb_notification_types_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_notification_types
    ADD CONSTRAINT phpbb_notification_types_pkey PRIMARY KEY (notification_type_id);


--
-- Name: phpbb_notifications phpbb_notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_notifications
    ADD CONSTRAINT phpbb_notifications_pkey PRIMARY KEY (notification_id);


--
-- Name: phpbb_oauth_accounts phpbb_oauth_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_oauth_accounts
    ADD CONSTRAINT phpbb_oauth_accounts_pkey PRIMARY KEY (user_id, provider);


--
-- Name: phpbb_posts phpbb_posts_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_posts
    ADD CONSTRAINT phpbb_posts_pkey PRIMARY KEY (post_id);


--
-- Name: phpbb_privmsgs_folder phpbb_privmsgs_folder_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_privmsgs_folder
    ADD CONSTRAINT phpbb_privmsgs_folder_pkey PRIMARY KEY (folder_id);


--
-- Name: phpbb_privmsgs phpbb_privmsgs_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_privmsgs
    ADD CONSTRAINT phpbb_privmsgs_pkey PRIMARY KEY (msg_id);


--
-- Name: phpbb_privmsgs_rules phpbb_privmsgs_rules_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_privmsgs_rules
    ADD CONSTRAINT phpbb_privmsgs_rules_pkey PRIMARY KEY (rule_id);


--
-- Name: phpbb_profile_fields_data phpbb_profile_fields_data_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_profile_fields_data
    ADD CONSTRAINT phpbb_profile_fields_data_pkey PRIMARY KEY (user_id);


--
-- Name: phpbb_profile_fields_lang phpbb_profile_fields_lang_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_profile_fields_lang
    ADD CONSTRAINT phpbb_profile_fields_lang_pkey PRIMARY KEY (field_id, lang_id, option_id);


--
-- Name: phpbb_profile_fields phpbb_profile_fields_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_profile_fields
    ADD CONSTRAINT phpbb_profile_fields_pkey PRIMARY KEY (field_id);


--
-- Name: phpbb_profile_lang phpbb_profile_lang_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_profile_lang
    ADD CONSTRAINT phpbb_profile_lang_pkey PRIMARY KEY (field_id, lang_id);


--
-- Name: phpbb_ranks phpbb_ranks_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_ranks
    ADD CONSTRAINT phpbb_ranks_pkey PRIMARY KEY (rank_id);


--
-- Name: phpbb_reports phpbb_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_reports
    ADD CONSTRAINT phpbb_reports_pkey PRIMARY KEY (report_id);


--
-- Name: phpbb_reports_reasons phpbb_reports_reasons_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_reports_reasons
    ADD CONSTRAINT phpbb_reports_reasons_pkey PRIMARY KEY (reason_id);


--
-- Name: phpbb_search_results phpbb_search_results_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_search_results
    ADD CONSTRAINT phpbb_search_results_pkey PRIMARY KEY (search_key);


--
-- Name: phpbb_search_wordlist phpbb_search_wordlist_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_search_wordlist
    ADD CONSTRAINT phpbb_search_wordlist_pkey PRIMARY KEY (word_id);


--
-- Name: phpbb_sessions_keys phpbb_sessions_keys_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_sessions_keys
    ADD CONSTRAINT phpbb_sessions_keys_pkey PRIMARY KEY (key_id, user_id);


--
-- Name: phpbb_sessions phpbb_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_sessions
    ADD CONSTRAINT phpbb_sessions_pkey PRIMARY KEY (session_id);


--
-- Name: phpbb_sitelist phpbb_sitelist_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_sitelist
    ADD CONSTRAINT phpbb_sitelist_pkey PRIMARY KEY (site_id);


--
-- Name: phpbb_smilies phpbb_smilies_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_smilies
    ADD CONSTRAINT phpbb_smilies_pkey PRIMARY KEY (smiley_id);


--
-- Name: phpbb_styles phpbb_styles_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_styles
    ADD CONSTRAINT phpbb_styles_pkey PRIMARY KEY (style_id);


--
-- Name: phpbb_teampage phpbb_teampage_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_teampage
    ADD CONSTRAINT phpbb_teampage_pkey PRIMARY KEY (teampage_id);


--
-- Name: phpbb_topics phpbb_topics_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_topics
    ADD CONSTRAINT phpbb_topics_pkey PRIMARY KEY (topic_id);


--
-- Name: phpbb_topics_posted phpbb_topics_posted_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_topics_posted
    ADD CONSTRAINT phpbb_topics_posted_pkey PRIMARY KEY (user_id, topic_id);


--
-- Name: phpbb_topics_track phpbb_topics_track_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_topics_track
    ADD CONSTRAINT phpbb_topics_track_pkey PRIMARY KEY (user_id, topic_id);


--
-- Name: phpbb_users phpbb_users_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_users
    ADD CONSTRAINT phpbb_users_pkey PRIMARY KEY (user_id);


--
-- Name: phpbb_warnings phpbb_warnings_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_warnings
    ADD CONSTRAINT phpbb_warnings_pkey PRIMARY KEY (warning_id);


--
-- Name: phpbb_words phpbb_words_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_words
    ADD CONSTRAINT phpbb_words_pkey PRIMARY KEY (word_id);


--
-- Name: phpbb_zebra phpbb_zebra_pkey; Type: CONSTRAINT; Schema: public; Owner: phpbb
--

ALTER TABLE ONLY public.phpbb_zebra
    ADD CONSTRAINT phpbb_zebra_pkey PRIMARY KEY (user_id, zebra_id);


--
-- Name: phpbb_acl_groups_auth_opt_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_acl_groups_auth_opt_id ON public.phpbb_acl_groups USING btree (auth_option_id);


--
-- Name: phpbb_acl_groups_auth_role_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_acl_groups_auth_role_id ON public.phpbb_acl_groups USING btree (auth_role_id);


--
-- Name: phpbb_acl_groups_group_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_acl_groups_group_id ON public.phpbb_acl_groups USING btree (group_id);


--
-- Name: phpbb_acl_options_auth_option; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE UNIQUE INDEX phpbb_acl_options_auth_option ON public.phpbb_acl_options USING btree (auth_option);


--
-- Name: phpbb_acl_roles_data_ath_op_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_acl_roles_data_ath_op_id ON public.phpbb_acl_roles_data USING btree (auth_option_id);


--
-- Name: phpbb_acl_roles_role_order; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_acl_roles_role_order ON public.phpbb_acl_roles USING btree (role_order);


--
-- Name: phpbb_acl_roles_role_type; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_acl_roles_role_type ON public.phpbb_acl_roles USING btree (role_type);


--
-- Name: phpbb_acl_users_auth_option_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_acl_users_auth_option_id ON public.phpbb_acl_users USING btree (auth_option_id);


--
-- Name: phpbb_acl_users_auth_role_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_acl_users_auth_role_id ON public.phpbb_acl_users USING btree (auth_role_id);


--
-- Name: phpbb_acl_users_user_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_acl_users_user_id ON public.phpbb_acl_users USING btree (user_id);


--
-- Name: phpbb_attachments_filetime; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_attachments_filetime ON public.phpbb_attachments USING btree (filetime);


--
-- Name: phpbb_attachments_is_orphan; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_attachments_is_orphan ON public.phpbb_attachments USING btree (is_orphan);


--
-- Name: phpbb_attachments_post_msg_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_attachments_post_msg_id ON public.phpbb_attachments USING btree (post_msg_id);


--
-- Name: phpbb_attachments_poster_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_attachments_poster_id ON public.phpbb_attachments USING btree (poster_id);


--
-- Name: phpbb_attachments_topic_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_attachments_topic_id ON public.phpbb_attachments USING btree (topic_id);


--
-- Name: phpbb_banlist_ban_email; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_banlist_ban_email ON public.phpbb_banlist USING btree (ban_email, ban_exclude);


--
-- Name: phpbb_banlist_ban_end; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_banlist_ban_end ON public.phpbb_banlist USING btree (ban_end);


--
-- Name: phpbb_banlist_ban_ip; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_banlist_ban_ip ON public.phpbb_banlist USING btree (ban_ip, ban_exclude);


--
-- Name: phpbb_banlist_ban_user; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_banlist_ban_user ON public.phpbb_banlist USING btree (ban_userid, ban_exclude);


--
-- Name: phpbb_bbcodes_display_on_post; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_bbcodes_display_on_post ON public.phpbb_bbcodes USING btree (display_on_posting);


--
-- Name: phpbb_bots_bot_active; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_bots_bot_active ON public.phpbb_bots USING btree (bot_active);


--
-- Name: phpbb_config_is_dynamic; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_config_is_dynamic ON public.phpbb_config USING btree (is_dynamic);


--
-- Name: phpbb_confirm_confirm_type; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_confirm_confirm_type ON public.phpbb_confirm USING btree (confirm_type);


--
-- Name: phpbb_drafts_save_time; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_drafts_save_time ON public.phpbb_drafts USING btree (save_time);


--
-- Name: phpbb_ext_ext_name; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE UNIQUE INDEX phpbb_ext_ext_name ON public.phpbb_ext USING btree (ext_name);


--
-- Name: phpbb_forums_forum_lastpost_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_forums_forum_lastpost_id ON public.phpbb_forums USING btree (forum_last_post_id);


--
-- Name: phpbb_forums_left_right_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_forums_left_right_id ON public.phpbb_forums USING btree (left_id, right_id);


--
-- Name: phpbb_forums_watch_forum_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_forums_watch_forum_id ON public.phpbb_forums_watch USING btree (forum_id);


--
-- Name: phpbb_forums_watch_notify_stat; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_forums_watch_notify_stat ON public.phpbb_forums_watch USING btree (notify_status);


--
-- Name: phpbb_forums_watch_user_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_forums_watch_user_id ON public.phpbb_forums_watch USING btree (user_id);


--
-- Name: phpbb_groups_group_legend_name; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_groups_group_legend_name ON public.phpbb_groups USING btree (group_legend, group_name);


--
-- Name: phpbb_icons_display_on_posting; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_icons_display_on_posting ON public.phpbb_icons USING btree (display_on_posting);


--
-- Name: phpbb_lang_lang_iso; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_lang_lang_iso ON public.phpbb_lang USING btree (lang_iso);


--
-- Name: phpbb_log_forum_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_log_forum_id ON public.phpbb_log USING btree (forum_id);


--
-- Name: phpbb_log_log_time; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_log_log_time ON public.phpbb_log USING btree (log_time);


--
-- Name: phpbb_log_log_type; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_log_log_type ON public.phpbb_log USING btree (log_type);


--
-- Name: phpbb_log_reportee_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_log_reportee_id ON public.phpbb_log USING btree (reportee_id);


--
-- Name: phpbb_log_topic_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_log_topic_id ON public.phpbb_log USING btree (topic_id);


--
-- Name: phpbb_log_user_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_log_user_id ON public.phpbb_log USING btree (user_id);


--
-- Name: phpbb_login_attempts_att_for; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_login_attempts_att_for ON public.phpbb_login_attempts USING btree (attempt_forwarded_for, attempt_time);


--
-- Name: phpbb_login_attempts_att_ip; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_login_attempts_att_ip ON public.phpbb_login_attempts USING btree (attempt_ip, attempt_time);


--
-- Name: phpbb_login_attempts_att_time; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_login_attempts_att_time ON public.phpbb_login_attempts USING btree (attempt_time);


--
-- Name: phpbb_login_attempts_user_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_login_attempts_user_id ON public.phpbb_login_attempts USING btree (user_id);


--
-- Name: phpbb_moderator_cache_disp_idx; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_moderator_cache_disp_idx ON public.phpbb_moderator_cache USING btree (display_on_index);


--
-- Name: phpbb_moderator_cache_forum_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_moderator_cache_forum_id ON public.phpbb_moderator_cache USING btree (forum_id);


--
-- Name: phpbb_modules_class_left_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_modules_class_left_id ON public.phpbb_modules USING btree (module_class, left_id);


--
-- Name: phpbb_modules_left_right_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_modules_left_right_id ON public.phpbb_modules USING btree (left_id, right_id);


--
-- Name: phpbb_modules_module_enabled; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_modules_module_enabled ON public.phpbb_modules USING btree (module_enabled);


--
-- Name: phpbb_notification_types_type; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE UNIQUE INDEX phpbb_notification_types_type ON public.phpbb_notification_types USING btree (notification_type_name);


--
-- Name: phpbb_notifications_item_ident; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_notifications_item_ident ON public.phpbb_notifications USING btree (notification_type_id, item_id);


--
-- Name: phpbb_notifications_user; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_notifications_user ON public.phpbb_notifications USING btree (user_id, notification_read);


--
-- Name: phpbb_oauth_states_provider; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_oauth_states_provider ON public.phpbb_oauth_states USING btree (provider);


--
-- Name: phpbb_oauth_states_user_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_oauth_states_user_id ON public.phpbb_oauth_states USING btree (user_id);


--
-- Name: phpbb_oauth_tokens_provider; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_oauth_tokens_provider ON public.phpbb_oauth_tokens USING btree (provider);


--
-- Name: phpbb_oauth_tokens_user_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_oauth_tokens_user_id ON public.phpbb_oauth_tokens USING btree (user_id);


--
-- Name: phpbb_poll_options_poll_opt_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_poll_options_poll_opt_id ON public.phpbb_poll_options USING btree (poll_option_id);


--
-- Name: phpbb_poll_options_topic_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_poll_options_topic_id ON public.phpbb_poll_options USING btree (topic_id);


--
-- Name: phpbb_poll_votes_topic_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_poll_votes_topic_id ON public.phpbb_poll_votes USING btree (topic_id);


--
-- Name: phpbb_poll_votes_vote_user_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_poll_votes_vote_user_id ON public.phpbb_poll_votes USING btree (vote_user_id);


--
-- Name: phpbb_poll_votes_vote_user_ip; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_poll_votes_vote_user_ip ON public.phpbb_poll_votes USING btree (vote_user_ip);


--
-- Name: phpbb_posts_forum_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_posts_forum_id ON public.phpbb_posts USING btree (forum_id);


--
-- Name: phpbb_posts_post_username; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_posts_post_username ON public.phpbb_posts USING btree (post_username);


--
-- Name: phpbb_posts_post_visibility; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_posts_post_visibility ON public.phpbb_posts USING btree (post_visibility);


--
-- Name: phpbb_posts_poster_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_posts_poster_id ON public.phpbb_posts USING btree (poster_id);


--
-- Name: phpbb_posts_poster_ip; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_posts_poster_ip ON public.phpbb_posts USING btree (poster_ip);


--
-- Name: phpbb_posts_tid_post_time; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_posts_tid_post_time ON public.phpbb_posts USING btree (topic_id, post_time);


--
-- Name: phpbb_posts_topic_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_posts_topic_id ON public.phpbb_posts USING btree (topic_id);


--
-- Name: phpbb_privmsgs_author_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_privmsgs_author_id ON public.phpbb_privmsgs USING btree (author_id);


--
-- Name: phpbb_privmsgs_author_ip; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_privmsgs_author_ip ON public.phpbb_privmsgs USING btree (author_ip);


--
-- Name: phpbb_privmsgs_folder_user_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_privmsgs_folder_user_id ON public.phpbb_privmsgs_folder USING btree (user_id);


--
-- Name: phpbb_privmsgs_message_time; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_privmsgs_message_time ON public.phpbb_privmsgs USING btree (message_time);


--
-- Name: phpbb_privmsgs_root_level; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_privmsgs_root_level ON public.phpbb_privmsgs USING btree (root_level);


--
-- Name: phpbb_privmsgs_rules_user_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_privmsgs_rules_user_id ON public.phpbb_privmsgs_rules USING btree (user_id);


--
-- Name: phpbb_privmsgs_to_author_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_privmsgs_to_author_id ON public.phpbb_privmsgs_to USING btree (author_id);


--
-- Name: phpbb_privmsgs_to_msg_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_privmsgs_to_msg_id ON public.phpbb_privmsgs_to USING btree (msg_id);


--
-- Name: phpbb_privmsgs_to_usr_flder_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_privmsgs_to_usr_flder_id ON public.phpbb_privmsgs_to USING btree (user_id, folder_id);


--
-- Name: phpbb_profile_fields_fld_ordr; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_profile_fields_fld_ordr ON public.phpbb_profile_fields USING btree (field_order);


--
-- Name: phpbb_profile_fields_fld_type; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_profile_fields_fld_type ON public.phpbb_profile_fields USING btree (field_type);


--
-- Name: phpbb_reports_pm_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_reports_pm_id ON public.phpbb_reports USING btree (pm_id);


--
-- Name: phpbb_reports_post_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_reports_post_id ON public.phpbb_reports USING btree (post_id);


--
-- Name: phpbb_search_wordlist_wrd_cnt; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_search_wordlist_wrd_cnt ON public.phpbb_search_wordlist USING btree (word_count);


--
-- Name: phpbb_search_wordlist_wrd_txt; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE UNIQUE INDEX phpbb_search_wordlist_wrd_txt ON public.phpbb_search_wordlist USING btree (word_text);


--
-- Name: phpbb_search_wordmatch_post_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_search_wordmatch_post_id ON public.phpbb_search_wordmatch USING btree (post_id);


--
-- Name: phpbb_search_wordmatch_un_mtch; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE UNIQUE INDEX phpbb_search_wordmatch_un_mtch ON public.phpbb_search_wordmatch USING btree (word_id, post_id, title_match);


--
-- Name: phpbb_search_wordmatch_word_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_search_wordmatch_word_id ON public.phpbb_search_wordmatch USING btree (word_id);


--
-- Name: phpbb_sessions_keys_last_login; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_sessions_keys_last_login ON public.phpbb_sessions_keys USING btree (last_login);


--
-- Name: phpbb_sessions_session_fid; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_sessions_session_fid ON public.phpbb_sessions USING btree (session_forum_id);


--
-- Name: phpbb_sessions_session_time; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_sessions_session_time ON public.phpbb_sessions USING btree (session_time);


--
-- Name: phpbb_sessions_session_user_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_sessions_session_user_id ON public.phpbb_sessions USING btree (session_user_id);


--
-- Name: phpbb_smilies_display_on_post; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_smilies_display_on_post ON public.phpbb_smilies USING btree (display_on_posting);


--
-- Name: phpbb_styles_style_name; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE UNIQUE INDEX phpbb_styles_style_name ON public.phpbb_styles USING btree (style_name);


--
-- Name: phpbb_topics_fid_time_moved; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_topics_fid_time_moved ON public.phpbb_topics USING btree (forum_id, topic_last_post_time, topic_moved_id);


--
-- Name: phpbb_topics_forum_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_topics_forum_id ON public.phpbb_topics USING btree (forum_id);


--
-- Name: phpbb_topics_forum_id_type; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_topics_forum_id_type ON public.phpbb_topics USING btree (forum_id, topic_type);


--
-- Name: phpbb_topics_forum_vis_last; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_topics_forum_vis_last ON public.phpbb_topics USING btree (forum_id, topic_visibility, topic_last_post_id);


--
-- Name: phpbb_topics_last_post_time; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_topics_last_post_time ON public.phpbb_topics USING btree (topic_last_post_time);


--
-- Name: phpbb_topics_latest_topics; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_topics_latest_topics ON public.phpbb_topics USING btree (forum_id, topic_last_post_time, topic_last_post_id, topic_moved_id);


--
-- Name: phpbb_topics_topic_visibility; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_topics_topic_visibility ON public.phpbb_topics USING btree (topic_visibility);


--
-- Name: phpbb_topics_track_forum_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_topics_track_forum_id ON public.phpbb_topics_track USING btree (forum_id);


--
-- Name: phpbb_topics_track_topic_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_topics_track_topic_id ON public.phpbb_topics_track USING btree (topic_id);


--
-- Name: phpbb_topics_watch_notify_stat; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_topics_watch_notify_stat ON public.phpbb_topics_watch USING btree (notify_status);


--
-- Name: phpbb_topics_watch_topic_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_topics_watch_topic_id ON public.phpbb_topics_watch USING btree (topic_id);


--
-- Name: phpbb_topics_watch_user_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_topics_watch_user_id ON public.phpbb_topics_watch USING btree (user_id);


--
-- Name: phpbb_user_group_group_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_user_group_group_id ON public.phpbb_user_group USING btree (group_id);


--
-- Name: phpbb_user_group_group_leader; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_user_group_group_leader ON public.phpbb_user_group USING btree (group_leader);


--
-- Name: phpbb_user_group_user_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_user_group_user_id ON public.phpbb_user_group USING btree (user_id);


--
-- Name: phpbb_user_notifications_itm_usr_mthd; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE UNIQUE INDEX phpbb_user_notifications_itm_usr_mthd ON public.phpbb_user_notifications USING btree (item_type, item_id, user_id, method);


--
-- Name: phpbb_user_notifications_uid_itm_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_user_notifications_uid_itm_id ON public.phpbb_user_notifications USING btree (user_id, item_id);


--
-- Name: phpbb_user_notifications_user_id; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_user_notifications_user_id ON public.phpbb_user_notifications USING btree (user_id);


--
-- Name: phpbb_user_notifications_usr_itm_tpe; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_user_notifications_usr_itm_tpe ON public.phpbb_user_notifications USING btree (user_id, item_type, item_id);


--
-- Name: phpbb_users_user_birthday; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_users_user_birthday ON public.phpbb_users USING btree (user_birthday);


--
-- Name: phpbb_users_user_email; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_users_user_email ON public.phpbb_users USING btree (user_email);


--
-- Name: phpbb_users_user_type; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE INDEX phpbb_users_user_type ON public.phpbb_users USING btree (user_type);


--
-- Name: phpbb_users_username_clean; Type: INDEX; Schema: public; Owner: phpbb
--

CREATE UNIQUE INDEX phpbb_users_username_clean ON public.phpbb_users USING btree (username_clean);


--
-- PostgreSQL database dump complete
--

